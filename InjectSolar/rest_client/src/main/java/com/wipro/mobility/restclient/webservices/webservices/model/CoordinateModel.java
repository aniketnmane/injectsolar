package com.wipro.mobility.restclient.webservices.webservices.model;

/**
 * Created by Aniket on 3/8/2019.
 */

/**
 *
 */
public class CoordinateModel {

    public static final String LAT_KEY = "lat";
    public static final String LONG_KEY = "lon";
    private double latitude;

    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
