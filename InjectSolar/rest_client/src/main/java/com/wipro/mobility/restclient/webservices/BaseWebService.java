package com.wipro.mobility.restclient.webservices;

import android.os.AsyncTask;

import java.util.ArrayList;

/**
 * Created by Aniket on 3/8/2019.
 */

public abstract class BaseWebService {
    protected enum CommunicationMethod {
        GET, POST, PUT, DELETE
    }

    protected abstract int getRequestCode();

    protected abstract String getUrl();

    protected abstract CommunicationMethod getCommunicationMethod();

    protected abstract ArrayList<CommunicationHeader> getHeaders();

    protected abstract ArrayList<CommunicationParam> getCommunicationParams();

    protected abstract String getPostData();

    protected abstract void handleResponse(CommunicationResponse response);

    protected abstract void handleError(CommunicationResponse error);

    public void sendRequest() {
        new NetworkCommunicationTask().execute();
    }

    private class NetworkCommunicationTask extends AsyncTask<Void, Integer, CommunicationResponse> {

        @Override
        protected CommunicationResponse doInBackground(Void... params) {
            HttpCommunication communication = new HttpCommunication();
            CommunicationResponse communicationResponse = communication.sendHttpRequest(BaseWebService.this);
            return communicationResponse;
        }

        @Override
        protected void onPostExecute(CommunicationResponse communicationResponse) {
            super.onPostExecute(communicationResponse);
            if (communicationResponse.getErrorCode() == 0) {
                //Notify success
                handleResponse(communicationResponse);
            } else {
                //Notify failure
                handleError(communicationResponse);
            }
        }
    }
}
