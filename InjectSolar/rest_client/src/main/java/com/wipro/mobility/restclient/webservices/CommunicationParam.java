package com.wipro.mobility.restclient.webservices;

/**
 * Created by Aniket on 3/8/2017.
 */

public class CommunicationParam {
    private String name;
    private String value;

    public CommunicationParam(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
