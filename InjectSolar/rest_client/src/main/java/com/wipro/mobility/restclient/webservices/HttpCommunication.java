package com.wipro.mobility.restclient.webservices;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;


/**
 * Created by Aniket on 3/8/2017.
 */

class HttpCommunication {
    private static final int COMMUNICATION_SUCCESS = 200;
    private final int CONNECTION_TIMEOUT = 10 * 1000;
    private final int READ_TIMEOUT = 10 * 1000;

    public CommunicationResponse sendHttpRequest(BaseWebService webService) {
        int requestCode = webService.getRequestCode();
        CommunicationResponse communicationResponse = null;
        BaseWebService.CommunicationMethod method = webService.getCommunicationMethod();
        String webserviceUrl = webService.getUrl();
        ArrayList<CommunicationHeader> headers = webService.getHeaders();
        ArrayList<CommunicationParam> params = webService.getCommunicationParams();
        String postData = webService.getPostData();

        URL url = null;

        StringBuilder strCommunicationParams = new StringBuilder();
        if (params != null && params.size() > 0) {
            for (CommunicationParam param : params) {
                strCommunicationParams.append(param.getName());
                strCommunicationParams.append("=");
                if (param.getValue() != null)
                    strCommunicationParams.append(param.getValue());
                else
                    strCommunicationParams.append("");

                strCommunicationParams.append("&");
            }

            strCommunicationParams.deleteCharAt(strCommunicationParams.length() - 1);
        }
        //Check if URL is valid
        try {
            url = new URL(webserviceUrl + strCommunicationParams.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            communicationResponse = new CommunicationResponse(requestCode, CommunicationError.MALFORMED_URL, "Invalid/Malformed url");
            return communicationResponse;
        }

        URLConnection connection = null;
        try {
            //Provision to Add SSL certificate for https connection
            if (url.getProtocol().toLowerCase().equals("https")) {
                connection = (HttpsURLConnection) url.openConnection();
                try {
                    KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
                    keyStore.load(null, null);

                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                    tmf.init(keyStore);

                    SSLSocketFactory sslSocketFactory = getSslSocketFactoryInstance();
                    ((HttpsURLConnection) connection).setSSLSocketFactory(sslSocketFactory);
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                    communicationResponse = new CommunicationResponse(requestCode, CommunicationError.KEYSTORE_ERROR, "Keystore access error.");
                    return communicationResponse;
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    communicationResponse = new CommunicationResponse(requestCode, CommunicationError.KEYSTORE_ERROR, "Keystore algorithm error.");
                    return communicationResponse;
                } catch (CertificateException e) {
                    e.printStackTrace();
                    communicationResponse = new CommunicationResponse(requestCode, CommunicationError.KEYSTORE_ERROR, "Keystore certificate error.");
                    return communicationResponse;
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                    communicationResponse = new CommunicationResponse(requestCode, CommunicationError.KEYSTORE_ERROR, "Keystore SSL factory error.");
                    return communicationResponse;
                }
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }

            //Set the http request method
            ((HttpURLConnection) connection).setRequestMethod(method.toString());

            //Set this connection not to use cache
            connection.setUseCaches(false);
            connection.setDoInput(true);

             /*   if (params != null && params.size() > 0) {
                    ((HttpURLConnection) connection).setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            }*/

            if (postData != null && postData.length() > 0 || params != null && params.size() > 0) {
                connection.setDoOutput(true);
            } else {
                connection.setDoOutput(false);
            }

            //Set the connection and read timeout
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setReadTimeout(READ_TIMEOUT);

            //Add request headers
            if (headers != null && headers.size() > 0) {
                for (CommunicationHeader header : headers) {
                    connection.addRequestProperty(header.getName(), header.getValue());
                }
            }

            connection.connect();



            int statusCode = ((HttpURLConnection) connection).getResponseCode();
            if (statusCode == COMMUNICATION_SUCCESS) {
                //If status is 200, read the response from server.
                StringBuffer responseMessage = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    responseMessage.append(line);
                }
                communicationResponse = new CommunicationResponse(requestCode, responseMessage.toString());
                return communicationResponse;

            } else {
                //If status code is not 200, that means error has occurred. Read error message from server.
                StringBuffer errorMessage = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(((HttpURLConnection) connection).getErrorStream()));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    errorMessage.append(line);
                }
                communicationResponse = new CommunicationResponse(requestCode, statusCode, errorMessage.toString());
                return communicationResponse;
            }
        } catch (IOException e) {
            e.printStackTrace();
            communicationResponse = new CommunicationResponse(requestCode, CommunicationError.CONNECTION_ERROR, "Error in connecting to server. " + e.getMessage());
            return communicationResponse;
        }
    }

    private SSLSocketFactory getSslSocketFactoryInstance() throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] managers = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        chain[0].checkValidity();
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, managers, new SecureRandom());
        return sslContext.getSocketFactory();
    }
}
