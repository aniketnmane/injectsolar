package com.wipro.mobility.restclient.webservices;

/**
 * Created by Aniket on 3/8/2019.
 */

public class CommunicationHeader {
    private String name;
    private String value;

    public CommunicationHeader(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
