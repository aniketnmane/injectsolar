package com.wipro.mobility.restclient.webservices.webservices.model;

public class LoginModel {
    public static final String USER_ID = "userid";
    public static final String PASSWORD = "password";
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
