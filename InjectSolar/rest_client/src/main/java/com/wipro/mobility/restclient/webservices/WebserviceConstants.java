package com.wipro.mobility.restclient.webservices;

/**
 * Created by Aniket on 3/8/2019.
 */

public class WebserviceConstants {
    public static final String WEB_SERVICE_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast?";
    public static final int WEATHER_BY_COORDINATES = 101;
    public static final int LOGIN_DETAILS = 102;

    public static final String APP_ID_KEY = "appid";
    public static final String APP_ID_VALUE = "c36235879802f67b5096c5239444cf8d";
}
