package com.wipro.mobility.restclient.webservices.webservices;

import com.wipro.mobility.restclient.webservices.BaseWebService;
import com.wipro.mobility.restclient.webservices.CommunicationHeader;
import com.wipro.mobility.restclient.webservices.CommunicationParam;
import com.wipro.mobility.restclient.webservices.CommunicationResponse;
import com.wipro.mobility.restclient.webservices.ICommunicationCallback;
import com.wipro.mobility.restclient.webservices.WebserviceConstants;
import com.wipro.mobility.restclient.webservices.webservices.model.LoginModel;

import java.util.ArrayList;

/**
 * Created by Aniket on 3/8/2019.
 */

/**
 * This class is used to get the weather data based upon coordinates
 *
 * @author AN348207
 * @version 1.0
 * @since 08-03-2017
 */
public class GetLoginDetails extends BaseWebService {
    private String URL = WebserviceConstants.WEB_SERVICE_BASE_URL;
    private LoginModel model;
    private ICommunicationCallback callbackListener;
    private final String REQUEST = "request";

    /**
     * @param model
     * @param callback
     */
    public GetLoginDetails(LoginModel model, ICommunicationCallback callback) {
        this.model = model;
        this.callbackListener = callback;
    }

    @Override
    protected int getRequestCode() {
        return WebserviceConstants.LOGIN_DETAILS;
    }

    @Override
    protected String getUrl() {
        return URL;
    }

    @Override
    protected CommunicationMethod getCommunicationMethod() {
        return CommunicationMethod.GET;
    }

    @Override
    protected ArrayList<CommunicationHeader> getHeaders() {
        return null;
    }

    @Override
    protected ArrayList<CommunicationParam> getCommunicationParams() {
        ArrayList<CommunicationParam> paramsFirst = new ArrayList<>(1);

        ArrayList<CommunicationParam> params = new ArrayList<>(2);
        params.add(new CommunicationParam(LoginModel.USER_ID, model.getUserName()));
        params.add(new CommunicationParam(LoginModel.PASSWORD, model.getPassword()));
        // params.add(new CommunicationParam(WebserviceConstants.APP_ID_KEY, WebserviceConstants.APP_ID_VALUE));
        return params;
    }

    @Override
    protected String getPostData() {
        return null;
    }

    @Override
    protected void handleResponse(CommunicationResponse response) {
        callbackListener.onCommunicationSuccess(response);

    }

    @Override
    protected void handleError(CommunicationResponse error) {
        callbackListener.onCommunicationFailure(error);
    }
}
