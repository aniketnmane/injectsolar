package com.wipro.mobility.restclient.webservices;

/**
 * Created by Aniket on 3/8/2019.
 */

public class CommunicationError {
    public static final int MALFORMED_URL = -1;
    public static final int KEYSTORE_ERROR = -2;
    public static final int CONNECTION_ERROR = -3;
}
