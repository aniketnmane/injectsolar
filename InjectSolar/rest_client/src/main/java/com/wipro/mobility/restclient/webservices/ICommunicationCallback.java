package com.wipro.mobility.restclient.webservices;

/**
 * Created by Aniket on 3/8/2019.
 */

public interface ICommunicationCallback {
    public void onCommunicationSuccess(CommunicationResponse response);

    public void onCommunicationFailure(CommunicationResponse error);
}
