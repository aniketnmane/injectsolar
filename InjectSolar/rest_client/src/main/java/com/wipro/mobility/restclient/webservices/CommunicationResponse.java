package com.wipro.mobility.restclient.webservices;

/**
 * Created by Aniket on 3/8/2019.
 */

public class CommunicationResponse {
    private int requestCode;
    private String responseMessage;
    private int errorCode;
    private String errorMessage;

    public CommunicationResponse(int requestCode, String responseMessage) {
        this.requestCode = requestCode;
        this.responseMessage = responseMessage;
    }

    public CommunicationResponse(int requestCode, int errorCode, String errorMessage) {
        this.requestCode = requestCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getRequestCode() {
        return requestCode;
    }
}

