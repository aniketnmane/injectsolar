package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aniket on 11/7/19.
 */

public class SystemDetailsChildViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDeviceNameSerialNo)
    public TextView tvDeviceNameSerialNo;
    @BindView(R.id.tvInverterNo)
    public TextView tvInverterNo;
    @BindView(R.id.tvInverterName)
    public TextView tvInverterName;
    @BindView(R.id.tvInverterCapacity)
    public TextView tvInverterCapacity;
    @BindView(R.id.tvInverterSpecification)
    public TextView tvInverterSpecification;
    @BindView(R.id.tvSensorName)
    public TextView tvSensorName;

    public SystemDetailsChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
