package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aniket on 11/7/19.
 */

public class EpcUserChildViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvLoginId)
    public TextView tvLoginId;
    @BindView(R.id.tvPlantCapacityEPC)
    public TextView tvPlantCapacityEPC;
    @BindView(R.id.tvCurrentPowerEPC)
    public TextView tvCurrentPowerEPC;
    @BindView(R.id.tvTodaysGeneration)
    public TextView tvTodaysGeneration;
    @BindView(R.id.tvMonthGeneration)
    public TextView tvMonthGeneration;
    @BindView(R.id.tvTotalGeneration)
    public TextView tvTotalGeneration;
    @BindView(R.id.tvPlantStatus)
    public TextView tvPlantStatus;

    public EpcUserChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
