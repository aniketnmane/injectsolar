package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

/**
 * Created by Aniket on 11/12/2019.
 */

public class SectionViewHolder extends RecyclerView.ViewHolder {

    public TextView tvInverterNo;
    public SectionViewHolder(View itemView) {
        super(itemView);
         tvInverterNo = (TextView) itemView.findViewById(R.id.tv_Inverter);
    }
}
