package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aniket on 11/7/19.
 */

public class ChildViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_MPPT)
    public TextView tv_mmpt;
    @BindView(R.id.tv_V)
    public TextView tv_v;
    @BindView(R.id.tv_I)
    public TextView tv_i;
    @BindView(R.id.tv_P)
    public TextView tv_p;

    public ChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
