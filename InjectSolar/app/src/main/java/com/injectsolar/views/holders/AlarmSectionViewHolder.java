package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Aniket on 11/12/2019.
 */

public class AlarmSectionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_device_inverter)
    public TextView tv_device_inverter;
    public AlarmSectionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
