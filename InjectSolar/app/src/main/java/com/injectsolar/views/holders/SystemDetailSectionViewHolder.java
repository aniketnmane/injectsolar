package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by apple on 11/7/16.
 */

public class SystemDetailSectionViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvSensorNameLbl)
    public TextView tvSensorNameLbl;
    public SystemDetailSectionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
