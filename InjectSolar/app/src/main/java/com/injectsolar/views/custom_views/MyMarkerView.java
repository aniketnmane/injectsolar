
package com.injectsolar.views.custom_views;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.injectsolar.R;

/**
 * Custom implementation of the MarkerView.
 *
 * @author Philipp Jahoda
 */
public class MyMarkerView extends MarkerView {

    private TextView tvContent;

    public MyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        String data = e.getData().toString();
        String[] dataArr = data.split(" ");
        String entryValue = dataArr[0];
        String entryUnit = dataArr[1];
        if (e instanceof CandleEntry) {
            CandleEntry ce = (CandleEntry) e;
            tvContent.setText(String.format("%s KW", Utils.formatNumber(ce.getHigh(), 0, true)));
        } else {
            if (dataArr.length > 2)
                tvContent.setText(String.format("%s - %s %s%s", entryValue, Utils.formatNumber(e.getY(), 0, true), entryUnit, dataArr[2]));
            else
                tvContent.setText(String.format("%s - %s %s", entryValue, Utils.formatNumber(e.getY(), 0, true), entryUnit));
        }

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
