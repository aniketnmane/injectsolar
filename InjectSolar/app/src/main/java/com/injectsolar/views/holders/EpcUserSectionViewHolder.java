package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by apple on 11/7/16.
 */

public class EpcUserSectionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvLoginId)
    protected TextView tvLoginId;
    @BindView(R.id.tvPlantCapacityEPC)
    protected TextView tvPlantCapacityEPC;
    @BindView(R.id.tvCurrentPowerEPC)
    protected TextView tvCurrentPowerEPC;
    @BindView(R.id.tvTodaysGeneration)
    protected TextView tvTodaysGeneration;
    @BindView(R.id.tvMonthGeneration)
    protected TextView tvMonthGeneration;
    @BindView(R.id.tvTotalGeneration)
    protected TextView tvTotalGeneration;
    @BindView(R.id.tvPlantStatus)
    protected TextView tvPlantStatus;

    public EpcUserSectionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
