package com.injectsolar.views.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.injectsolar.R;
import com.injectsolar.application.ApplicationController;

import java.time.format.TextStyle;


public class CustomTextView extends AppCompatTextView {
    private int fontStyle;
    private TypedArray a = null;
    boolean fit = false;

    public CustomTextView(Context context) {
        super(context);
        init(null, 1);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 1);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        try {
            this.a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView, defStyle, 0);
            this.fontStyle = a.getInteger(R.styleable.CustomTextView_customfontStyle, 0);
        } finally {
            if (this.a != null)
                this.a.recycle();
        }

        if (!isInEditMode()) {
            switch (fontStyle) {
                case 1:
                    setTypeface(ApplicationController.fntLatoBold);
                    break;
                case 2:
                    setTypeface(ApplicationController.fntLatoRegular);
                    break;
                case 3:
                    setTypeface(ApplicationController.fntMontserratBold);
                    break;
                case 4:
                    setTypeface(ApplicationController.fntMontserratRegular);
                    break;
              /*  default:
                    setTypeface(ApplicationController.fntLatoRegular);
                    break;*/
            }
        }
    }
}
