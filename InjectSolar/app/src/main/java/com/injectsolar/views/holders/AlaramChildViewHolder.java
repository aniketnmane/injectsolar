package com.injectsolar.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by apple on 11/7/16.
 */

public class AlaramChildViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_item_device_inverter)
    public TextView tv_item_device_inverter;
    @BindView(R.id.tv_item_alarm_code)
    public TextView tv_item_alarm_code;
    @BindView(R.id.tv_item_alarm_message)
    public TextView tv_item_alarm_message;
    @BindView(R.id.tv_item_occurrence_time)
    public TextView tv_item_occurrence_time;
    @BindView(R.id.tv_item_clearance_time)
    public TextView tv_item_alaram;

    public AlaramChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
