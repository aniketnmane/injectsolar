package com.injectsolar.views.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.widget.AppCompatButton;

import com.injectsolar.R;
import com.injectsolar.application.ApplicationController;


public class CustomButton extends AppCompatButton {
    private static final String TAG = CustomButton.class.getSimpleName();
    private int fontStyle;
    private TypedArray a = null;

    public CustomButton(Context context) {
        super(context);
        init(null, 0);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        try {
            this.a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView, defStyle, 0);
            this.fontStyle = a.getInteger(R.styleable.CustomTextView_customfontStyle, 1);
            Log.d(TAG, "fontStyle: " + this.fontStyle);
        } finally {
            if (this.a != null)
                this.a.recycle();
        }

        setAllCaps(false);

        setBackgroundResource(R.drawable.button_rounded_corners_white_bg);
        if (!isInEditMode()) {
            switch (fontStyle) {
                case 1:
                    setTypeface(ApplicationController.fntLatoBold);
                    break;
                case 2:
                    setTypeface(ApplicationController.fntLatoRegular);
                    break;
                case 3:
                    setTypeface(ApplicationController.fntMontserratBold);
                    break;
                case 4:
                    setTypeface(ApplicationController.fntMontserratRegular);
                    break;
                default:
                    setTypeface(ApplicationController.fntMontserratRegular);
                    break;
            }
        }
    }
}
