package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class SystemDetails {
    @SerializedName("system_size")
    private String system_size;
    @SerializedName("customer_name")
    private String customer_name;
    @SerializedName("solar_panel")
    private String solar_panel;
    @SerializedName("address")
    private String address;
    @SerializedName("solar_inverter")
    private String solar_inverter;
    @SerializedName("contact_detail")
    private String contact_detail;
    @SerializedName("num_of_devices")
    private String num_of_devices;
    @SerializedName("elec_rate")
    private String elec_rate;
    @SerializedName("num_inverter")
    private String num_inverter;

    public String getSystem_size() {
        return system_size;
    }

    public void setSystem_size(String system_size) {
        this.system_size = system_size;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getSolar_panel() {
        return solar_panel;
    }

    public void setSolar_panel(String solar_panel) {
        this.solar_panel = solar_panel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSolar_inverter() {
        return solar_inverter;
    }

    public void setSolar_inverter(String solar_inverter) {
        this.solar_inverter = solar_inverter;
    }

    public String getContact_detail() {
        return contact_detail;
    }

    public void setContact_detail(String contact_detail) {
        this.contact_detail = contact_detail;
    }

    public String getNum_of_devices() {
        return num_of_devices;
    }

    public void setNum_of_devices(String num_of_devices) {
        this.num_of_devices = num_of_devices;
    }

    public String getElec_rate() {
        return elec_rate;
    }

    public void setElec_rate(String elec_rate) {
        this.elec_rate = elec_rate;
    }

    public String getNum_inverter() {
        return num_inverter;
    }

    public void setNum_inverter(String num_inverter) {
        this.num_inverter = num_inverter;
    }
}
