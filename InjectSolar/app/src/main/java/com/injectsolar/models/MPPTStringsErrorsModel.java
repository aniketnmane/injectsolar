package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MPPTStringsErrorsModel {

    @SerializedName("device")
    private String device;

    @SerializedName("inverters")
    private  ArrayList<MPPTStringsInverterModel> inverters;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public ArrayList<MPPTStringsInverterModel> getInverters() {
        return inverters;
    }

    public void setInverters(ArrayList<MPPTStringsInverterModel> inverters) {
        this.inverters = inverters;
    }
}
