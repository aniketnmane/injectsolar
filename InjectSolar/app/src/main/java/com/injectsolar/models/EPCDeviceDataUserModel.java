package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class EPCDeviceDataUserModel {
    @SerializedName("id")
    String id;
    @SerializedName("role")
    String role;
    @SerializedName("login_id")
    String login_id;
    @SerializedName("plant_capacity")
    String plant_capacity;
    @SerializedName("totalCurrentPower")
    String totalCurrentPower;
    @SerializedName("totalTodayEnergyGenerated")
    String totalTodayEnergyGenerated;
    @SerializedName("totalEnergyGenerated")
    String totalEnergyGenerated;
    @SerializedName("totalMonthlyEnergyGenerated")
    String totalMonthlyEnergyGenerated;
    @SerializedName("irradiation")
    String irradiation;
    @SerializedName("PR")
    String PR;
    @SerializedName("Device_status")
    String Device_status;
    @SerializedName("error_generate")
    String error_generate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLogin_id() {
        return login_id;
    }

    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }

    public String getPlant_capacity() {
        return plant_capacity;
    }

    public void setPlant_capacity(String plant_capacity) {
        this.plant_capacity = plant_capacity;
    }

    public String getTotalCurrentPower() {
        return totalCurrentPower;
    }

    public void setTotalCurrentPower(String totalCurrentPower) {
        this.totalCurrentPower = totalCurrentPower;
    }

    public String getTotalTodayEnergyGenerated() {
        return totalTodayEnergyGenerated;
    }

    public void setTotalTodayEnergyGenerated(String totalTodayEnergyGenerated) {
        this.totalTodayEnergyGenerated = totalTodayEnergyGenerated;
    }

    public String getTotalMonthlyEnergyGenerated() {
        return totalMonthlyEnergyGenerated;
    }

    public void setTotalMonthlyEnergyGenerated(String totalMonthlyEnergyGenerated) {
        this.totalMonthlyEnergyGenerated = totalMonthlyEnergyGenerated;
    }

    public String getIrradiation() {
        return irradiation;
    }

    public void setIrradiation(String irradiation) {
        this.irradiation = irradiation;
    }

    public String getPR() {
        return PR;
    }

    public void setPR(String PR) {
        this.PR = PR;
    }

    public String getDevice_status() {
        return Device_status;
    }

    public void setDevice_status(String device_status) {
        Device_status = device_status;
    }

    public String getError_generate() {
        return error_generate;
    }

    public void setError_generate(String error_generate) {
        this.error_generate = error_generate;
    }

    public String getTotalEnergyGenerated() {
        return totalEnergyGenerated;
    }

    public void setTotalEnergyGenerated(String totalEnergyGenerated) {
        this.totalEnergyGenerated = totalEnergyGenerated;
    }
}
