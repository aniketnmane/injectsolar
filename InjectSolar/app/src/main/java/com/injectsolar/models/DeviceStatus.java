package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class DeviceStatus {

    @SerializedName("device_id")
    int device_id;
    @SerializedName("dev_name")
    String dev_name;
    @SerializedName("deviceStatus")
    String deviceStatus;

    public int getDevice_id() {
        return device_id;
    }

    public void setDevice_id(int device_id) {
        this.device_id = device_id;
    }

    public String getDev_name() {
        return dev_name;
    }

    public void setDev_name(String dev_name) {
        this.dev_name = dev_name;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }
}
