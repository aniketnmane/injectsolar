package com.injectsolar.models;



public class GetLoginData  {
    private static GetLoginData instance;
    public static GetLoginData getInstance() {
        if (instance == null) {
            synchronized (String.class) {
                instance = new GetLoginData();
            }
        }
        return instance;
    }

}
