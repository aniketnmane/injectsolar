package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class SensorInfo {
    /*"id": "43",
            "sensor_id": "6",
            "user_id": "14",
            "device_id": "21",
            "sub_device_id": "2",
            "param_no": "1",
            "gain": "1.0000000",
            "created_at": "2019-12-26 21:17:01+00",
            "updated_at": "2019-12-27 10:53:03+00",
            "status": "1",
            "user_dev_subdev_mapp_id": null,
            "sensorName": "Wind Direction"*/
    @SerializedName("id")
    private String id;
    @SerializedName("sensor_id")
    private String sensor_id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("device_id")
    private String device_id;
    @SerializedName("sub_device_id")
    private String sub_device_id;
    @SerializedName("param_no")
    private String param_no;
    @SerializedName("gain")
    private String gain;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("status")
    private String status;
    @SerializedName("user_dev_subdev_mapp_id")
    private String user_dev_subdev_mapp_id;
    @SerializedName("sensorName")
    private String sensorName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSensor_id() {
        return sensor_id;
    }

    public void setSensor_id(String sensor_id) {
        this.sensor_id = sensor_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getSub_device_id() {
        return sub_device_id;
    }

    public void setSub_device_id(String sub_device_id) {
        this.sub_device_id = sub_device_id;
    }

    public String getParam_no() {
        return param_no;
    }

    public void setParam_no(String param_no) {
        this.param_no = param_no;
    }

    public String getGain() {
        return gain;
    }

    public void setGain(String gain) {
        this.gain = gain;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_dev_subdev_mapp_id() {
        return user_dev_subdev_mapp_id;
    }

    public void setUser_dev_subdev_mapp_id(String user_dev_subdev_mapp_id) {
        this.user_dev_subdev_mapp_id = user_dev_subdev_mapp_id;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }
}
