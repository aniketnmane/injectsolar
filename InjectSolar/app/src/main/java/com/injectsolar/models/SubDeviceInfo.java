package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SubDeviceInfo {
    @SerializedName("id")
    private String id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("device_id")
    private String device_id;
    @SerializedName("master_dev_name")
    private String master_dev_name;
    @SerializedName("dev_name")
    private String dev_name;
    @SerializedName("serial_no")
    private String serial_no;
    @SerializedName("sub_device_id")
    private String sub_device_id;
    @SerializedName("sub_dev_type_model_mapp_id")
    private String sub_dev_type_model_mapp_id;
    @SerializedName("name")
    private String name;
    @SerializedName("inv_name")
    private String inv_name;
    @SerializedName("installed_capacity")
    private String installed_capacity;
    @SerializedName("spec")
    private String spec;
    @SerializedName("isFaulty")
    private String isFaulty;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("sub_device_type_id")
    private String sub_device_type_id;
    @SerializedName("sub_device_name")
    private String sub_device_name;
    @SerializedName("model_id")
    private String model_id;
    @SerializedName("model_name")
    private String model_name;
    @SerializedName("sensor_info")
    private ArrayList<SensorInfo> sensor_info;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMaster_dev_name() {
        return master_dev_name;
    }

    public void setMaster_dev_name(String master_dev_name) {
        this.master_dev_name = master_dev_name;
    }

    public String getDev_name() {
        return dev_name;
    }

    public void setDev_name(String dev_name) {
        this.dev_name = dev_name;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getSub_device_id() {
        return sub_device_id;
    }

    public void setSub_device_id(String sub_device_id) {
        this.sub_device_id = sub_device_id;
    }

    public String getSub_dev_type_model_mapp_id() {
        return sub_dev_type_model_mapp_id;
    }

    public void setSub_dev_type_model_mapp_id(String sub_dev_type_model_mapp_id) {
        this.sub_dev_type_model_mapp_id = sub_dev_type_model_mapp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInv_name() {
        return inv_name;
    }

    public void setInv_name(String inv_name) {
        this.inv_name = inv_name;
    }

    public String getInstalled_capacity() {
        return installed_capacity;
    }

    public void setInstalled_capacity(String installed_capacity) {
        this.installed_capacity = installed_capacity;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getIsFaulty() {
        return isFaulty;
    }

    public void setIsFaulty(String isFaulty) {
        this.isFaulty = isFaulty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getSub_device_type_id() {
        return sub_device_type_id;
    }

    public void setSub_device_type_id(String sub_device_type_id) {
        this.sub_device_type_id = sub_device_type_id;
    }

    public String getSub_device_name() {
        return sub_device_name;
    }

    public void setSub_device_name(String sub_device_name) {
        this.sub_device_name = sub_device_name;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public ArrayList<SensorInfo> getSensor_info() {
        return sensor_info;
    }

    public void setSensor_info(ArrayList<SensorInfo> sensor_info) {
        this.sensor_info = sensor_info;
    }
}
