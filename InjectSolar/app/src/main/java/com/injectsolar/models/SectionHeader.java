package com.injectsolar.models;


import com.injectsolar.views.custom_views.Section;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class SectionHeader implements Section<MPPTStringsModel>, Comparable<SectionHeader> {

    List<MPPTStringsModel> childList;
    String sectionText;
    int index;

    public SectionHeader(List<MPPTStringsModel> childList, String sectionText, int index) {
        this.childList = childList;
        this.sectionText = sectionText;
        this.index = index;
    }

    @Override
    public List<MPPTStringsModel> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }

    @Override
    public int compareTo(SectionHeader another) {
        if (this.index > another.index) {
            return -1;
        } else {
            return 1;
        }
    }
}