package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DeviceInfo {
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("device_id")
    private String device_id;
    @SerializedName("master_dev_name")
    private String master_dev_name;
    @SerializedName("dev_name")
    private String dev_name;
    @SerializedName("serial_no")
    private String serial_no;
    @SerializedName("isNewDevice")
    private boolean isNewDevice;
    @SerializedName("isRemoveDevice")
    private boolean isRemoveDevice;
    @SerializedName("sub_device_info")
    private ArrayList<SubDeviceInfo> sub_device_info_list;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<SubDeviceInfo> getSub_device_info_list() {
        return sub_device_info_list;
    }

    public void setSub_device_info_list(ArrayList<SubDeviceInfo> sub_device_info_list) {
        this.sub_device_info_list = sub_device_info_list;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMaster_dev_name() {
        return master_dev_name;
    }

    public void setMaster_dev_name(String master_dev_name) {
        this.master_dev_name = master_dev_name;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public boolean isNewDevice() {
        return isNewDevice;
    }

    public void setNewDevice(boolean newDevice) {
        isNewDevice = newDevice;
    }

    public boolean isRemoveDevice() {
        return isRemoveDevice;
    }

    public void setRemoveDevice(boolean removeDevice) {
        isRemoveDevice = removeDevice;
    }

    public String getDev_name() {
        return dev_name;
    }

    public void setDev_name(String dev_name) {
        this.dev_name = dev_name;
    }
}
