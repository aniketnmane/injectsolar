package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class EPCDashboardCountModel {
 /* "totalusers": 30,
          "totaldevices": "69",
          "activedevices": "39",
          "inactivedevices": "30"*/

    @SerializedName("totalusers")
    private String totalusers;
    @SerializedName("totaldevices")
    private String totaldevices;
    @SerializedName("activedevices")
    private String activedevices;
    @SerializedName("inactivedevices")
    private String inactivedevices;

    public String getTotalusers() {
        return totalusers;
    }

    public void setTotalusers(String totalusers) {
        this.totalusers = totalusers;
    }

    public String getTotaldevices() {
        return totaldevices;
    }

    public void setTotaldevices(String totaldevices) {
        this.totaldevices = totaldevices;
    }

    public String getActivedevices() {
        return activedevices;
    }

    public void setActivedevices(String activedevices) {
        this.activedevices = activedevices;
    }

    public String getInactivedevices() {
        return inactivedevices;
    }

    public void setInactivedevices(String inactivedevices) {
        this.inactivedevices = inactivedevices;
    }
}
