package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class EPCDeviceInfo {
    @SerializedName("totalEnergyGenerated")
    private String totalEnergyGenerated;
    @SerializedName("co2EmmisionSaved")
    private String co2EmmisionSaved;
    @SerializedName("totalSolarCapacity")
    private String totalSolarCapacity;
    @SerializedName("totalProject")
    private String totalProject;
    @SerializedName("totalDeviceInstalled")
    private String totalDeviceInstalled;

    public String getTotalSolarCapacity() {
        return totalSolarCapacity;
    }

    public void setTotalSolarCapacity(String totalSolarCapacity) {
        this.totalSolarCapacity = totalSolarCapacity;
    }

    public String getTotalProject() {
        return totalProject;
    }

    public void setTotalProject(String totalProject) {
        this.totalProject = totalProject;
    }

    public String getTotalDeviceInstalled() {
        return totalDeviceInstalled;
    }

    public void setTotalDeviceInstalled(String totalDeviceInstalled) {
        this.totalDeviceInstalled = totalDeviceInstalled;
    }

    public String getTotalEnergyGenerated() {
        return totalEnergyGenerated;
    }

    public void setTotalEnergyGenerated(String totalEnergyGenerated) {
        this.totalEnergyGenerated = totalEnergyGenerated;
    }

    public String getCo2EmmisionSaved() {
        return co2EmmisionSaved;
    }

    public void setCo2EmmisionSaved(String co2EmmisionSaved) {
        this.co2EmmisionSaved = co2EmmisionSaved;
    }
}
