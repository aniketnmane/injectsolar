package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@org.parceler.Parcel(Parcel.Serialization.BEAN)
public class User {
    /*"user_id": "Thane",
            "installed_capacity": "25KW",
            "count_inverter": "1",
            "elec_rate": "8.00",
            "tvInverterNo": null,
            "address": null,
            "contact": null,
            device_serial_no
            "email": null,
            "start_date": "2018-01-20",
            "end_date": "2019-02-14",
            "user_added_on": null,
            "user_epic_id": "inject_solar"


            role": "4",
    "login_id": "unity_test",
    "name": "UNITY INDUSTRIES",
    "email": "rppatil7@gmail.com",
    "password": "c053dd06da6e21356d6a98c2951ea3ef",
    "mobile_no": "8879794953",
    "address": "Karad",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjQzNCIsInJvbGUiOiI0IiwidGltZXN0YW1wIjoxNTc1NDU0NjU1LCJzdGF0dXMiOjF9.W7ZGbszygOoMwTn6H_nlXz05JaZ5QuJfrwqDFU58TRM",
    "created_at": "2019-10-18 11:09:58+00",
    "updated_at": "2019-12-04 10:17:35.915864+00",
    "status": "1",
    "last_login": "2019-12-04 10:17:35.915864+00",
    "device_id": "434",
    "admin_id": "0",
    "epc_id": "419",
    "company_name": "Inject Solar Pvt. Ltd.",
    "website": "http:\/\/injectsolar.com\/",
    "company_email": "rahulpatil@injectsolar.com",
    "contact_name": "Rahul Patil",
    "logo": "https:\/\/injectsolar.s3.amazonaws.com\/epc_logos\/inject_solar-468.png"


    "device_id": "2",
                "admin_id": "1",
                "epc_id": "0",
                "role": "2",
                "login_id": "injectsolar",
                "name": "INJECT SOLAR LLP",
                "email": "rahulpatil@injectsolar.com",
                "password": "084d774e703385eff1a41cfc7502ae6f",
                "mobile_no": "8879794953",
                "address": "Balewadi ,pune",
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJyb2xlIjoiMiIsInRpbWVzdGFtcCI6MTU3OTI1NzcwMiwic3RhdHVzIjoxfQ.v0x0QVjQ-uht5lyRrbY0ywtbe1s9N_tdb8EYApoWwkk",
                "created_at": "2019-12-19 23:55:24+00",
                "updated_at": "2020-01-17 10:41:42.600605+00",
                "last_login": "2020-01-17 10:41:42.600605+00",
                "status": "1",
                "is_active": "1",
                "company_name": "INJECT SOLAR LLP",
                "website": "http://www.injectsolar.in",
                "company_email": "rahulpatil@injectsolar.com",
                "contact_name": "Rahul Patil",
                "logo": "https://injectsolar.s3.amazonaws.com/epc_logos/logo-671.jpg"*/


    public static User instance;
    public String userName;
    @SerializedName("status")
    public String status;

    @SerializedName("is_active")
    public String is_active;

    @SerializedName("website")
    public String website;

    @SerializedName("token")
    public String token;

    @SerializedName("id")
    public String userId;

    @SerializedName("device_id")
    public String device_id;

    @SerializedName("logo")
    public String logo;

    @SerializedName("role")
    public String role;

    @SerializedName("admin_id")
    public String admin_id;

    @SerializedName("login_id")
    public String login_id;

    @SerializedName("device_serial_no")
    public String device_serial_no;

    @SerializedName("installed_capacity")
    public String installed_capacity;

    @SerializedName("count_inverter")
    public String count_inverter;

    @SerializedName("elec_rate")
    public String elec_rate;

    @SerializedName("name")
    public String name;

    @SerializedName("address")
    public String address;

    @SerializedName("contact")
    public String contact;

    @SerializedName("email")
    public String email;

    @SerializedName("start_date")
    public String start_date;

    @SerializedName("end_date")
    public String end_date;

    @SerializedName("user_added_on")
    public String user_added_on;

    @SerializedName("user_epic_id")
    public String user_epic_id;

    @SerializedName("epc_id")
    public String epc_id;

    public String getEpc_id() {
        return epc_id;
    }

    public void setEpc_id(String epc_id) {
        this.epc_id = epc_id;
    }

    public User() {

    }


    /*protected User(Parcel in) {

        token = in.readString();
        userId = in.readString();
        userName= in.readString();
        device_serial_no = in.readString();
        installed_capacity = in.readString();
        count_inverter = in.readString();
        elec_rate = in.readString();
        name = in.readString();
        address = in.readString();
        contact = in.readString();
        email = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        user_added_on = in.readString();
        user_epic_id = in.readString();
        userName = in.readString();
        login_id = in.readString();
        logo = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };*/

    public static User getInstance() {
        if (instance == null) {
            instance = new User();
        }

        return instance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInstalled_capacity() {
        return installed_capacity;
    }

    public void setInstalled_capacity(String installed_capacity) {
        this.installed_capacity = installed_capacity;
    }

    public String getDevice_serial_no() {
        return device_serial_no;
    }

    public void setDevice_serial_no(String device_serial_no) {
        this.device_serial_no = device_serial_no;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getCount_inverter() {
        return count_inverter;
    }

    public void setCount_inverter(String count_inverter) {
        this.count_inverter = count_inverter;
    }

    public String getElec_rate() {
        return elec_rate;
    }

    public void setElec_rate(String elec_rate) {
        this.elec_rate = elec_rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getLogin_id() {
        return login_id;
    }

    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }

    /*public static Creator<User> getCREATOR() {
        return CREATOR;
    }*/

    public String getToken() {
        return token;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getUser_added_on() {
        return user_added_on;
    }

    public void setUser_added_on(String user_added_on) {
        this.user_added_on = user_added_on;
    }

    public String getUser_epic_id() {
        return user_epic_id;
    }

    public void setUser_epic_id(String user_epic_id) {
        this.user_epic_id = user_epic_id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

   /* @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(userId);
        dest.writeString(userName);
        dest.writeString(device_serial_no);
        dest.writeString(installed_capacity);
        dest.writeString(count_inverter);
        dest.writeString(elec_rate);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(contact);
        dest.writeString(email);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeString(user_added_on);
        dest.writeString(user_epic_id);
        dest.writeString(login_id);
        dest.writeString(logo);
    }*/
}
