package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class InverterStatus {

    @SerializedName("id")
    int id;
    @SerializedName("inv_name")
    String inv_name;
    @SerializedName("inv_status")
    String inv_status;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    public String getInvName() {
        return inv_name;
    }

    public void setInvName(String inv_name) {
        this.inv_name = inv_name;
    }

    public String getInvStatus() {
        return inv_status;
    }

    public void setInvStatus(String inv_status) {
        this.inv_status = inv_status;
    }
}
