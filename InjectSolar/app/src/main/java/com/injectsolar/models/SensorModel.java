package com.injectsolar.models;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class SensorModel {
    @SerializedName("pname")
    private String pname;
    @SerializedName("val")
    private String val;
    @SerializedName("unit")
    private String unit;

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
