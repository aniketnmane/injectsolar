package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SystemDetailsResponse {
    @SerializedName("systemDetails")
    private SystemDetails systemDetails;
    @SerializedName("status")
    private String status;
    @SerializedName("device_info")
    private ArrayList<DeviceInfo> device_info;

    public ArrayList<DeviceInfo> getDevice_info() {
        return device_info;
    }

    public void setDevice_info(ArrayList<DeviceInfo> device_info) {
        this.device_info = device_info;
    }


    public SystemDetails getSystemDetails() {
        return systemDetails;
    }

    public void setSystemDetails(SystemDetails systemDetails) {
        this.systemDetails = systemDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
