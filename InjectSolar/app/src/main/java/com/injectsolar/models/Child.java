package com.injectsolar.models;

/**
 * Created by Aniket on 11/7/19.
 */
public class Child {

    String name;

    public Child(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}