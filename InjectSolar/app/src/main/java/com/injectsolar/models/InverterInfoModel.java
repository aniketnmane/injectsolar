package com.injectsolar.models;

import android.os.Parcelable;


import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

public class InverterInfoModel implements Parcelable {
    /*
     "id": "32",
      "user_id": "11",
      "device_id": "17",
      "sub_device_id": "1",
      "sub_dev_type_model_mapp_id": "10",
      "name": "Nazira Guest House RCC Roof(Sungrow 10kw)",
      "inv_name": "INV-1",
      "installed_capacity": "10",
      "isFaulty": "1",
      "status": "1",
      "created_at": "2019-12-26 18:03:59+00",
      "updated_at": "2019-12-27 10:21:34+00",
      "connected_solar_capacity": "10.00",
      "spec": "Sungrow 10kw",
      "serial_no": "864502037705878"
*/

    @SerializedName("id")
    String id;
    @SerializedName("user_id")
    String user_id;
    @SerializedName("device_id")
    String device_id;
    @SerializedName("sub_device_id")
    String sub_device_id;
    @SerializedName("sub_dev_type_model_mapp_id")
    String sub_dev_type_model_mapp_id;
    @SerializedName("name")
    String name;
    @SerializedName("inv_name")
    String inv_name;
    @SerializedName("installed_capacity")
    String installed_capacity;
    @SerializedName("isFaulty")
    String isFaulty;
    @SerializedName("status")
    String status;
    @SerializedName("created_at")
    String created_at;
    @SerializedName("updated_at")
    String updated_at;
    @SerializedName("connected_solar_capacity")
    String connected_solar_capacity;
    @SerializedName("spec")
    String spec;
    @SerializedName("serial_no")
    String serial_no;

    protected InverterInfoModel(android.os.Parcel in) {
        id = in.readString();
        user_id = in.readString();
        device_id = in.readString();
        sub_device_id = in.readString();
        sub_dev_type_model_mapp_id = in.readString();
        name = in.readString();
        inv_name = in.readString();
        installed_capacity = in.readString();
        isFaulty = in.readString();
        status = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        connected_solar_capacity = in.readString();
        spec = in.readString();
        serial_no = in.readString();
    }

    public static final Creator<InverterInfoModel> CREATOR = new Creator<InverterInfoModel>() {
        @Override
        public InverterInfoModel createFromParcel(android.os.Parcel in) {
            return new InverterInfoModel(in);
        }

        @Override
        public InverterInfoModel[] newArray(int size) {
            return new InverterInfoModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getSub_device_id() {
        return sub_device_id;
    }

    public void setSub_device_id(String sub_device_id) {
        this.sub_device_id = sub_device_id;
    }

    public String getSub_dev_type_model_mapp_id() {
        return sub_dev_type_model_mapp_id;
    }

    public void setSub_dev_type_model_mapp_id(String sub_dev_type_model_mapp_id) {
        this.sub_dev_type_model_mapp_id = sub_dev_type_model_mapp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInv_name() {
        return inv_name;
    }

    public void setInv_name(String inv_name) {
        this.inv_name = inv_name;
    }

    public String getInstalled_capacity() {
        return installed_capacity;
    }

    public void setInstalled_capacity(String installed_capacity) {
        this.installed_capacity = installed_capacity;
    }

    public String getIsFaulty() {
        return isFaulty;
    }

    public void setIsFaulty(String isFaulty) {
        this.isFaulty = isFaulty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getConnected_solar_capacity() {
        return connected_solar_capacity;
    }

    public void setConnected_solar_capacity(String connected_solar_capacity) {
        this.connected_solar_capacity = connected_solar_capacity;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(device_id);
        dest.writeString(sub_device_id);
        dest.writeString(sub_dev_type_model_mapp_id);
        dest.writeString(name);
        dest.writeString(inv_name);
        dest.writeString(installed_capacity);
        dest.writeString(isFaulty);
        dest.writeString(status);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(connected_solar_capacity);
        dest.writeString(spec);
        dest.writeString(serial_no);
    }
}
