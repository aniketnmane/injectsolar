package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by aniket on 27/01/2018.
 */

public class InverterDataModel {
    /*
            "inv_no": "3",
            "inv_name": "Main STP Govindpuram(Powerone)",
            "capacity": "50",
            "current_power": "0",
            "today_generation": "112",
            "month_generation": "788",
            "total_generation": "70191"
    */
    @SerializedName("subdevice_id")
    private String deviceInverterName;
    private String deviceStatus;
    @SerializedName("capacity")
    private String capacity;
    @SerializedName("inv_name")
    private String inv_name;
    @SerializedName("inv_no")
    private String inv_no;
    @SerializedName("current_power")
    private String currentPower;
    @SerializedName("today_energy")
    private String todaysEnergy;
    @SerializedName("today_generation")
    private String todayGeneration;
    @SerializedName("total_generation")
    private String totalGeneration;
    @SerializedName("month_generation")
    private String monthGeneration;
    @SerializedName("life_time_energy")
    private String lifetimeEnergy;

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getInv_name() {
        return inv_name;
    }

    public void setInv_name(String inv_name) {
        this.inv_name = inv_name;
    }

    public String getInv_no() {
        return inv_no;
    }

    public void setInv_no(String inv_no) {
        this.inv_no = inv_no;
    }

    public String getTodayGeneration() {
        return todayGeneration;
    }

    public void setTodayGeneration(String todayGeneration) {
        this.todayGeneration = todayGeneration;
    }

    public String getTotalGeneration() {
        return totalGeneration;
    }

    public void setTotalGeneration(String totalGeneration) {
        this.totalGeneration = totalGeneration;
    }

    public String getMonthGeneration() {
        return monthGeneration;
    }

    public void setMonthGeneration(String monthGeneration) {
        this.monthGeneration = monthGeneration;
    }

    public String getInverter_efficiency() {
        return inverter_efficiency;
    }

    public void setInverter_efficiency(String inverter_efficiency) {
        this.inverter_efficiency = inverter_efficiency;
    }

    @SerializedName("inverter_efficiency")
    private String inverter_efficiency;
    private String deviceSerialNo;

    public String getDeviceSerialNo() {
        return deviceSerialNo;
    }

    public void setDeviceSerialNo(String deviceSerialNo) {
        this.deviceSerialNo = deviceSerialNo;
    }

    public String getDeviceInverterName() {
        return deviceInverterName;
    }

    public void setDeviceInverterName(String deviceInverterName) {
        this.deviceInverterName = deviceInverterName;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getCurrentPower() {
        return currentPower;
    }

    public void setCurrentPower(String currentPower) {
        this.currentPower = currentPower;
    }

    public String getTodaysEnergy() {
        return todaysEnergy;
    }

    public void setTodaysEnergy(String todaysEnergy) {
        this.todaysEnergy = todaysEnergy;
    }

    public String getLifetimeEnergy() {
        return lifetimeEnergy;
    }

    public void setLifetimeEnergy(String lifetimeEnergy) {
        this.lifetimeEnergy = lifetimeEnergy;
    }


}
