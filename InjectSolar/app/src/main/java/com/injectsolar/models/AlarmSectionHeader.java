package com.injectsolar.models;


import com.google.gson.annotations.SerializedName;
import com.injectsolar.views.custom_views.Section;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class AlarmSectionHeader implements Section<AlarmsModel>, Comparable<AlarmSectionHeader> {

    @SerializedName("resultObject")
    List<AlarmsModel> childList;
    private String sectionText;
    private int index;

    public AlarmSectionHeader(List<AlarmsModel> childList, String sectionText, int index) {
        this.childList = childList;
        this.sectionText = sectionText;
        this.index = index;
    }

    @Override
    public List<AlarmsModel> getChildItems() {
        return childList;
    }

    @Override
    public int compareTo(AlarmSectionHeader another) {
        if (this.index > another.index) {
            return -1;
        } else {
            return 1;
        }
    }
}