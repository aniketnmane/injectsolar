package com.injectsolar.models;


import com.google.gson.annotations.SerializedName;
import com.injectsolar.views.custom_views.Section;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class SystemDetailsSectionHeader implements Section<DeviceInfo>, Comparable<SystemDetailsSectionHeader> {
    @SerializedName("resultObject")
    private List<DeviceInfo> childList;

    public List<DeviceInfo> getChildList() {
        return childList;
    }

    public void setChildList(List<DeviceInfo> childList) {
        this.childList = childList;
    }

    private String sectionText;
    private int index;

    public SystemDetailsSectionHeader(List<DeviceInfo> childList, String sectionText, int index) {
        this.childList = childList;
        this.sectionText = sectionText;
        this.index = index;
    }

    @Override
    public List<DeviceInfo> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }

    @Override
    public int compareTo(SystemDetailsSectionHeader another) {
        if (this.index > another.index) {
            return -1;
        } else {
            return 1;
        }
    }
}