package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class ParameterModel {
    /*  "param_no": "01",
              "p_name": "Input DC - P1",
              "p_value": 0*/
    @SerializedName("param_no")
    private String param_no;
    @SerializedName("p_name")
    private String p_name;
    @SerializedName("p_value")
    private String p_value;

    public String getParam_no() {
        return param_no;
    }

    public void setParam_no(String param_no) {
        this.param_no = param_no;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_value() {
        return p_value;
    }

    public void setP_value(String p_value) {
        this.p_value = p_value;
    }
}
