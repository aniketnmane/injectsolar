package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class MPPTStringsModel {

    /*
    "serial_no": "864502037741014",
			"sub_deviceid": "1",
			"V": 0,
			"I": 0,
			"P": 0,
			"make_id": "100"
			*/
    @SerializedName("serial_no")
    private String serial_no;
    @SerializedName("sub_deviceid")
    private String sub_deviceid;
    @SerializedName("v")
    private String v;
    @SerializedName("i")
    private String i;
    @SerializedName("p")
    private String p;
    @SerializedName("make_id")
    private String make_id;
    @SerializedName("string")
    private String string;

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getSub_deviceid() {
        return sub_deviceid;
    }

    public void setSub_deviceid(String sub_deviceid) {
        this.sub_deviceid = sub_deviceid;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getI() {
        return i;
    }

    public void setI(String i) {
        this.i = i;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getMake_id() {
        return make_id;
    }

    public void setMake_id(String make_id) {
        this.make_id = make_id;
    }
}
