package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MPPTStringsInverterModel {

    @SerializedName("inv_id")
    private String inverter_no;

    @SerializedName("mppt")
    private ArrayList<MPPTStringsModel> mppt;

    @SerializedName("String")
    private ArrayList<MPPTStringsModel> strings;


    public ArrayList<MPPTStringsModel> getStrings() {
        return strings;
    }

    public void setStrings(ArrayList<MPPTStringsModel> strings) {
        this.strings = strings;
    }

    public String getInverter_no() {
        return inverter_no;
    }

    public void setInverter_no(String inverter_no) {
        this.inverter_no = inverter_no;
    }

    public ArrayList<MPPTStringsModel> getMppt() {
        return mppt;
    }

    public void setMppt(ArrayList<MPPTStringsModel> mppt) {
        this.mppt = mppt;
    }
}
