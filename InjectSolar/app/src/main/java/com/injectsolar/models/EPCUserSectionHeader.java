package com.injectsolar.models;


import com.google.gson.annotations.SerializedName;
import com.injectsolar.views.custom_views.Section;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class EPCUserSectionHeader implements Section<EPCDeviceDataUserModel>, Comparable<EPCUserSectionHeader> {
    @SerializedName("resultObject")
    private
    List<EPCDeviceDataUserModel> childList;

    public List<EPCDeviceDataUserModel> getChildList() {
        return childList;
    }

    public void setChildList(List<EPCDeviceDataUserModel> childList) {
        this.childList = childList;
    }

    private String sectionText;
    private int index;

    public EPCUserSectionHeader(List<EPCDeviceDataUserModel> childList, String sectionText, int index) {
        this.childList = childList;
        this.sectionText = sectionText;
        this.index = index;
    }

    @Override
    public List<EPCDeviceDataUserModel> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }

    @Override
    public int compareTo(EPCUserSectionHeader another) {
        if (this.index > another.index) {
            return -1;
        } else {
            return 1;
        }
    }
}