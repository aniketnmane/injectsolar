package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class AlarmsModel {
    /*"dev_name": "DEV-1",
            "name": "INV-1",
            "inv_name": "INV-1",
            "alarm_id": "E178",
            "date_time": "2020-01-29 15:41:58",
            "clear_time": "2020-01-29 16:48:46",
            "alarm_msg": "Abnormal Grid Volt"*/
    @SerializedName("dev_name")
    private String dev_name;
    @SerializedName("name")
    private String name;
    @SerializedName("inv_name")
    private String inv_name;
    @SerializedName("alarm_id")
    private String alarm_id;
    @SerializedName("date_time")
    private String date_time;
    @SerializedName("clear_time")
    private String clear_time;
    @SerializedName("alarm_msg")
    private String alarm_msg;

    public String getDev_name() {
        return dev_name;
    }

    public void setDev_name(String dev_name) {
        this.dev_name = dev_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInv_name() {
        return inv_name;
    }

    public void setInv_name(String inv_name) {
        this.inv_name = inv_name;
    }

    public String getAlarm_id() {
        return alarm_id;
    }

    public void setAlarm_id(String alarm_id) {
        this.alarm_id = alarm_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getClear_time() {
        return clear_time;
    }

    public void setClear_time(String clear_time) {
        this.clear_time = clear_time;
    }

    public String getAlarm_msg() {
        return alarm_msg;
    }

    public void setAlarm_msg(String alarm_msg) {
        this.alarm_msg = alarm_msg;
    }
}
