package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class ErrorLogsModel {

    @SerializedName("err_no")
    String errorNo;
    @SerializedName("make_id")
    String makeId;
    @SerializedName("param_no")
    String paramNo;
    @SerializedName("param_bit")
    String paramBit;
    @SerializedName("param_value")
    String paramValue;
    @SerializedName("alarm_name")
    String alarmName;
    @SerializedName("alarm_msg")
    String alarmMsg;
    @SerializedName("received_at")
    String received_at;
    @SerializedName("serial_no")
    String serial_no;
    @SerializedName("sub_deviceid")
    String sub_deviceid;

    public String getErrorNo() {
        return errorNo;
    }

    public void setErrorNo(String errorNo) {
        this.errorNo = errorNo;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getParamNo() {
        return paramNo;
    }

    public void setParamNo(String paramNo) {
        this.paramNo = paramNo;
    }

    public String getParamBit() {
        return paramBit;
    }

    public void setParamBit(String paramBit) {
        this.paramBit = paramBit;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getAlarmName() {
        return alarmName;
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
    }

    public String getAlarmMsg() {
        return alarmMsg;
    }

    public void setAlarmMsg(String alarmMsg) {
        this.alarmMsg = alarmMsg;
    }

    public String getReceived_at() {
        return received_at;
    }

    public void setReceived_at(String received_at) {
        this.received_at = received_at;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getSub_deviceid() {
        return sub_deviceid;
    }

    public void setSub_deviceid(String sub_deviceid) {
        this.sub_deviceid = sub_deviceid;
    }
}
