package com.injectsolar.models;

public class SystemDetailsUIModel {
    private String deviceNameSrNo;
    private String inverterNo;
    private String inverterName;
    private String inverterCapacity;
    private String inverterSpecification;
    private String sensorName="";

    public String getDeviceNameSrNo() {
        return deviceNameSrNo;
    }

    public void setDeviceNameSrNo(String deviceNameSrNo) {
        this.deviceNameSrNo = deviceNameSrNo;
    }

    public String getInverterNo() {
        return inverterNo;
    }

    public void setInverterNo(String inverterNo) {
        this.inverterNo = inverterNo;
    }

    public String getInverterName() {
        return inverterName;
    }

    public void setInverterName(String inverterName) {
        this.inverterName = inverterName;
    }

    public String getInverterCapacity() {
        return inverterCapacity;
    }

    public void setInverterCapacity(String inverterCapacity) {
        this.inverterCapacity = inverterCapacity;
    }

    public String getInverterSpecification() {
        return inverterSpecification;
    }

    public void setInverterSpecification(String inverterSpecification) {
        this.inverterSpecification = inverterSpecification;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }
}
