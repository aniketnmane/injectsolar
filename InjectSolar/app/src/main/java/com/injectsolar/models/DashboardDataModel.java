package com.injectsolar.models;

import com.google.gson.annotations.SerializedName;

public class DashboardDataModel {

    @SerializedName("instd_capacity")
    private String instd_capacity;
    @SerializedName("totalEnergyGenerated")
    private String totalEnergyGenerated;
    @SerializedName("totalCurrentPower")
    private String totalCurrentPower;
    @SerializedName("estimated_saving")
    private String estimated_saving;
    @SerializedName("totalTodaysPeakPower")
    private String totalTodaysPeakPower;
    @SerializedName("co2EmmisionSaved")
    private String co2EmmisionSaved;
    @SerializedName("totalTodayEnergyGenerated")
    private String totalTodayEnergyGenerated;
    @SerializedName("totalMonthlyEnergyGenerated")
    private String totalMonthlyEnergyGenerated;
    @SerializedName("irradiance")
    private String irradiance;
    @SerializedName("ambientTemp")
    private String ambientTemp;
    @SerializedName("irradiation")
    private String irradiation;
    @SerializedName("PR")
    private String PR;
    @SerializedName("CUF")
    private String CUF;

    public String getIrradiance() {
        return irradiance;
    }

    public void setIrradiance(String irradiance) {
        this.irradiance = irradiance;
    }

    public String getAmbientTemp() {
        return ambientTemp;
    }

    public void setAmbientTemp(String ambientTemp) {
        this.ambientTemp = ambientTemp;
    }

    public String getIrradiation() {
        return irradiation;
    }

    public void setIrradiation(String irradiation) {
        this.irradiation = irradiation;
    }

    public String getPR() {
        return PR;
    }

    public void setPR(String PR) {
        this.PR = PR;
    }

    public String getCUF() {
        return CUF;
    }

    public void setCUF(String CUF) {
        this.CUF = CUF;
    }

    public String getInstd_capacity() {
        return instd_capacity;
    }

    public void setInstd_capacity(String instd_capacity) {
        this.instd_capacity = instd_capacity;
    }

    public String getTotalEnergyGenerated() {
        return totalEnergyGenerated;
    }

    public void setTotalEnergyGenerated(String totalEnergyGenerated) {
        this.totalEnergyGenerated = totalEnergyGenerated;
    }

    public String getTotalCurrentPower() {
        return totalCurrentPower;
    }

    public void setTotalCurrentPower(String totalCurrentPower) {
        this.totalCurrentPower = totalCurrentPower;
    }

    public String getEstimated_saving() {
        return estimated_saving;
    }

    public void setEstimated_saving(String estimated_saving) {
        this.estimated_saving = estimated_saving;
    }

    public String getTotalTodaysPeakPower() {
        return totalTodaysPeakPower;
    }

    public void setTotalTodaysPeakPower(String totalTodaysPeakPower) {
        this.totalTodaysPeakPower = totalTodaysPeakPower;
    }

    public String getCo2EmmisionSaved() {
        return co2EmmisionSaved;
    }

    public void setCo2EmmisionSaved(String co2EmmisionSaved) {
        this.co2EmmisionSaved = co2EmmisionSaved;
    }

    public String getTotalTodayEnergyGenerated() {
        return totalTodayEnergyGenerated;
    }

    public void setTotalTodayEnergyGenerated(String totalTodayEnergyGenerated) {
        this.totalTodayEnergyGenerated = totalTodayEnergyGenerated;
    }

    public String getTotalMonthlyEnergyGenerated() {
        return totalMonthlyEnergyGenerated;
    }

    public void setTotalMonthlyEnergyGenerated(String totalMonthlyEnergyGenerated) {
        this.totalMonthlyEnergyGenerated = totalMonthlyEnergyGenerated;
    }
}
