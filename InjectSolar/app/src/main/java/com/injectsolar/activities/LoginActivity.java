package com.injectsolar.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.injectsolar.R;
import com.injectsolar.fragments.HomeFragment;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.utils.SharedSharedPreference;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, NetworkCallback {
    @BindView(R.id.tvRegisterHere)
    TextView tvRegisterHere;
    @BindView(R.id.btn_login)
    Button btnLogin;
    private String TAG = "";
    String tag_json_obj = "json_req";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        // setTitleActionBar(getString(R.string.app_name));
        setClickListeners();


    }

    protected void setTitleActionBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
    }

    private void setClickListeners() {
        tvRegisterHere.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvRegisterHere:
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_login:
                JSONObject objectMain = new JSONObject();
                String userName = ((EditText) findViewById(R.id.et_user_name)).getText().toString();
                String password = ((EditText) findViewById(R.id.et_password)).getText().toString();
                if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password)) {
                    try {
                        objectMain.put("login_id", ((EditText) findViewById(R.id.et_user_name)).getText().toString());
                        objectMain.put("password", password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    NetworkCommunication.getInstance().setUseNewUrl(true);
                    NetworkCommunication.getInstance().connect(LoginActivity.this, objectMain, "admin/Admin/login", Request.Method.POST, tag_json_obj, LoginActivity.this, 100, true, false, null);
                } else {
                    Toast.makeText(LoginActivity.this, "User Name & Password should not be blank", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        User user;
        try {
            SharedSharedPreference.getInstance(getApplicationContext()).putUserDetails(((EditText) findViewById(R.id.et_user_name)).getText().toString(), ((EditText) findViewById(R.id.et_password)).getText().toString());
            user = NetworkCommunication.getInstance().gson.fromJson(object.getJSONObject("resultObject").toString(), User.class);
            user.setUserName(((EditText) findViewById(R.id.et_user_name)).getText().toString());
            boolean isHomeScreen = false;
            if (user.getRole().equalsIgnoreCase(HomeFragment.EPC_USER))
                isHomeScreen = true;
            Intent intent = new Intent(LoginActivity.this, NavigationDrawerMainActivity.class);
            intent.putExtra("User", Parcels.wrap(user));
            intent.putExtra("IS_NORMAL_USER", isHomeScreen);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(LoginActivity.this, "Invalid Credentials ", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(int requestCode) {
        Toast.makeText(LoginActivity.this, "Invalid Credentials ", Toast.LENGTH_LONG).show();
    }
}
