package com.injectsolar.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.adapter.TabAdapter;
import com.injectsolar.fragments.EPCNormalUserFragment;
import com.injectsolar.fragments.EPCWMSUserFragment;
import com.injectsolar.models.EPCDashboardCountModel;
import com.injectsolar.models.EPCDeviceInfo;
import com.injectsolar.models.InverterInfoModel;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EPCUserActivity extends BaseActivity implements NetworkCallback, AdapterView.OnItemSelectedListener{
    @BindView(R.id.tabLayout)
    protected TabLayout tabLayout;
    @BindView(R.id.viewPager)
    protected ViewPager viewPager;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    private String tag_json_obj = "EPCUser";
    private ArrayList<InverterInfoModel> inverterInfoModelList;
    @BindView(R.id.tvNoOfProjectsValue)
    protected TextView tvNoOfProjectsValue;
    @BindView(R.id.tvTotalDevicesValue)
    protected TextView tvTotalDevicesValue;
    @BindView(R.id.tvTotalSolarCapcityValue)
    protected TextView tvTotalSolarCapcityValue;
    @BindView(R.id.tvActiveDevicesValue)
    protected TextView tvActiveDevicesValue;
    @BindView(R.id.tvInActiveDevicesValue)
    protected TextView tvInActiveDevicesValue;
    @BindView(R.id.tvTotalCO2EmmisionValue)
    protected TextView tvTotalCO2EmmisionValue;
    @BindView(R.id.tvTotalGeneratedEnergyValue)
    protected TextView tvTotalGeneratedEnergyValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_epc_user);
        if (user == null)
            user = Parcels.unwrap(getIntent().getParcelableExtra("User"));
        ButterKnife.bind(this);
        getDashboardData();
    }

    private void getDashboardData() {
        JSONObject object = new JSONObject();
        String url = "epc/Epc/getDashboardData";
        NetworkCommunication.getInstance().connect(this, object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.EPC_DASHBOARD_COUNT, true, true, user.getToken());
    }

    private void getDashboardLowerCount() {
        JSONObject object = new JSONObject();
        String url = "epc/Epc/totalEnergyGeneratedEPC";
        NetworkCommunication.getInstance().connect(this, object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.EPC_DASHBOARD_LOWER_COUNT, true, true, user.getToken());
    }

    private void getDashboardDeviceInfo() {
        JSONObject object = new JSONObject();
        String url = "epc/Epc/getDeviceInfo";
        NetworkCommunication.getInstance().connect(EPCUserActivity.this, object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.EPC_DEVICE_INFO, true, true, user.getToken());
    }

    private void setDashboardCount(EPCDashboardCountModel model) {
        tvNoOfProjectsValue.setText(model.getTotalusers());
        tvTotalDevicesValue.setText(model.getTotaldevices());
        tvActiveDevicesValue.setText(model.getActivedevices());
        tvInActiveDevicesValue.setText(model.getInactivedevices());
    }

    private void setDashboardLowerCount(EPCDeviceInfo model) {
        String totalDeviceValue = model.getTotalEnergyGenerated() == null ? "" : model.getTotalEnergyGenerated();
        if (!totalDeviceValue.isEmpty())
            tvTotalGeneratedEnergyValue.setText(model.getTotalEnergyGenerated());
        String solarCapacity = model.getTotalSolarCapacity() == null ? "" : model.getTotalSolarCapacity();
        if (!solarCapacity.isEmpty())
            tvTotalSolarCapcityValue.setText(model.getTotalSolarCapacity());
        String co2Emission = model.getCo2EmmisionSaved() == null ? "" : model.getCo2EmmisionSaved();
        if (!co2Emission.isEmpty())
            tvTotalCO2EmmisionValue.setText(model.getCo2EmmisionSaved());
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        try {
            switch (requestCode) {
                case NetworkConstants.EPC_DASHBOARD_COUNT:
                    JSONObject mainObject = object.getJSONObject("resultObject");
                    if (mainObject.has("dashboardCount")) {
                        JSONObject countObject = mainObject.getJSONObject("dashboardCount");
                        Gson g = new Gson();
                        EPCDashboardCountModel model = g.fromJson(countObject.toString(), EPCDashboardCountModel.class);
                        setDashboardCount(model);
                        getDashboardLowerCount();
                    }
                    break;
                case NetworkConstants.EPC_DASHBOARD_LOWER_COUNT:
                    if (object.optJSONObject("resultObject") != null) {
                        JSONObject countObject = object.getJSONObject("resultObject");
                        Gson g = new Gson();
                        EPCDeviceInfo model = g.fromJson(countObject.toString(), EPCDeviceInfo.class);
                        setDashboardLowerCount(model);
                        getDashboardDeviceInfo();
                    }
                    break;
                case NetworkConstants.EPC_DEVICE_INFO:
                    if (object.has("resultObject")) {
                        JSONObject countObject = object.getJSONObject("resultObject");
                        Gson g = new Gson();
                        EPCDeviceInfo model = g.fromJson(countObject.toString(), EPCDeviceInfo.class);
                        setDashboardLowerCount(model);
                     //   navigateToFragment();
                    }
                    break;
            }
        } catch (JSONException e) {
            Log.e("", e.getMessage());
        }
    }


    private void navigateToFragment() {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        Fragment mppt = new EPCNormalUserFragment();
        Bundle b = new Bundle();
        b.putParcelable(USER, Parcels.wrap(user));
        mppt.setArguments(b);
        Fragment strings = new EPCWMSUserFragment();
        strings.setArguments(b);
        adapter.addFragment(mppt, "NormalReport");
        adapter.addFragment(strings, "WMSReport");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Toast.makeText(getApplicationContext(),position,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(getApplicationContext(),position,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Toast.makeText(getApplicationContext(),state,Toast.LENGTH_SHORT).show();
            }
        });
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onFailure(int requestCode) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        InverterInfoModel model = inverterInfoModelList.get(position);
        navigateToFragment();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public User getUser() {
        return user;
    }

}
