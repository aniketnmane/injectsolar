package com.injectsolar.activities;


import android.view.Menu;
import com.injectsolar.R;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import com.injectsolar.utils.SharedSharedPreference;

import com.injectsolar.models.User;

public class BaseActivity extends AppCompatActivity {
    protected static User user;
    public static final String USER = "user";
    public static final String INVERTER_INFO = "inverter_info";
    public int id;

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onBackPressed() {

        if (id == R.id.nav_home) {
            showLogoutDialog();
        } else {
            super.onBackPressed();
        }
    }

    protected void setID(int id) {
        this.id = id;
    }

    protected void showLogoutDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Are you sure you want to logout?");
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Yes",
                (dialog, id1) -> {
                    SharedSharedPreference.clearPreference();
                    user = null;
                    finish();
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    dialog.cancel();

                });

        builder1.setNegativeButton(
                "No",
                (dialog, id12) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
