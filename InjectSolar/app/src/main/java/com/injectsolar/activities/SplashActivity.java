package com.injectsolar.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.injectsolar.R;
import com.injectsolar.fragments.HomeFragment;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.utils.SharedSharedPreference;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

public class SplashActivity extends AppCompatActivity implements NetworkCallback {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    String tag_json_obj = "json_req";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.splashscreen);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */

                if (TextUtils.isEmpty(SharedSharedPreference.getInstance(getApplicationContext()).getUserName()) && TextUtils.isEmpty(SharedSharedPreference.getInstance(getApplicationContext()).getUserPassword())) {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                    SplashActivity.this.finish();
                } else {
                    JSONObject objectMain = null, object = null;
                    objectMain = new JSONObject();
                    if (!TextUtils.isEmpty(SharedSharedPreference.getInstance(getApplicationContext()).getUserName()) && !TextUtils.isEmpty(SharedSharedPreference.getInstance(getApplicationContext()).getUserPassword())) {
                        try {
                            objectMain.put("login_id", SharedSharedPreference.getInstance(getApplicationContext()).getUserName());
                            objectMain.put("password", SharedSharedPreference.getInstance(getApplicationContext()).getUserPassword());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        NetworkCommunication.getInstance().connect(SplashActivity.this, objectMain, "/admin/Admin/login", Request.Method.POST, tag_json_obj, SplashActivity.this, 100, true, false, null);
                    }
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        User user;
        try {
            // JSONObject subObject = object.getJSONObject("solar");
            user = NetworkCommunication.getInstance().gson.fromJson(object.getJSONObject("resultObject").toString(), User.class);
            user.setUserName(SharedSharedPreference.getInstance(getApplicationContext()).getUserName());
            boolean isHomeScreen = false;
            if (user.getRole().equalsIgnoreCase(HomeFragment.EPC_USER))
                isHomeScreen = true;
            Intent intent = new Intent(SplashActivity.this, NavigationDrawerMainActivity.class);
            intent.putExtra("User", Parcels.wrap(user));
            intent.putExtra("IS_NORMAL_USER", isHomeScreen);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(SplashActivity.this, "Invalid Credentials ", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(int requestCode) {

    }
}