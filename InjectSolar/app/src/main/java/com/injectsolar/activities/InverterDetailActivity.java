package com.injectsolar.activities;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.injectsolar.R;
import com.injectsolar.adapter.ParametersViewAdapter;
import com.injectsolar.fragments.HomeFragment;
import com.injectsolar.models.InverterInfoModel;
import com.injectsolar.models.ParameterModel;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InverterDetailActivity extends AppCompatActivity implements NetworkCallback {

    public static final String INVERTER_API_NAME = "InverterName";
    public static final String INVERTER_DISPLAY_NAME = "InverterDisplayName";
    private User user = null;
    public static final String CURRENT_POWER = "current_power";
    public static final String TODAYS_ENERGY = "todays_nergy";
    public static final String LIFETIME_ENERGY = "lifetime_energy";
    public static final String DEVICE_SERIAL_NO = "device_serial_no";
    public static final String FLAG_FROM = "flag_from";
    public static final String INVERTER_NAME = "inverter_name";
    public static final String INVERTER_SERIAL_SUBDEVICE_NO = "inverter_serial_subdevice_no";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private String tag_json_obj = "TAG";
    private InverterInfoModel inverterInfo;
    private ArrayList<InverterInfoModel> invertList;
    @BindView(R.id.rvParameterList)
    RecyclerView rvParameterList;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    private Bundle savedInstanceState;
    private String inverterName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inverter_details);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("Inverter details");
            setSupportActionBar(toolbar);
            // add back arrow to toolbar
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        setTitle("Inverter details");

        user = Parcels.unwrap(getIntent().getParcelableExtra("User"));
        this.savedInstanceState = savedInstanceState;
        getInverterListData();
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
    }

    private void setHomeFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            HomeFragment newFragment = new HomeFragment(user);
            Bundle bundle = new Bundle();
            String str_current = getIntent().getStringExtra(CURRENT_POWER) ;
            String str_todays_energy = getIntent().getStringExtra(TODAYS_ENERGY);
            String str_lifetime = getIntent().getStringExtra(LIFETIME_ENERGY) ;
            String str_inverterName = getIntent().getStringExtra(INVERTER_API_NAME);
            String strDEVICE_SERIAL_NO = getIntent().getStringExtra(DEVICE_SERIAL_NO);
            bundle.putString(INVERTER_API_NAME, str_inverterName);
            bundle.putString(CURRENT_POWER, str_current);
            bundle.putString(TODAYS_ENERGY, str_todays_energy);
            bundle.putString(LIFETIME_ENERGY, str_lifetime);
            bundle.putString(DEVICE_SERIAL_NO, strDEVICE_SERIAL_NO);
            bundle.putString(INVERTER_DISPLAY_NAME, getIntent().getStringExtra(INVERTER_DISPLAY_NAME));
            bundle.putString(INVERTER_NAME, inverterName);
            bundle.putString(INVERTER_SERIAL_SUBDEVICE_NO, getData().toString());
            bundle.putBoolean(FLAG_FROM, true);
            newFragment.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.container, newFragment).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void getInverterListData() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER) ? "normal/Report/getInverrterList" : "wms/Report/getInverrterList";
        NetworkCommunication.getInstance().connect(this, object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.INVERTER_LIST_INFO, true, true, user.getToken());
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        switch (requestCode) {
            case NetworkConstants.INVERTER_LIST_INFO:
                try {
                    if (object.has("resultObject")) {
                        invertList = new ArrayList<>();
                        JSONArray array = object.getJSONArray("resultObject");
                        for (int i = 0; i < array.length(); i++) {
                            invertList.add(NetworkCommunication.getInstance().gson.fromJson(array.get(i).toString(), InverterInfoModel.class));
                        }
                        if (invertList.size()>0) inverterName= invertList.get(0).getInv_name();
                    }
                } catch (JSONException e) {
                    Log.e("", e.getMessage());
                }
                getNameAndValue();
                break;
            case NetworkConstants.PARAMETER_NAME:
                try {
                    if (object.has("resultObject")) {
                        ArrayList<ParameterModel> parameterList = new Gson().fromJson(object.getJSONArray("resultObject").toString(), new TypeToken<List<ParameterModel>>() {
                        }.getType());
                        rvParameterList.setLayoutManager(new LinearLayoutManager(this));
                        ParametersViewAdapter adapter = new ParametersViewAdapter(InverterDetailActivity.this, parameterList);
                        rvParameterList.setAdapter(adapter);
                    }
                    setHomeFragment(savedInstanceState);
                    break;
                } catch (JSONException e) {
                    Log.e("", e.getMessage());
                }
        }

    }

    private JSONObject getData() {
        String inverterName = getIntent().getStringExtra(INVERTER_NAME);
        JSONObject object = new JSONObject();
        try {
            for (int i = 0; i < invertList.size(); i++) {
                if (invertList.get(i).getName().equalsIgnoreCase(inverterName)) {
                    object.put("serial_no", invertList.get(i).getSerial_no());
                    object.put("sub_device_id", invertList.get(i).getSub_device_id());
                }
            }
        } catch (JSONException e) {
            Log.e("", e.getMessage());
        }
        return object;
    }

    private void getNameAndValue() {
        JSONObject object = getData();
        String url = user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER) ? "normal/Normal/getParameterAndValueList" : "wms/Wms/getParameterAndValueList";
        NetworkCommunication.getInstance().connect(this, object, url, Request.Method.POST, tag_json_obj, this, NetworkConstants.PARAMETER_NAME, true, true, user.getToken());
    }

    @Override
    public void onFailure(int requestCode) {

    }

}
