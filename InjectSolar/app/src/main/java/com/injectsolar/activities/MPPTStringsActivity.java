package com.injectsolar.activities;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.injectsolar.R;
import com.injectsolar.adapter.TabAdapter;
import com.injectsolar.fragments.HomeFragment;
import com.injectsolar.fragments.MPPTFragment;
import com.injectsolar.fragments.StringsFragment;
import com.injectsolar.models.InverterInfoModel;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MPPTStringsActivity extends BaseActivity implements NetworkCallback, AdapterView.OnItemSelectedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private String tag_json_obj = "InverterList";
    @BindView(R.id.spinnerInverterList)
    protected Spinner spinnerInvList;
    private ArrayList<InverterInfoModel> inverterInfoModelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mppt_strings);
        ButterKnife.bind(this);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        toolbar = findViewById(R.id.toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("MPPT/Strings");
            setSupportActionBar(toolbar);
            // add back arrow to toolbar
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

        getInverterListData();
    }

    private void getInverterListData() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER) ? "normal/Report/getInverrterList" : "wms/Report/getInverrterList";
        NetworkCommunication.getInstance().connect(this, object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.INVERTER_LIST_INFO, true, true, user.getToken());
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        List<String> inverterNameList = new ArrayList<>();
        InverterInfoModel inverterInfo;
        try {
            if (object.has("resultObject")) {
                JSONArray array = object.getJSONArray("resultObject");
                inverterInfoModelList = new Gson().fromJson(object.getJSONArray("resultObject").toString(), new TypeToken<List<InverterInfoModel>>() {
                }.getType());
                JSONObject responseObject = array.getJSONObject(0);
                inverterInfo = NetworkCommunication.getInstance().gson.fromJson(responseObject.toString(), InverterInfoModel.class);
                if (inverterInfoModelList != null) {
                    for (int i = 0; i < inverterInfoModelList.size(); i++) {
                        inverterNameList.add(inverterInfoModelList.get(i).getName());
                    }
                    setInvAdapterList(inverterNameList);
                }
                navigateToFragment(inverterInfo);
            }
        } catch (JSONException e) {
            Log.e("", e.getMessage());
        }
    }

    private void setInvAdapterList(List<String> list) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerInvList.setAdapter(dataAdapter);
        spinnerInvList.setOnItemSelectedListener(this);
    }

    private void navigateToFragment(InverterInfoModel inverterInfo) {
        tabLayout.setupWithViewPager(null);
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        Fragment mppt = new MPPTFragment();
        Bundle b = new Bundle();
        b.putParcelable(USER, Parcels.wrap(user));
        b.putParcelable(INVERTER_INFO, inverterInfo);
        mppt.setArguments(b);
        Fragment strings = new StringsFragment();
        strings.setArguments(b);
        adapter.addFragment(mppt, "MPPT");
        adapter.addFragment(strings, "Strings");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onFailure(int requestCode) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        InverterInfoModel model = inverterInfoModelList.get(position);
        navigateToFragment(model);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
