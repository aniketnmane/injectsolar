package com.injectsolar.activities;


import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.injectsolar.R;
import com.injectsolar.adapter.TabAdapter;
import com.injectsolar.fragments.ClearedAlarms;
import com.injectsolar.fragments.UnClearedAlarms;
import com.injectsolar.models.InverterInfoModel;

import org.parceler.Parcels;

public class AlarmsActivity extends BaseActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private Spinner spinnerInverterList;
    private String tag_json_obj = "InverterList";

    private InverterInfoModel inverterInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mppt_strings);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        toolbar = findViewById(R.id.toolbar);
        spinnerInverterList = findViewById(R.id.spinnerInverterList);
        spinnerInverterList.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("Alarms");
            setSupportActionBar(toolbar);
            // add back arrow to toolbar
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        navigateToFragment();
    }

    private void navigateToFragment() {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        Fragment clearedAlarms = new ClearedAlarms();
        Bundle b = new Bundle();
        b.putParcelable(USER, Parcels.wrap(user));
        b.putParcelable(INVERTER_INFO, inverterInfo);
        clearedAlarms.setArguments(b);
        Fragment unclearedAlarmFragment = new UnClearedAlarms();
        unclearedAlarmFragment.setArguments(b);
        adapter.addFragment(unclearedAlarmFragment, "UnClearedAlarms");
        adapter.addFragment(clearedAlarms, "ClearedAlarms");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}
