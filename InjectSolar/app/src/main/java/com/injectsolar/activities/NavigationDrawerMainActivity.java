package com.injectsolar.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;
import com.injectsolar.R;
import com.injectsolar.fragments.EPCUserFragment;
import com.injectsolar.fragments.HomeFragment;
import com.injectsolar.fragments.InverterListFragment;
import com.injectsolar.fragments.SensorFragment;
import com.injectsolar.networking.NetworkCallback;

import org.json.JSONObject;
import org.parceler.Parcels;

import butterknife.ButterKnife;

public class NavigationDrawerMainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, NetworkCallback {
    public static final String PARAM_URL = "URL";
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private TextView tvUserName;
    private TextView tvMailId;
    private static final String URL = "http://www.injectsolar.com/product.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);
        setTitleActionBar("");
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        user = Parcels.unwrap(getIntent().getParcelableExtra("User"));
        boolean isNormalHomeScreen = getIntent().getBooleanExtra("IS_NORMAL_USER", false);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        tvUserName = headerLayout.findViewById(R.id.tvUserName);
        tvUserName.setText(user.getUser_epic_id());
        tvMailId = headerLayout.findViewById(R.id.tvMailId);
        tvMailId.setText(user.getUserName());
        navigationView.setNavigationItemSelectedListener(this);
        Fragment fragment;
        if (user.getRole().equalsIgnoreCase(HomeFragment.EPC_USER) && isNormalHomeScreen) {
            fragment = new EPCUserFragment(user);
            Menu menu = navigationView.getMenu();
            MenuItem target = menu.findItem(R.id.nav_inverter_data);
            target.setVisible(false);
            MenuItem target2 = menu.findItem(R.id.nav_mppt_strings);
            target2.setVisible(false);
            MenuItem target3 = menu.findItem(R.id.nav_system_details);
            target3.setVisible(false);
           // setID(R.id.nav_home);
        } else {
            fragment = new HomeFragment(user);
           // setID(R.id.nav_home);
        }

        setFragment(fragment);
        toggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawer.setDrawerListener(toggle);
        drawer.post(new Runnable() {
            @Override
            public void run() {
                toggle.syncState();
            }
        });
        if (user.getRole().equalsIgnoreCase(HomeFragment.WMS_USER)) {
            Menu menu = navigationView.getMenu();
            MenuItem target = menu.findItem(R.id.nav_sensor_data);
            target.setVisible(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle(user.getName());
            setSupportActionBar(toolbar);
        }
    }


    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle
            persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        toggle.syncState();
    }

    protected void setTitleActionBar(String title) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        // toolbar.setNavigationIcon();
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            toolbar.setTitle(user.getName());
            if (user.getRole().equalsIgnoreCase(HomeFragment.EPC_USER)) {
                fragment = new EPCUserFragment(user);
            } else {
               // setID(id);
                fragment = new HomeFragment(user);
            }
        } else if (id == R.id.nav_inverter_data) {
            toolbar.setTitle(getResources().getString(R.string.inverter_details));
            fragment = new InverterListFragment(user);
        } else if (id == R.id.nav_send) {
            Intent intent = new Intent(NavigationDrawerMainActivity.this, WebViewActivity.class);
            intent.putExtra(PARAM_URL, URL);
            startActivity(intent);
        } else if (id == R.id.nav_alaram) {
            toolbar.setTitle(getResources().getString(R.string.alarms));
            Intent intent = new Intent(NavigationDrawerMainActivity.this, AlarmsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sensor_data) {
            toolbar.setTitle(getResources().getString(R.string.sensor_details));
            fragment = new SensorFragment(user);
        } /*else if (id == R.id.nav_share) {
            fragment = new HomeFragment(user);
        } else if (id == R.id.nav_send) {
            fragment = new HomeFragment(user);
        }*/ else if (id == R.id.nav_mppt_strings) {
            Intent intent = new Intent(NavigationDrawerMainActivity.this, MPPTStringsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_system_details) {
            Intent intent = new Intent(NavigationDrawerMainActivity.this, SystemDetailsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            showLogoutDialog();
        }
        if (fragment != null)
            setFragment(fragment);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {

    }

    @Override
    public void onFailure(int requestCode) {

    }


}
