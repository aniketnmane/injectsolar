package com.injectsolar.activities;

import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.injectsolar.R;
import com.injectsolar.adapter.SystemDetailsRecyclerAdapter;
import com.injectsolar.fragments.HomeFragment;
import com.injectsolar.models.SystemDetails;
import com.injectsolar.models.SystemDetailsResponse;
import com.injectsolar.models.SystemDetailsSectionHeader;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;
import com.injectsolar.utils.RecyclerItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SystemDetailsActivity extends BaseActivity implements NetworkCallback {
    private String tag_json_obj = "SystemDetails";
    @BindView(R.id.rvSystemDetails)
    RecyclerView rvSystemDetails;
    @BindView(R.id.tvSystemSize)
    TextView tvSystemSize;
    @BindView(R.id.tvCustomerName)
    TextView tvCustomerName;
    @BindView(R.id.tvSolarPanel)
    TextView tvSolarPanel;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvContactDetails)
    TextView tvContactDetails;
    @BindView(R.id.tvStrNoDevices)
    TextView tvStrNoDevices;
    @BindView(R.id.tvElectricityName)
    TextView tvElectricityName;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_details);
        ButterKnife.bind(this);
        getSytemDetails();
        setupToolBar();
    }

    private void setupToolBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("System Details");
            this.setSupportActionBar(toolbar);
            // add back arrow to toolbar
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void getSytemDetails() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER) ? "/normal/Normal/getSystemDetails" : "/wms/Wms/getSystemDetails";
        NetworkCommunication.getInstance().connect(this, object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.SYSTEM_DETAILS, true, true, user.getToken());
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        try {
            SystemDetailsResponse systemDetails = NetworkCommunication.getInstance().gson.fromJson(object.toString(), SystemDetailsResponse.class);
            setTopUI(systemDetails.getSystemDetails());
            if (systemDetails.getStatus().equalsIgnoreCase("true")) {
                ArrayList<SystemDetailsSectionHeader> headers = new ArrayList<>();
                //   for (int i = 0; i < systemDetails.getDevice_info().size(); i++) {
                SystemDetailsSectionHeader header = new SystemDetailsSectionHeader(systemDetails.getDevice_info(), "", 0);
                headers.add(header);
                //  }
                setAdapter(headers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(int requestCode) {

    }

    private void setTopUI(SystemDetails systemDetails) {
        tvSystemSize.setText(systemDetails.getSystem_size());
        tvCustomerName.setText(systemDetails.getCustomer_name());
        tvSolarPanel.setText(systemDetails.getSolar_panel());
        tvAddress.setText(systemDetails.getAddress());
        tvContactDetails.setText(systemDetails.getContact_detail());
        tvStrNoDevices.setText(systemDetails.getNum_of_devices());
        tvElectricityName.setText(systemDetails.getElec_rate());
    }

    private void setAdapter(ArrayList<SystemDetailsSectionHeader> headerList) {
        SystemDetailsRecyclerAdapter adapter = new SystemDetailsRecyclerAdapter(this, headerList, user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvSystemDetails.setLayoutManager(linearLayoutManager);
        rvSystemDetails.setHasFixedSize(true);
        rvSystemDetails.setAdapter(adapter);
        rvSystemDetails.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), rvSystemDetails, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        //   String loginId = headerList.get(0).getChildList().get(position - 1).getId();
                        //getNormalUserLogin(loginId);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
    }
}
