package com.injectsolar.deps;


import com.injectsolar.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Aniket on 6/28/19.
 */
@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
}
