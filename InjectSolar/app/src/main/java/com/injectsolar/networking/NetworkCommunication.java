package com.injectsolar.networking;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.injectsolar.application.ApplicationController;
import com.injectsolar.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkCommunication {

    private static final int MY_SOCKET_TIMEOUT_MS = 50000;
    private static boolean useNewUrl = false;
    private final String TAG = NetworkCommunication.class.getName();

    private NetworkCallback callback;

    private String baseURL = "http://34.204.184.160/apis/index.php/home";
    private String newBaseURL = "http://3.6.0.2/inject-solar-angular/inject_solar_server/";
    private String finalUrl = baseURL;
    private int REQUEST_CODE;

    private static NetworkCommunication instance;

    private GsonBuilder gsonBuilder;

    public Gson gson;

    private NetworkCommunication() {
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public static NetworkCommunication getInstance() {
        if (instance == null) {
            instance = new NetworkCommunication();
        }

        return instance;
    }

    public void connect(Context context, JSONObject requestObject, String url, int method, String jsonTag, final NetworkCallback callback, final int requestCode, final boolean isNewUrl, final boolean useHeader, final String authToken) {
        try {
            finalUrl = isNewUrl ? newBaseURL + url : baseURL + url;
            this.callback = callback;
            this.REQUEST_CODE = requestCode;
            final ProgressDialog pDialog = new ProgressDialog(context);

            Log.d("URL==>", "" + finalUrl);
            Log.d("Request==>", "" + requestObject.toString());
            pDialog.setMessage("Loading...");
            pDialog.show();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(method,
                    finalUrl, requestObject,
                    response -> {
                        try {
                            if (response.getString("status").equals("true")) {
                                Log.d("Response==>", response.toString());
                                pDialog.hide();

                                callback.onSuccess(response, REQUEST_CODE);
                            } else {
                                callback.onFailure(REQUEST_CODE);
                                pDialog.hide();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                callback.onFailure(REQUEST_CODE);
                pDialog.hide();
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    if (!useHeader) return super.getHeaders();
                    else {
                        Map<String, String> params = new HashMap<>();
                        params.put("content-type", "application/json");
                        params.put("Authorization", authToken);
                        return params;
                    }
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
// Adding request to request queue
            ApplicationController.getInstance().addToRequestQueue(jsonObjReq, jsonTag);
        } catch (Exception e) {
           Log.e("APIError",e.getMessage()) ;
        }
    }

    public void connect(Context context, JSONObject requestObject, String url, int method, String jsonTag, final NetworkCallback callback, final int requestCode) {
        finalUrl = baseURL + url;
        this.callback = callback;
        this.REQUEST_CODE = requestCode;
        final ProgressDialog pDialog = new ProgressDialog(context);

        Log.d("URL==>", "" + finalUrl);
        Log.d("Request==>", "" + requestObject.toString());
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(method,
                finalUrl, requestObject,
                response -> {
                    Log.d("Response==>", response.toString());
                    pDialog.hide();
                    JSONObject objectMain = null;
                    try {
                        objectMain = response.getJSONObject("solar");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callback.onSuccess(objectMain, REQUEST_CODE);

                }, error -> {
            //Toast.makeText(LoginActivity.this, "Username or password is wrong", Toast.LENGTH_LONG).show();
            VolleyLog.d(TAG, "Error: " + error.getMessage());
            callback.onFailure(REQUEST_CODE);
            // hide the progress dialog
            pDialog.hide();
        }
        );

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

// Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(jsonObjReq, jsonTag);
    }

    public void setUseNewUrl(boolean isNewUrl) {
        useNewUrl = isNewUrl;
    }
}
