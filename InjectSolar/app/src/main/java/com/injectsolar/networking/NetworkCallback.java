package com.injectsolar.networking;

import org.json.JSONObject;

public interface NetworkCallback {
    void onSuccess(JSONObject object,int requestCode);

    void onFailure(int requestCode);
}
