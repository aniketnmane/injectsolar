package com.injectsolar.networking;


import com.injectsolar.models.LoginResponse;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by ennur on 6/25/16.
 */
public interface NetworkService {

    @GET("v1/city")
    Observable<LoginResponse> getLoginData();

}
