package com.injectsolar.networking;

public class NetworkConstants {
    public static final int DASHBOARD_DATA = 1001;
    public static final int TODAYS_GRAPH_DATA = 1002;
    public static final int MONTH_GRAPH_DATA = 1003;
    public static final int YEAR_GRAPH_DATA = 1004;
    public static final int LIFE_TIME = 1005;
    public static final int INVERTER_LIST = 1007;
    public static final int MPPT = 1008;
    public static final int ERROR_LOGS = 1009;
    public static final int DEVICE_LIST = 1010;
    public static final int INVERTER_DEVICE_LIST = 1011;
    public static final int INVERTER_LIST_INFO = 1012;
    public static final int CLEARED_ALARAMS = 1013;
    public static final int UN_CLEARED_ALARAMS = 1014;
    public static final int PARAMETER_NAME = 1015;
    public static final int SENSOR_LIST = 1016;
    public static final int SENSOR_DATA = 1017;
    public static final int SENSOR_GRAPH_DATA = 1018;
    public static final int EPC_DASHBOARD_COUNT = 1019;
    public static final int EPC_DASHBOARD_LOWER_COUNT = 1020;
    public static final int EPC_DEVICE_INFO = 1021;
    public static final int EPC_DEVICE_DATA_USER = 1022;
    public static final int EPC_GET_ALL_WMS_USER = 1023;
    public static final int EPC_NORMAL_USER_LOGIN = 1024;
    public static final int SYSTEM_DETAILS = 1025;
}
