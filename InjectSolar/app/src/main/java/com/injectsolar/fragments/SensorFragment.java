package com.injectsolar.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.injectsolar.R;
import com.injectsolar.application.ApplicationController;
import com.injectsolar.models.SensorDataModel;
import com.injectsolar.models.SensorModel;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;
import com.injectsolar.utils.MyCustomXAxisValueFormatter;
import com.injectsolar.views.custom_views.MyMarkerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SensorFragment extends BaseFragment implements SeekBar.OnSeekBarChangeListener,
        OnChartGestureListener, OnChartValueSelectedListener, View.OnClickListener, DatePickerDialog.OnDateSetListener, OnDateSelected, NetworkCallback, AdapterView.OnItemSelectedListener {
    @BindView(R.id.chart_line)
    LineChart mChart;
    @BindView(R.id.tvStartDate)
    TextView tvStartDate;
    @BindView(R.id.tvEndDate)
    TextView tvEndDate;
    @BindView(R.id.spinnerSensorList)
    Spinner spinnerSensorList;
    @BindView(R.id.btnSearch)
    ImageButton btnSearch;
    @BindView(R.id.tvAmbientTemperatureValue)
    TextView tvAmbientTemperatureValue;
    @BindView(R.id.tvHumidityValue)
    TextView tvHumidityValue;
    @BindView(R.id.tvIrradianceValue)
    TextView tvIrradianceValue;
    @BindView(R.id.tvModuleTemperatureValue)
    TextView tvModuleTemperatureValue;
    @BindView(R.id.tvWindDirectionValue)
    TextView tvWindDirectionValue;
    @BindView(R.id.tvWindSpeedValue)
    TextView tvWindSpeedValue;

    @BindView(R.id.tvSensor1)
    TextView tvSensor1;
    @BindView(R.id.tvSensorUni1)
    TextView tvSensorUnit1;
    @BindView(R.id.tvSensor2)
    TextView tvSensor2;
    @BindView(R.id.tvSensorUni2)
    TextView tvSensorUnit2;
    @BindView(R.id.tvSensor3)
    TextView tvSensor3;
    @BindView(R.id.tvSensorUni3)
    TextView tvSensorUnit3;
    @BindView(R.id.tvSensor4)
    TextView tvSensor4;
    @BindView(R.id.tvSensorUni4)
    TextView tvSensorUnit4;
    @BindView(R.id.tvSensor5)
    TextView tvSensor5;
    @BindView(R.id.tvSensorUni5)
    TextView tvSensorUnit5;
    @BindView(R.id.tvSensor6)
    TextView tvSensor6;
    @BindView(R.id.tvSensorUni6)
    TextView tvSensorUnit6;


    private String tag_json_obj = "";
    private int selectedSensor = 0;
    private String selectedStartDate = "";
    private String selectedEndDate = "";
    private List<SensorDataModel> sensorList;
    private TextView tvSelectedDate;
    private boolean tvStartSelected = false;
    private float maxValue;
    private ArrayList<SensorModel> parameterList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sensor_fragment, container, false);
        ButterKnife.bind(this, view);
        Objects.requireNonNull(getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setUIDateMonthYear();
        getSensorList();
        setOnclickListener();
        return view;
    }

    @SuppressLint("ValidFragment")
    public SensorFragment(User user) {
        this.user = user;
    }

    private void setOnclickListener() {
        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    private void setUIDateMonthYear() {
        Calendar calendar = Calendar.getInstance();
        selectedStartDate = getFormatter().format(calendar.getTime());
        selectedEndDate = getFormatter().format(calendar.getTime());
        tvStartDate.setText(selectedStartDate);
        tvEndDate.setText(selectedEndDate);
    }

    public void getSensorList() {
        JSONObject object = new JSONObject();
        String url = "wms/Wms/getSensorList";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, SensorFragment.this, NetworkConstants.SENSOR_LIST, true, true, user.getToken());
    }

    private void getSensorGraphData() {
        try {
            JSONObject object = new JSONObject();
            String url = "graph/Graph/loadWmsSensorGraphData";
            object.put("user_id", user.getUserName());
            object.put("pname", parameterList.get(selectedSensor).getPname());
            object.put("fromDate", selectedStartDate);
            object.put("toDate", selectedEndDate);
            NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.POST, tag_json_obj, SensorFragment.this, NetworkConstants.SENSOR_GRAPH_DATA, true, true, user.getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getSensorPageData() {
        try {
            JSONObject object = new JSONObject();
            String url = "graph/Graph/loadWmsSensorData";
            object.put("user_id", user.getUserName());
            NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.POST, tag_json_obj, SensorFragment.this, NetworkConstants.SENSOR_DATA, true, true, user.getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvStartDate:
                tvSelectedDate = tvStartDate;
                showDatePicker();
                tvStartSelected = true;
                break;
            case R.id.tvEndDate:
                tvSelectedDate = tvEndDate;
                showDatePicker();
                tvStartSelected = false;
                break;
            case R.id.btnSearch:
                getSensorGraphData();
                break;
        }
    }

    @Override
    public void onDateSelected(String selectedDate) {
        String formatter = "yyyy-MM-dd";
        try {
            mChart.clear();
            mChart.invalidate();
            @SuppressLint("SimpleDateFormat") java.text.SimpleDateFormat dateFormatter = new java.text.SimpleDateFormat(formatter);
            Date sDate = dateFormatter.parse(selectedDate);
            assert sDate != null;
            String formattedCurrentDate = getFormatter().format(sDate);
            Toast.makeText(getActivity(), "Your selected date is " + formattedCurrentDate, Toast.LENGTH_SHORT).show();
            tvSelectedDate.setText(formattedCurrentDate);
            if (tvStartSelected) selectedStartDate = formattedCurrentDate;
            else selectedEndDate = formattedCurrentDate;
            // getGraphData(formattedCurrentDate, month + 1, year, NetworkConstants.TODAYS_GRAPH_DATA);
        } catch (ParseException e) {
            Log.e("Error", Objects.requireNonNull(e.getMessage()) + "\n" + e.getCause() + "\n" + Arrays.toString(e.getStackTrace()) + "\n" + e.fillInStackTrace() + "\n" + e.getLocalizedMessage() + "\n" + Arrays.toString(e.getStackTrace()));
        }
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        try {
            switch (requestCode) {
                case NetworkConstants.SENSOR_LIST:
                    try {
                        List<String> sensorList = new ArrayList<>();
                        if (object.has("resultObject") && object.optJSONArray("resultObject") != null) {
                            this.sensorList = new Gson().fromJson(Objects.requireNonNull(object.optJSONArray("resultObject")).toString(), new TypeToken<List<SensorDataModel>>() {
                            }.getType());
                            for (int i = 0; i < this.sensorList.size(); i++) {
                                sensorList.add(this.sensorList.get(i).getSensor_name());
                            }
                            setSensorListAdapter(sensorList);
                            getSensorPageData();
                        }
                    } catch (Exception e) {
                        Log.e("Error", Objects.requireNonNull(e.getMessage()) + "\n" + e.getCause() + "\n" + Arrays.toString(e.getStackTrace()) + "\n" + e.fillInStackTrace() + "\n" + e.getLocalizedMessage() + "\n" + Arrays.toString(e.getStackTrace()));
                    }
                    break;
                case NetworkConstants.SENSOR_DATA:
                    try {
                        JSONObject mainObject = object.optJSONObject("resultObject");
                        assert mainObject != null;
                       /* JSONArray sensorArray = mainObject.getJSONArray("sensorData");
                        if (sensorArray != null && sensorArray.length() > 0) {
                            for (int i = 0; i < sensorArray.length(); i++) {
                                SensorModel model = new SensorModel();
                                JSONObject objArray = sensorArray.getJSONObject(i);
                                model.setPname(objArray.getString("pname"));
                                model.setUnit(objArray.getString("unit"));
                                model.setPname(objArray.getString("val"));
                                parameterList.add(model);
                            }*/
                            parameterList = new Gson().fromJson(Objects.requireNonNull(mainObject.getJSONArray("sensorData")).toString(), new TypeToken<List<SensorModel>>() {
                            }.getType());
                            setSensorData(parameterList);
                        if (mainObject.optJSONArray("graphData") != null && mainObject.getJSONArray("graphData").length() > 0) {
                            getSensorGraphData();
                        }
                    } catch (Exception e) {
                        Log.e("Error", Objects.requireNonNull(e.getMessage()) + "\n" + e.getCause() + "\n" + Arrays.toString(e.getStackTrace()) + "\n" + e.fillInStackTrace() + "\n" + e.getLocalizedMessage() + "\n" + Arrays.toString(e.getStackTrace()));
                    }
                    break;
                case NetworkConstants.SENSOR_GRAPH_DATA:
                    if (object.has("resultObject") && object.optJSONArray("resultObject") != null && object.getJSONArray("resultObject").length() > 0) {
                        JSONArray arrayGraph = object.getJSONArray("resultObject");
                        graphInitialization(arrayGraph);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int requestCode) {

    }

    private void setSensorListAdapter(List<String> list) {
        if (list != null) {
            selectedSensor = 0;
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSensorList.setAdapter(dataAdapter);
            spinnerSensorList.setOnItemSelectedListener(this);
        }
    }

    private void setSensorData(ArrayList<SensorModel> dataList) {
        if (dataList != null) {
            for (int i = 0; i < dataList.size(); i++) {
                //    Log.i("Test",dataList.get(i).getPname());
                switch (i) {
                    case 0:
                        tvSensor1.setText(dataList.get(0).getPname());
                        tvAmbientTemperatureValue.setText(dataList.get(0).getVal());
                        tvSensorUnit1.setText(dataList.get(0).getUnit());
                        break;
                    case 1:
                        tvSensor2.setText(dataList.get(i).getPname());
                        tvHumidityValue.setText(dataList.get(i).getVal());
                        tvSensorUnit2.setText(dataList.get(i).getUnit());
                        break;
                    case 2:
                        tvSensor3.setText(dataList.get(i).getPname());
                        tvIrradianceValue.setText(dataList.get(i).getVal());
                        tvSensorUnit3.setText(dataList.get(i).getUnit());
                        break;
                    case 3:
                        tvSensor4.setText(dataList.get(i).getPname());
                        tvModuleTemperatureValue.setText(dataList.get(i).getVal());
                        tvSensorUnit4.setText(dataList.get(i).getUnit());
                        break;
                    case 4:
                        tvSensor5.setText(dataList.get(i).getPname());
                        tvWindDirectionValue.setText(dataList.get(i).getVal());
                        tvSensorUnit5.setText(dataList.get(i).getUnit());
                        break;
                    case 5:
                        tvSensor6.setText(dataList.get(i).getPname());
                        tvWindSpeedValue.setText(dataList.get(i).getVal());
                        tvSensorUnit6.setText(dataList.get(i).getUnit());
                        break;
                }
            }
        }
    }

    /*private String getSensorUnit(ArrayList<SensorModel> dataList, String sensorName) {
        if (dataList != null && sensorName != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (sensorName.toUpperCase().compareToIgnoreCase(dataList.get(i).getPname().toUpperCase()) == 0) {
                    return dataList.get(i).getUnit();
                }
            }
        }
        return "N/A";
    }*/
/*
    private String getSensorValue(ArrayList<SensorModel> dataList, String sensorName) {
        if (dataList != null && sensorName != null) {
            for (int i = 0; i < dataList.size(); i++) {
                if (sensorName.toUpperCase().compareTo(dataList.get(i).) == 0) {
                    return dataList.get(i).getVal();
                }
            }
        }
        return "N/A";
    }*/


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedSensor = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showDatePicker() {
        DialogFragment newFragment = new MyDatePickerFragment(SensorFragment.this);
        Bundle args = new Bundle();
        args.putInt("view_id", R.id.tvStartDate);
        newFragment.setArguments(args);
        newFragment.show(Objects.requireNonNull(getActivity()).getFragmentManager(), "date picker");
    }

    private void graphInitialization(JSONArray array) {
        maxValue = 0;
        mChart = null;
        mChart = Objects.requireNonNull(getActivity()).findViewById(R.id.chart_line);
        mChart.zoomOut();
        mChart.fitScreen();
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        // mChart.setScaleXEnabled(true);
        // mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        // x-axis limit line
        LimitLine llXAxis = new LimitLine(10f, "");
        llXAxis.setLineWidth(0f);
        // llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        //   xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line


        // Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Typefaces.FONT_LATO_REGULAR);

        LimitLine ll1 = new LimitLine(150f, "");
        ll1.setLineWidth(0f);
        //  ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
        ll1.setTextSize(10f);
        ll1.setTypeface(ApplicationController.fntLatoRegular);

        LimitLine ll2 = new LimitLine(0f, "");
        ll2.setLineWidth(0f);
        //   ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);
        ll2.setTypeface(ApplicationController.fntLatoRegular);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(ll1);
        leftAxis.addLimitLine(ll2);

        leftAxis.setAxisMinimum(0f);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(false);

        mChart.getAxisRight().setEnabled(false);
        // add data
        if (array != null && array.length() > 0) {
            setData(array);
            mChart.resetZoom();
            mChart.animateX(2500);
            float value = (float) (maxValue * 1.2);
            leftAxis.setAxisMaximum(value);
            // get the legend (only possible after setting data)
            Legend l = mChart.getLegend();
            // modify the legend ...
            l.setForm(Legend.LegendForm.DEFAULT);
        }
    }

    private void setData(JSONArray array) {
        mChart.clear();
        ArrayList<Entry> values = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String keyJSON = object.getString("date_time");
                if (!TextUtils.isEmpty(keyJSON)) {
                    String[] keys = keyJSON.split(" ");
                    Date dateFormatted = formatter.parse(keyJSON);
                    String[] parts = keyJSON.split("\\ "); // String array, each element is text between dots
                    String key = parts[1];
                    String strXAxis = parts[1];
                    String updateXis = strXAxis.replace(":", ".");
                    float val = Float.parseFloat(object.getString("value"));
                    if (val > maxValue)
                        maxValue = val;
                    assert dateFormatted != null;
                    values.add(new Entry(dateFormatted.getTime(), val, keys[1] + " (" + parameterList.get(selectedSensor).getUnit() + ")"));
                }
            }
        } catch (JSONException e) {
            Log.e(SensorFragment.class.getSimpleName(), Objects.requireNonNull(e.getMessage()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        LineDataSet set1;
        LineDataSet set2;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, parameterList.get(selectedSensor).getPname() + " " + parameterList.get(selectedSensor).getUnit());
            LineDataSet updatedSet = setCustomisationDataSet(set1);
            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(updatedSet); // add the datasets
            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            // set data
            mChart.setData(data);

        }
    }

    private LineDataSet setCustomisationDataSet(LineDataSet set) {
        set.setDrawIcons(false);
        // set the line to be drawn like this "- - - - - -"
        set.enableDashedLine(10f, 0f, 0f);
        set.enableDashedHighlightLine(10f, 0f, 0f);
        set.setLineWidth(1f);
        set.setCircleRadius(3f);
        set.setDrawCircleHole(false);
        set.setDrawCircles(false);
        set.setValueTextSize(0f);
        set.setDrawFilled(true);
        set.setFormLineWidth(1f);
        set.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set.setFormSize(15.f);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = null;
            drawable = ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.fade_red);
            set.setColor(Color.BLACK);
            set.setCircleColor(Color.BLACK);
            set.setFillDrawable(drawable);
        } else {
            set.setFillColor(Color.BLACK);
        }
        return set;
    }

    private String getUnit(String sensorName) {
        String unitName = "";
        switch (sensorName) {
            case "Irradiation":
                unitName = getString(R.string.w_m_2);
                break;
            case "Ambient Temperature":
            case "Module Temperature":
                unitName = getString(R.string.degree_c);
                break;
            case "Humidity":
                unitName = getString(R.string.percentage);
                break;
            case "Wind Speed":
                unitName = getString(R.string.m_s_2);
                break;
            case "Wind Direction":
                unitName = getString(R.string.degree);
                break;
        }
        return unitName;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
