package com.injectsolar.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.injectsolar.models.InverterDataModel;
import com.injectsolar.models.User;

import java.text.SimpleDateFormat;
import java.util.List;

public abstract class BaseFragment extends Fragment {
    protected Typeface mTfRegular;
    protected Typeface mTfLight;
    InverterDataModel selectedModel;
    protected SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    protected User user;
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String REQUEST_DATE_FORMAT = "yyyy-MM-dd";

    protected SimpleDateFormat getFormatter() {
        return new java.text.SimpleDateFormat(DATE_FORMAT);
    }

    protected SimpleDateFormat getFormatter(String formatter) {
        return new java.text.SimpleDateFormat(formatter);
    }
}
