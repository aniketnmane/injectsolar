/*
package com.injectsolar.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.android.volley.Request
import com.injectsolar.R
import com.injectsolar.activities.BaseActivity
import com.injectsolar.networking.NetworkCallback
import com.injectsolar.networking.NetworkCommunication
import com.injectsolar.networking.NetworkConstants
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ClearedAlarmFragment : BaseFragment(), NetworkCallback {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.alarm_fragment, container, false)
        clearedAlarms
        ButterKnife.bind(this, view)
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        assert(arguments != null)
        user = arguments!!.getParcelable(BaseActivity.USER)
    }

    //  objectMain.put("device_serial_no", user.getDevice_serial_no());
    private val clearedAlarms: Unit
        get() {
            val calendar=Calendar.getInstance()
            val currentDate = Calendar.getInstance().time
            val formattedCurrentDate: String = formatter.format(currentDate)
            calendar.add(Calendar.DAY_OF_YEAR, 7)
            val weekBeforeDate =calendar.time
            val formattedWeekBeforeDate: String = formatter.format(weekBeforeDate)
            val objectMain = JSONObject()
            try { //  objectMain.put("device_serial_no", user.getDevice_serial_no());
                objectMain.put("user_id", user!!.userId)
                objectMain.put("limit", 20)
                objectMain.put("offset", 0)
                objectMain.put("start_date", 0)
                objectMain.put("end_date", formattedCurrentDate)

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            NetworkCommunication.getInstance().connect(activity, objectMain, "/mppt", Request.Method.POST, tag_json_obj, this@ClearedAlarmFragment, NetworkConstants.MPPT)
        }

    override fun onSuccess(jsonObject: JSONObject, requestCode: Int) {
       */
/* try {
            if (!TextUtils.isEmpty(jsonObject.getString("res_code")) && jsonObject.getString("res_code").equals("true", ignoreCase = true)) {
                val finalObject = jsonObject.getJSONArray("errors")
                val headerList = ArrayList<SectionHeader>()
                val allInverterList = ArrayList<MPPTStringsInverterModel>()
                for (i in 0 until finalObject.length()) {
                    val invertersList = finalObject.getJSONObject(i).getJSONArray("inverters")
                    for (j in 0 until invertersList.length()) {
                        val g = Gson()
                        val model = g.fromJson(invertersList.getJSONObject(j).toString(), MPPTStringsInverterModel::class.java)
                        allInverterList.add(model)
                    }
                }
                for (j in allInverterList.indices) {
                    val header = SectionHeader(allInverterList[j].mppt, allInverterList[j].inverter_no, j)
                    if (allInverterList[j].mppt.size > 0) headerList.add(header)
                }
                if (headerList.size != 0) {
                    tv_Data_not_available!!.visibility = View.GONE
                    rv_mppt_list!!.visibility = View.VISIBLE
                    val adapter = MPPTStringRecyclerAdapter(activity, headerList, true)
                    val linearLayoutManager = LinearLayoutManager(activity)
                    rv_mppt_list!!.layoutManager = linearLayoutManager
                    rv_mppt_list!!.setHasFixedSize(true)
                    rv_mppt_list!!.adapter = adapter
                } else {
                    tv_Data_not_available!!.visibility = View.VISIBLE
                    rv_mppt_list!!.visibility = View.GONE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }*//*

    }

    override fun onFailure(requestCode: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

*/
