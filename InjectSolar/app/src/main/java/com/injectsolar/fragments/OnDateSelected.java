package com.injectsolar.fragments;

public interface OnDateSelected {
    void onDateSelected(String selectedDate);

}
