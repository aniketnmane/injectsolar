package com.injectsolar.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.activities.BaseActivity;
import com.injectsolar.adapter.AlarmsRecyclerAdapter;
import com.injectsolar.models.AlarmSectionHeader;
import com.injectsolar.models.AlarmsModel;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClearedAlarms extends BaseFragment implements NetworkCallback {

    @BindView(R.id.rv_cleared_alarm)
    protected RecyclerView rv_cleared_list;
    @BindView(R.id.tv_Data_not_available)
    protected TextView tv_Data_not_available;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alarm_fragment, container, false);
        getAlarmData();
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        user = Parcels.unwrap(getArguments().getParcelable(BaseActivity.USER));
    }

    private void getAlarmData() {
        String tag_json_obj = "";
        if (user.getRole().equalsIgnoreCase(HomeFragment.EPC_USER)) {
            Calendar calendar = Calendar.getInstance();
            Date currentDate = Calendar.getInstance().getTime();
            String formattedCurrentDate = getFormatter(REQUEST_DATE_FORMAT).format(currentDate);
            calendar.add(Calendar.DAY_OF_YEAR, -7);
            Date weekBeforeDate = calendar.getTime();
            String formattedWeekBeforeDate = getFormatter(REQUEST_DATE_FORMAT).format(weekBeforeDate);
            JSONObject objectMain = new JSONObject();
            try { //  objectMain.put("device_serial_no", user.getDevice_serial_no());
                objectMain.put("epc_id", user.getEpc_id());
                objectMain.put("user_id", user.getUserId());
                objectMain.put("limit", 100);
                objectMain.put("offset", 0);
                objectMain.put("start_date", formattedWeekBeforeDate);
                objectMain.put("end_date", formattedCurrentDate);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            NetworkCommunication.getInstance().connect(getActivity(), objectMain, "epc/Alarms/getClearedEpcAlarms", Request.Method.POST, tag_json_obj, this, NetworkConstants.CLEARED_ALARAMS, true, true, user.getToken());

        } else {
            Calendar calendar = Calendar.getInstance();
            Date currentDate = Calendar.getInstance().getTime();
            String formattedCurrentDate = getFormatter(REQUEST_DATE_FORMAT).format(currentDate);
            calendar.add(Calendar.DAY_OF_YEAR, -30);
            Date weekBeforeDate = calendar.getTime();
            String formattedWeekBeforeDate = getFormatter(REQUEST_DATE_FORMAT).format(weekBeforeDate);
            JSONObject objectMain = new JSONObject();
            try { //  objectMain.put("device_serial_no", user.getDevice_serial_no());
                objectMain.put("user_id", user.getUserId());
                objectMain.put("limit", 20);
                objectMain.put("offset", 0);
                objectMain.put("start_date", formattedWeekBeforeDate);
                objectMain.put("end_date", formattedCurrentDate);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            NetworkCommunication.getInstance().connect(getActivity(), objectMain, "normal/Alarms/getClearedNormalAlarms", Request.Method.POST, tag_json_obj, this, NetworkConstants.CLEARED_ALARAMS, true, true, user.getToken());
        }
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        try {
            if (object.has("resultObject")) {
                ArrayList<AlarmSectionHeader> headerList = new ArrayList<>();
                Gson g = new Gson();
                AlarmSectionHeader model = g.fromJson(object.toString(), AlarmSectionHeader.class);
                if (model.getChildItems().size() > 0) {
                    rv_cleared_list.setVisibility(View.VISIBLE);
                    tv_Data_not_available.setVisibility(View.GONE);
                    headerList.add(model);
                    AlarmsRecyclerAdapter adapter = new AlarmsRecyclerAdapter(getActivity(), headerList, true);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    rv_cleared_list.setLayoutManager(linearLayoutManager);
                    rv_cleared_list.setHasFixedSize(true);
                    rv_cleared_list.setAdapter(adapter);
                } else {
                    rv_cleared_list.setVisibility(View.GONE);
                    tv_Data_not_available.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

    @Override
    public void onFailure(int requestCode) {

    }
}
