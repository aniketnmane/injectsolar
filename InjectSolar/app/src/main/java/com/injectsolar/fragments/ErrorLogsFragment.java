package com.injectsolar.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.adapter.ErrorLogsViewAdapter;
import com.injectsolar.models.ErrorLogsModel;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class ErrorLogsFragment extends BaseFragment implements NetworkCallback {

    @BindView(R.id.rvErrors)
    protected RecyclerView rv_inverterDataList;
    @BindView(R.id.tv_Data_not_available)
    protected TextView tv_Data_not_available;
    private ErrorLogsViewAdapter adapter;
    private String tag_json_obj = "ErrorLogs";

    @SuppressLint("ValidFragment")
    public ErrorLogsFragment(User user) {
        this.user = user;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.error_list_fragment, container, false);
        ButterKnife.bind(this, view);
        getErrorData();
        getActivity().setTitle("Error Logs");


        // setAdapterListData();
        return view;
    }

    private void getErrorData() {
        Date date = new Date();
        JSONObject object = new JSONObject();
        JSONObject objectMain = new JSONObject();
        try {
            objectMain.put("user_id", user.getUserId());
            object.put("solar", "1");
            object.put("request", objectMain);

        } catch (JSONException e) {
            e.printStackTrace();

        }
        NetworkCommunication.getInstance().connect(getActivity(), object, "/getErrorLog", Request.Method.POST, tag_json_obj, ErrorLogsFragment.this, NetworkConstants.ERROR_LOGS);
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        ArrayList<ErrorLogsModel> errorList = null;
        try {
            if (!TextUtils.isEmpty(object.getString("res_code")) && object.getString("res_code").equalsIgnoreCase("true")) {
                tv_Data_not_available.setVisibility(View.GONE);
                rv_inverterDataList.setVisibility(View.VISIBLE);
                errorList = new ArrayList<>();
                JSONArray finalObject = object.getJSONArray("errors");
                for (int i = 0; i < finalObject.length(); i++) {
                    Gson g = new Gson();
                    ErrorLogsModel model = g.fromJson(finalObject.getJSONObject(i).toString(), ErrorLogsModel.class);
                    errorList.add(model);
                }
                rv_inverterDataList.setLayoutManager(new LinearLayoutManager(getActivity()));
                ErrorLogsViewAdapter adapter = new ErrorLogsViewAdapter(getActivity(), errorList);
                rv_inverterDataList.setAdapter(adapter);

            } else {
                tv_Data_not_available.setVisibility(View.VISIBLE);
                rv_inverterDataList.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int requestCode) {
        tv_Data_not_available.setVisibility(View.VISIBLE);
        rv_inverterDataList.setVisibility(View.GONE);
    }
}
