package com.injectsolar.fragments

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.android.volley.Request
import com.google.gson.Gson
import com.injectsolar.R
import com.injectsolar.activities.BaseActivity
import com.injectsolar.adapter.MPPTStringRecyclerAdapter
import com.injectsolar.models.MPPTStringsInverterModel
import com.injectsolar.models.SectionHeader
import com.injectsolar.models.User
import com.injectsolar.networking.NetworkCallback
import com.injectsolar.networking.NetworkCommunication
import com.injectsolar.networking.NetworkConstants
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class UnclearedAlarmFragment : Fragment(), NetworkCallback {
    private val tag_json_obj = "ErrorLogs"
    private var user: User? = null
    @BindView(R.id.recycler_view)
    protected var rv_mppt_list: RecyclerView? = null
    @BindView(R.id.tv_Data_not_available)
    protected var tv_Data_not_available: TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.alarm_fragment, container, false)
        mPPTData
        ButterKnife.bind(this, view)
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        assert(arguments != null)
        user = arguments!!.getParcelable(BaseActivity.USER)
    }

    //  objectMain.put("device_serial_no", user.getDevice_serial_no());
    private val mPPTData: Unit
        get() {
            val date = Date()
            val childObject = JSONObject()
            val objectMain = JSONObject()
            try { //  objectMain.put("device_serial_no", user.getDevice_serial_no());
                objectMain.put("user_id", user!!.userId)
                childObject.put("solar", "1")
                childObject.put("request", objectMain)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            NetworkCommunication.getInstance().connect(activity, childObject, "/mppt", Request.Method.POST, tag_json_obj, this@ClearedAlarmFragment, NetworkConstants.MPPT)
        }

    override fun onSuccess(jsonObject: JSONObject, requestCode: Int) {
        try {
            if (!TextUtils.isEmpty(jsonObject.getString("res_code")) && jsonObject.getString("res_code").equals("true", ignoreCase = true)) {
                val finalObject = jsonObject.getJSONArray("errors")
                val headerList = ArrayList<SectionHeader>()
                val allInverterList = ArrayList<MPPTStringsInverterModel>()
                for (i in 0 until finalObject.length()) {
                    val invertersList = finalObject.getJSONObject(i).getJSONArray("inverters")
                    for (j in 0 until invertersList.length()) {
                        val g = Gson()
                        val model = g.fromJson(invertersList.getJSONObject(j).toString(), MPPTStringsInverterModel::class.java)
                        allInverterList.add(model)
                    }
                }
                for (j in allInverterList.indices) {
                    val header = SectionHeader(allInverterList[j].mppt, allInverterList[j].inverter_no, j)
                    if (allInverterList[j].mppt.size > 0) headerList.add(header)
                }
                if (headerList.size != 0) {
                    tv_Data_not_available!!.visibility = View.GONE
                    rv_mppt_list!!.visibility = View.VISIBLE
                    val adapter = MPPTStringRecyclerAdapter(activity, headerList, true)
                    val linearLayoutManager = LinearLayoutManager(activity)
                    rv_mppt_list!!.layoutManager = linearLayoutManager
                    rv_mppt_list!!.setHasFixedSize(true)
                    rv_mppt_list!!.adapter = adapter
                } else {
                    tv_Data_not_available!!.visibility = View.VISIBLE
                    rv_mppt_list!!.visibility = View.GONE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onFailure(requestCode: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

