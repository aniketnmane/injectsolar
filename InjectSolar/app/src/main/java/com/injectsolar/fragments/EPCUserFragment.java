package com.injectsolar.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.activities.BaseActivity;
import com.injectsolar.adapter.TabAdapter;
import com.injectsolar.models.EPCDashboardCountModel;
import com.injectsolar.models.EPCDeviceInfo;
import com.injectsolar.models.InverterInfoModel;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EPCUserFragment extends BaseFragment implements NetworkCallback, AdapterView.OnItemSelectedListener {
    @BindView(R.id.tabLayout)
    protected TabLayout tabLayout;
    @BindView(R.id.viewPager)
    protected ViewPager viewPager;
    private String tag_json_obj = "EPCUser";
    private ArrayList<InverterInfoModel> inverterInfoModelList;
    @BindView(R.id.tvNoOfProjectsValue)
    protected TextView tvNoOfProjectsValue;
    @BindView(R.id.tvTotalDevicesValue)
    protected TextView tvTotalDevicesValue;
    @BindView(R.id.tvTotalSolarCapcity)
    protected TextView tvTotalSolarCapcity;
    @BindView(R.id.tvTotalSolarCapcityValue)
    protected TextView tvTotalSolarCapcityValue;
    @BindView(R.id.tvActiveDevicesValue)
    protected TextView tvActiveDevicesValue;
    @BindView(R.id.tvInActiveDevicesValue)
    protected TextView tvInActiveDevicesValue;
    @BindView(R.id.tvTotalCO2EmmisionValue)
    protected TextView tvTotalCO2EmmisionValue;
    @BindView(R.id.tvTotalCO2Emmision)
    protected TextView tvTotalCO2Emmision;
    @BindView(R.id.tvTotalGeneratedEnergyValue)
    protected TextView tvTotalGeneratedEnergyValue;
    @BindView(R.id.tvTotalGeneratedEnergy)
    protected TextView tvTotalGeneratedEnergy;


    @SuppressLint("ValidFragment")
    public EPCUserFragment(User user) {
        this.user = user;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_epc_user, container, false);
        ButterKnife.bind(this, view);
        getDashboardData();
        return view;
    }

    private void getDashboardData() {
        JSONObject object = new JSONObject();
        String url = "epc/Epc/getDashboardData";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.EPC_DASHBOARD_COUNT, true, true, user.getToken());
    }

    private void getDashboardLowerCount() {
        JSONObject object = new JSONObject();
        String url = "epc/Epc/totalEnergyGeneratedEPC";
        NetworkCommunication.getInstance().connect(getContext(), object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.EPC_DASHBOARD_LOWER_COUNT, true, true, user.getToken());
    }

    private void getDashboardDeviceInfo() {
        JSONObject object = new JSONObject();
        String url = "epc/Epc/getDeviceInfo";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.EPC_DEVICE_INFO, true, true, user.getToken());
    }

    private void setDashboardCount(EPCDashboardCountModel model) {
        tvNoOfProjectsValue.setText(model.getTotalusers());
        tvTotalDevicesValue.setText(model.getTotaldevices());
        tvActiveDevicesValue.setText(model.getActivedevices());
        tvInActiveDevicesValue.setText(model.getInactivedevices());
    }

    private void setDashboardLowerCount(EPCDeviceInfo model) {
        String totalDeviceValue = model.getTotalEnergyGenerated() == null ? "" : model.getTotalEnergyGenerated();
        if (!totalDeviceValue.isEmpty()) {
            String[] strEnergyGenerated = totalDeviceValue.split(" ");
            tvTotalGeneratedEnergy.setText(String.format("%s (%s)", getString(R.string.total_enegery_genrated), strEnergyGenerated[1]));
            String energy = strEnergyGenerated[0];
            if (strEnergyGenerated[0].contains(".")) {
                int end = strEnergyGenerated[0].indexOf(".");
                energy = strEnergyGenerated[0].substring(0, end);
            }
            tvTotalGeneratedEnergyValue.setText(energy);
        }

        String solarCapacity = model.getTotalSolarCapacity() == null ? "" : model.getTotalSolarCapacity();
        if (!solarCapacity.isEmpty()) {
            String[] strSolarCapacity = solarCapacity.split(" ");
            tvTotalSolarCapcity.setText(String.format("%s (%s)", getString(R.string.total_solar_capacity), strSolarCapacity[1]));
            String capacity = strSolarCapacity[0];
            if (strSolarCapacity[0].contains(".")) {
                int end = strSolarCapacity[0].indexOf(".");
                capacity = strSolarCapacity[0].substring(0, end);
            }
            tvTotalSolarCapcityValue.setText(capacity);
        }
        String co2Emission = model.getCo2EmmisionSaved() == null ? "" : model.getCo2EmmisionSaved();
        if (!co2Emission.isEmpty()) {
            String[] strCo2Emission = co2Emission.split(" ");
            tvTotalCO2Emmision.setText(String.format("%s (%s)", getString(R.string.co2_emmission_saved), strCo2Emission[1]));
            String coEmission = strCo2Emission[0];
            if (strCo2Emission[0].contains(".")) {
                int end = strCo2Emission[0].indexOf(".");
                coEmission = strCo2Emission[0].substring(0, end);
            }
            tvTotalCO2EmmisionValue.setText(coEmission);
        }
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        try {
            switch (requestCode) {
                case NetworkConstants.EPC_DASHBOARD_COUNT:
                    JSONObject mainObject = object.getJSONObject("resultObject");
                    if (mainObject.has("dashboardCount")) {
                        JSONObject countObject = mainObject.getJSONObject("dashboardCount");
                        Gson g = new Gson();
                        EPCDashboardCountModel model = g.fromJson(countObject.toString(), EPCDashboardCountModel.class);
                        setDashboardCount(model);
                        getDashboardLowerCount();
                    }
                    break;
                case NetworkConstants.EPC_DASHBOARD_LOWER_COUNT:
                    if (object.optJSONObject("resultObject") != null) {
                        JSONObject countObject = object.getJSONObject("resultObject");
                        Gson g = new Gson();
                        EPCDeviceInfo model = g.fromJson(countObject.toString(), EPCDeviceInfo.class);
                        setDashboardLowerCount(model);
                        getDashboardDeviceInfo();
                    }
                    break;
                case NetworkConstants.EPC_DEVICE_INFO:
                    if (object.has("resultObject")) {
                        JSONObject countObject = object.getJSONObject("resultObject");
                        Gson g = new Gson();
                        EPCDeviceInfo model = g.fromJson(countObject.toString(), EPCDeviceInfo.class);
                        setDashboardLowerCount(model);
                        navigateToFragment();
                    }
                    break;
            }
        } catch (JSONException e) {
            Log.e("", e.getMessage());
        }
    }


    private void navigateToFragment() {
        tabLayout.setupWithViewPager(null);
        TabAdapter adapter = new TabAdapter(getActivity().getSupportFragmentManager());
        Fragment normalReport = new EPCNormalUserFragment();
        Bundle b = new Bundle();
        b.putParcelable(BaseActivity.USER, Parcels.wrap(user));
        normalReport.setArguments(b);
        Fragment wmsReport = new EPCWMSUserFragment();
        wmsReport.setArguments(b);
        adapter.addFragment(normalReport, "NormalReport");
        adapter.addFragment(wmsReport, "WMSReport");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onFailure(int requestCode) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        InverterInfoModel model = inverterInfoModelList.get(position);
        navigateToFragment();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*public User getUser() {
        return user;
    }*/
}