package com.injectsolar.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import com.injectsolar.R;

import java.util.Calendar;

@SuppressLint("ValidFragment")
public class MyDatePickerFragment extends DialogFragment {
    TextView textView;
    private OnDateSelected interfaceDate;


    @SuppressLint("ValidFragment")
    public MyDatePickerFragment(OnDateSelected interfaceDate) {
        this.interfaceDate = interfaceDate;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        textView = getActivity().findViewById(getArguments().getInt("view_id", 0));
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), R.style.DialogTheme, dateSetListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener dateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    String modMonth, modDay = "";
                    if (Integer.toString(view.getMonth()).length() == 1) {
                        modMonth = "0" + Integer.toString(view.getMonth() + 1);
                    } else
                        modMonth = Integer.toString(view.getMonth() + 1);
                    if (Integer.toString(view.getDayOfMonth()).length() == 1) {
                        modDay = "0" + Integer.toString(view.getDayOfMonth());
                    } else
                        modDay = Integer.toString(view.getDayOfMonth());
                    String selectedDate = view.getYear() + "-" +
                            (modMonth) + "-" + modDay;
                    textView.setText(selectedDate);
                    interfaceDate.onDateSelected(selectedDate);
                }
            };
}