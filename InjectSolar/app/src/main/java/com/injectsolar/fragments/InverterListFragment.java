package com.injectsolar.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.activities.InverterDetailActivity;
import com.injectsolar.adapter.InverterListViewAdapter;
import com.injectsolar.models.InverterDataModel;
import com.injectsolar.models.InverterInfoModel;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;
import com.injectsolar.utils.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 27/01/2018.
 */

@SuppressLint("ValidFragment")
public class InverterListFragment extends BaseFragment implements NetworkCallback {
    private User user = null;
    @BindView(R.id.rv_inverterDataList)
    RecyclerView rv_inverterDataList;
    private InverterListViewAdapter adapter;
    private String tag_json_obj = "";
    private ArrayList<InverterDataModel> inverterDataList;
    private InverterInfoModel inverterInfo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ValidFragment")
    public InverterListFragment(User user) {
        this.user = user;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inverter_data_fragment, container, false);
        ButterKnife.bind(this, view);
        getInverterListData();
        return view;
    }

    private void getInverterListData() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER) ? "normal/Report/getInverterDetails" : "/wms/Wms/getInverterDetails";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, this, NetworkConstants.INVERTER_LIST_INFO, true, true, user.getToken());
    }

    private void setAdapterListData(final ArrayList<InverterDataModel> arrayList) {
        rv_inverterDataList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new InverterListViewAdapter(getContext(), arrayList);
        rv_inverterDataList.setAdapter(adapter);
        rv_inverterDataList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), rv_inverterDataList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        Intent intent = new Intent(getActivity(), InverterDetailActivity.class);
                        intent.putExtra("User", Parcels.wrap(user));
                        intent.putExtra(InverterDetailActivity.INVERTER_API_NAME, arrayList.get(position).getInv_name());
                        String inverterName="Inverter-"+arrayList.get(position).getInv_no()+": "+arrayList.get(position).getInv_name();
                        intent.putExtra(InverterDetailActivity.INVERTER_DISPLAY_NAME,inverterName);
                        intent.putExtra(InverterDetailActivity.CURRENT_POWER, arrayList.get(position).getCurrentPower());
                        intent.putExtra(InverterDetailActivity.TODAYS_ENERGY, arrayList.get(position).getTodayGeneration());
                        intent.putExtra(InverterDetailActivity.LIFETIME_ENERGY, arrayList.get(position).getTotalGeneration());
                        intent.putExtra(InverterDetailActivity.DEVICE_SERIAL_NO, arrayList.get(position).getDeviceSerialNo());
                        intent.putExtra(InverterDetailActivity.DEVICE_SERIAL_NO, arrayList.get(position).getDeviceSerialNo());
                        intent.putExtra(InverterDetailActivity.DEVICE_SERIAL_NO, arrayList.get(position).getDeviceSerialNo());
                        intent.putExtra(InverterDetailActivity.INVERTER_NAME, arrayList.get(position).getInv_name());
                        startActivity(intent);
                        setSelectedModel(arrayList.get(position));
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }

    public void setSelectedModel(InverterDataModel model) {
        selectedModel = model;
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        switch (requestCode) {
            case NetworkConstants.INVERTER_LIST_INFO:
                try {
                    if (object.has("resultObject")) {
                        inverterDataList = new ArrayList<>();
                        JSONArray inverterResponseList = object.getJSONArray("resultObject");
                        if (inverterDataList != null) {
                            for (int i = 0; i < inverterResponseList.length(); i++) {
                                Gson g = new Gson();
                                InverterDataModel im = g.fromJson(inverterResponseList.getJSONObject(i).toString(), InverterDataModel.class);
                                inverterDataList.add(im);
                            }
                            setAdapterListData(inverterDataList);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onFailure(int requestCode) {

    }

}
