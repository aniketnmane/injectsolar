package com.injectsolar.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.activities.BaseActivity;
import com.injectsolar.activities.NavigationDrawerMainActivity;
import com.injectsolar.adapter.EPCUserRecyclerAdapter;
import com.injectsolar.models.EPCUserSectionHeader;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;
import com.injectsolar.utils.RecyclerItemClickListener;

import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EPCNormalUserFragment extends BaseFragment implements NetworkCallback {
    private String tag_json_obj = "ErrorLogs";
    private User user;
    @BindView(R.id.recycler_view)
    protected RecyclerView rvEPCUserList;
    private ArrayList<EPCUserSectionHeader> headerList = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = Parcels.unwrap(getArguments().getParcelable(BaseActivity.USER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.epc_wms_normal_fragment, container, false);
        ButterKnife.bind(this, view);
        getDashboardLowerCount();
        return view;
    }

    private void getDashboardLowerCount() {
        try {
            JSONObject object = new JSONObject();
            object.put("search", "");
            object.put("role", "4");
            object.put("limit", "100");
            object.put("offset", "0");
            String url = "epc/Epc/getDeviceDataOfUsers";
            NetworkCommunication.getInstance().connect(this.getContext(), object, url, Request.Method.POST, tag_json_obj, this, NetworkConstants.EPC_DEVICE_DATA_USER, true, true, user.getToken());
        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
    }

    private void getNormalUserLogin(String userId) {
        try {
            JSONObject object = new JSONObject();
            object.put("user_id", userId);
            object.put("isview", "true");
            String url = "admin/Admin/loginViewPortal";
            NetworkCommunication.getInstance().connect(this.getContext(), object, url, Request.Method.POST, tag_json_obj, this, NetworkConstants.EPC_NORMAL_USER_LOGIN, true, true, user.getToken());
        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        if (requestCode == NetworkConstants.EPC_NORMAL_USER_LOGIN) {
            try {
                user = NetworkCommunication.getInstance().gson.fromJson(object.getJSONObject("resultObject").toString(), User.class);
                user.setUserName(user.getLogin_id());
                Intent intent = new Intent(getActivity(), NavigationDrawerMainActivity.class);
                intent.putExtra("User", Parcels.wrap(user));
                startActivity(intent);
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
        } else {
            try {
                if (object.optJSONArray("resultObject") != null) {
                    if (object.has("resultObject")) {
                        headerList = new ArrayList<>();
                        Gson g = new Gson();
                        EPCUserSectionHeader model = g.fromJson(object.toString(), EPCUserSectionHeader.class);
                        if (model.getChildItems().size() > 0) {
                            headerList.add(model);
                            setAdapter(headerList);
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("", e.getMessage());
            }
        }

    }

    @Override
    public void onFailure(int requestCode) {

    }

    private void setAdapter(ArrayList<EPCUserSectionHeader> headerList) {
        EPCUserRecyclerAdapter adapter = new EPCUserRecyclerAdapter(getActivity(), headerList, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
       // linearLayoutManager.setOrientation(getActivity(), LinearLayoutManager.HORIZONTAL);
        rvEPCUserList.setLayoutManager(linearLayoutManager);
        rvEPCUserList.setHasFixedSize(true);
        rvEPCUserList.setAdapter(adapter);
        rvEPCUserList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), rvEPCUserList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String loginId = headerList.get(0).getChildList().get(position-1).getId();
                        getNormalUserLogin(loginId);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
    }
}
