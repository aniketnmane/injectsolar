package com.injectsolar.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.activities.BaseActivity;
import com.injectsolar.adapter.MPPTStringRecyclerAdapter;
import com.injectsolar.models.InverterInfoModel;
import com.injectsolar.models.MPPTStringsInverterModel;
import com.injectsolar.models.SectionHeader;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.injectsolar.activities.BaseActivity.INVERTER_INFO;
import static com.injectsolar.activities.BaseActivity.USER;

public class MPPTFragment extends Fragment implements NetworkCallback {

    private String tag_json_obj = "ErrorLogs";
    private User user;
    @BindView(R.id.recycler_view)
    protected RecyclerView rv_mppt_list;
    @BindView(R.id.tv_Data_not_available)
    protected TextView tv_Data_not_available;
    private InverterInfoModel inverterInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mppt_fragment, container, false);
        getMPPTData();
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        user = Parcels.unwrap(getArguments().getParcelable(BaseActivity.USER));
        inverterInfo = getArguments().getParcelable(INVERTER_INFO);
    }

    private void getMPPTData() {
        JSONObject objectMain = new JSONObject();
        try {
            objectMain.put("inv_id", inverterInfo.getId());
            objectMain.put("sub_device_id", inverterInfo.getSub_device_id());
        } catch (JSONException e) {
            e.printStackTrace();

        }
        String url = user.getRole().equalsIgnoreCase(HomeFragment.NORMAL_USER) ? "normal/Mppt/getMpptData" : "wms/Mppt/getMpptData";
        NetworkCommunication.getInstance().connect(getActivity(), objectMain, url, Request.Method.POST, tag_json_obj, MPPTFragment.this, NetworkConstants.MPPT, true, true, user.getToken());
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        try {
            if (object.has("resultObject")) {
                JSONArray resultObjectArray = object.getJSONArray("resultObject");
                ArrayList<SectionHeader> headerList = new ArrayList<>();
                ArrayList<MPPTStringsInverterModel> allInverterList = new ArrayList<>();
                for (int i = 0; i < resultObjectArray.length(); i++) {
                    Gson g = new Gson();
                    MPPTStringsInverterModel model = g.fromJson(resultObjectArray.getJSONObject(i).toString(), MPPTStringsInverterModel.class);
                    allInverterList.add(model);
                }
                if (allInverterList.size() > 0) {
                    for (int j = 0; j < allInverterList.size(); j++) {
                        String inverterName = inverterInfo.getInv_name() != null ? inverterInfo.getInv_name() : allInverterList.get(j).getInverter_no();
                        SectionHeader header = new SectionHeader(allInverterList.get(j).getMppt(), inverterName, j);
                        if (allInverterList.get(j).getMppt().size() > 0)
                            headerList.add(header);
                    }
                }
                if (headerList.size() != 0) {
                    tv_Data_not_available.setVisibility(View.GONE);
                    rv_mppt_list.setVisibility(View.VISIBLE);
                    MPPTStringRecyclerAdapter adapter = new MPPTStringRecyclerAdapter(getActivity(), headerList, true);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    rv_mppt_list.setLayoutManager(linearLayoutManager);
                    rv_mppt_list.setHasFixedSize(true);
                    rv_mppt_list.setAdapter(adapter);
                } else {
                    tv_Data_not_available.setVisibility(View.VISIBLE);
                    rv_mppt_list.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int requestCode) {

    }
}