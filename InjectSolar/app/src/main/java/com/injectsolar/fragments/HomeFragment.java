package com.injectsolar.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;
import com.injectsolar.R;
import com.injectsolar.activities.InverterDetailActivity;
import com.injectsolar.adapter.HorizontalInverterStatusAdapter;
import com.injectsolar.adapter.HorizontalRecyclerViewAdapter;
import com.injectsolar.application.ApplicationController;
import com.injectsolar.models.DashboardDataModel;
import com.injectsolar.models.DeviceStatus;
import com.injectsolar.models.InverterStatus;
import com.injectsolar.models.User;
import com.injectsolar.networking.NetworkCallback;
import com.injectsolar.networking.NetworkCommunication;
import com.injectsolar.networking.NetworkConstants;
import com.injectsolar.utils.DayAxisValueFormatter;
import com.injectsolar.utils.MyAxisValueFormatter;
import com.injectsolar.utils.MyCustomXAxisValueFormatter;
import com.injectsolar.utils.XYMarkerView;
import com.injectsolar.views.custom_views.MyMarkerView;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.injectsolar.activities.InverterDetailActivity.INVERTER_NAME;

@SuppressLint("ValidFragment")
public class HomeFragment extends BaseFragment implements SeekBar.OnSeekBarChangeListener,
        OnChartGestureListener, OnChartValueSelectedListener, View.OnClickListener, DatePickerDialog.OnDateSetListener, NetworkCallback, OnDateSelected {
    @BindView(R.id.chart_line)
    LineChart mChart;
    private SeekBar mSeekBarX, mSeekBarY;
    private TextView tvX, tvY;
    @BindView(R.id.chart_bar)
    protected BarChart mBarChart;
    @BindView(R.id.tvStartDate)
    TextView tvStartDate;
    @BindView(R.id.tv_day)
    TextView tv_day;
    @BindView(R.id.tv_month)
    TextView tv_month;
    @BindView(R.id.tv_year)
    TextView tv_year;
    @BindView(R.id.tv_lifetime)
    TextView tv_lifetime;
    @BindView(R.id.tv_plant_capacity_value)
    TextView tv_plant_capacity_value;
    @BindView(R.id.tv_plant_capacity_lbl)
    TextView tv_plant_capacity_lbl;
    @BindView(R.id.tv_today_energy_value)
    TextView tv_today_energy_value;
    @BindView(R.id.tv_today_energy_lbl)
    TextView tv_today_energy_lbl;
    @BindView(R.id.tv_lifetime_revenue_value)
    TextView tv_lifetime_revenue_value;
    @BindView(R.id.tv_lifetime_revenue_lbl)
    TextView tv_lifetime_revenue_lbl;
    @BindView(R.id.tv_current_power_value)
    TextView tv_current_power_value;
    @BindView(R.id.tv_current_power_lbl)
    TextView tv_current_power_lbl;
    @BindView(R.id.tv_lifetime_energy_value)
    TextView tv_lifetime_energy_value;
    @BindView(R.id.tv_lifetime_energy_lbl)
    TextView tv_lifetime_energy_lbl;
    @BindView(R.id.tv_co_2_saving_value)
    TextView tv_co_2_saving_value;
    @BindView(R.id.tv_co_2_saving_lbl)
    TextView tv_co_2_saving_lbl;
    @BindView(R.id.tv_inverterName)
    TextView tv_inverterName;
    @BindView(R.id.ll1)
    LinearLayout ll1;
    @BindView(R.id.ll2)
    LinearLayout ll2;
    @BindView(R.id.llInverterStatus)
    LinearLayout llInverterStatus;
    @BindView(R.id.id_rv1)
    RecyclerView rv1;
    @BindView(R.id.id_rv2)
    RecyclerView rv2;
    private DatePicker dpResult;
    @BindView(R.id.llWMSData)
    LinearLayout llWMSData;
    @BindView(R.id.tvIrradianceValue)
    TextView tvIrradianceValue;
    @BindView(R.id.tvInsolationValue)
    TextView tvInsolationValue;
    @BindView(R.id.tvPRValue)
    TextView tvPRValue;
    @BindView(R.id.tvCUFValue)
    TextView tvCUFValue;
    private static int year, month, day;
    private String tag_json_obj = "JSON";
    static final int DATE_DIALOG_ID = 999;
    public static final String WMS_USER = "3";
    public static final String EPC_USER = "2";
    public static final String NORMAL_USER = "4";
    public static final String POWER = "00";
    public static final String IRRADIANCE = "11";
    private float maxValue = 0;
    private float maxValueSet2 = 0;
    private int selectedViewId;

    private boolean flagFrom = false;
    private String TAG = HomeFragment.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("ValidFragment")
    public HomeFragment(User user) {
        this.user = user;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);
        selectedViewId = R.id.tv_day;
        setOnClickListeners();
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Calendar calendar = Calendar.getInstance();
        setUIDateMonthYear(getFormatter().format(calendar.getTime()));
        showHideData();
        return view;
    }

    private String str_inverterDisplayName = null;

    private void showRVDevice(List<DeviceStatus> deviceList) {
        HorizontalRecyclerViewAdapter adapter = new HorizontalRecyclerViewAdapter(deviceList);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv1.setLayoutManager(horizontalLayoutManager);
        horizontalLayoutManager.setSmoothScrollbarEnabled(true);
        rv1.setAdapter(adapter);
    }

    private void showRVInverter(List<InverterStatus> deviceList) {
        HorizontalInverterStatusAdapter adapter = new HorizontalInverterStatusAdapter(deviceList);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv2.setLayoutManager(horizontalLayoutManager);
        horizontalLayoutManager.setSmoothScrollbarEnabled(true);
        rv2.setAdapter(adapter);
    }

    private void showHideData() {
        Bundle bundle = getArguments();
        String str_current_power = null, str_todays_energy = null, str_lifetime = null;
        if (bundle != null) {
            flagFrom = bundle.getBoolean(InverterDetailActivity.FLAG_FROM, false);
            str_current_power = bundle.getString(InverterDetailActivity.CURRENT_POWER);
            str_todays_energy = bundle.getString(InverterDetailActivity.TODAYS_ENERGY);
            str_lifetime = bundle.getString(InverterDetailActivity.LIFETIME_ENERGY);
            str_inverterDisplayName = bundle.getString(InverterDetailActivity.INVERTER_DISPLAY_NAME);
        }
        if (flagFrom) {
            llInverterStatus.setVisibility(View.GONE);
            ll1.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, 0);
            params.setMargins(5, 0, 5, 0);
            params.weight = 1.2f;
            ll2.setLayoutParams(params);
            tv_inverterName.setVisibility(View.VISIBLE);
            tv_inverterName.setTextColor(getActivity().getColor(R.color.colorPrimaryDark));
            tv_inverterName.setText(str_inverterDisplayName);

            if (!TextUtils.isEmpty(str_current_power) && !TextUtils.isEmpty(str_todays_energy) && !TextUtils.isEmpty(str_lifetime)) {
                tv_current_power_lbl.setText(new StringBuilder().append(getString(R.string.current_power)).append(" (kW)").toString());
                tv_current_power_value.setText(str_current_power);
                tv_lifetime_energy_lbl.setText(new StringBuilder().append(getString(R.string.todays_energy)).append(" (KWh)").toString());
                tv_lifetime_energy_value.setText(str_todays_energy);
                tv_co_2_saving_lbl.setText(new StringBuilder().append(getString(R.string.lifetime_energy)).append(" (KWh)").toString());
                tv_co_2_saving_value.setText(str_lifetime);
            }
            getDashBoardData();
        } else {
            getDeviceList();
            llInverterStatus.setVisibility(View.VISIBLE);
            ll1.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, 0);
            params.setMargins(5, 0, 5, 0);
            params.weight = 1.0f;
            ll1.setLayoutParams(params);
            ll2.setLayoutParams(params);
            tv_inverterName.setVisibility(View.GONE);
        }

    }

    private void setUIDateMonthYear(String date) {
        tvStartDate.setText(date);
    }

    private void getDeviceList() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equals(NORMAL_USER) ? "normal/Normal/getDeviceStatus" : "wms/Wms/getDeviceStatus";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, HomeFragment.this, NetworkConstants.DEVICE_LIST, true, true, user.getToken());
    }

    private void getInverterList() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equals(NORMAL_USER) ? "normal/Normal/getInverterStatus" : "wms/Wms/getInverterStatus";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, HomeFragment.this, NetworkConstants.INVERTER_DEVICE_LIST, true, true, user.getToken());
    }

    private void getDashboardData() {
        JSONObject object = new JSONObject();
        String url = user.getRole().equals(NORMAL_USER) ? "normal/Normal/getDashboardData" : "wms/Wms/getDashboardData";
        NetworkCommunication.getInstance().connect(getActivity(), object, url, Request.Method.GET, tag_json_obj, HomeFragment.this, NetworkConstants.DASHBOARD_DATA, true, true, user.getToken());
    }

    private void getGraphData(String date, int month, int year, int type) {
        try {
            switch (type) {
                case NetworkConstants.TODAYS_GRAPH_DATA:
                    JSONObject objectMain = new JSONObject();
                    objectMain.put("date", date);
                    objectMain.put("user_id", user.getUserName());
                    if (!flagFrom)
                        NetworkCommunication.getInstance().connect(getActivity(), objectMain, "graph/Graph/cumulative_today_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.TODAYS_GRAPH_DATA, true, true, user.getToken());
                    else {
                        JSONObject objectInverterData = getInverterRequestJSON(month, year, type, date);
                        NetworkCommunication.getInstance().connect(getActivity(), objectInverterData, "graph/Graph/inverter_today_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.TODAYS_GRAPH_DATA, true, true, user.getToken());
                    }
                    break;
                case NetworkConstants.MONTH_GRAPH_DATA:
                    JSONObject object = new JSONObject();
                    objectMain = new JSONObject();
                    objectMain.put("month", month);
                    objectMain.put("year", year);
                    objectMain.put("user_id", user.getUserName());
                    if (!flagFrom)
                        NetworkCommunication.getInstance().connect(getActivity(), objectMain, "graph/Graph/cumulative_month_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.MONTH_GRAPH_DATA, true, true, user.getToken());
                    else {
                        JSONObject objectInverterData = getInverterRequestJSON(month, year, type, date);
                        NetworkCommunication.getInstance().connect(getActivity(), objectInverterData, "graph/Graph/inverter_month_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.MONTH_GRAPH_DATA, true, true, user.getToken());
                    }
                    break;
                case NetworkConstants.YEAR_GRAPH_DATA:
                    objectMain = new JSONObject();
                    objectMain.put("user_id", user.getUserName());
                    objectMain.put("year", year);
                    if (!flagFrom)
                        NetworkCommunication.getInstance().connect(getActivity(), objectMain, "graph/Graph/cumulative_year_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.YEAR_GRAPH_DATA, true, true, user.getToken());
                    else {
                        Bundle bundle = getArguments();
                        String strDeviceData = bundle.getString(InverterDetailActivity.INVERTER_SERIAL_SUBDEVICE_NO);
                        objectMain = new JSONObject(strDeviceData);
                        objectMain.put("user_id", user.getUserName());
                        objectMain.put("year", year);
                        NetworkCommunication.getInstance().connect(getActivity(), objectMain, "graph/Graph/inverter_year_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.YEAR_GRAPH_DATA, true, true, user.getToken());
                    }
                    break;
                case NetworkConstants.LIFE_TIME:
                    objectMain = new JSONObject();
                    objectMain.put("user_id", user.getUserName());
                    objectMain.put("start_year", "2018");
                    NetworkCommunication.getInstance().connect(getActivity(), objectMain, "graph/Graph/cumulative_life_time_graph", Request.Method.POST, tag_json_obj, HomeFragment.this, NetworkConstants.LIFE_TIME, true, true, user.getToken());
                    break;
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

    }

    private JSONObject getInverterRequestJSON(int month, int year, int type, String date) {
        try {
            Bundle bundle = getArguments();
            String strDeviceData = bundle.getString(InverterDetailActivity.INVERTER_SERIAL_SUBDEVICE_NO);
            JSONObject objectInverterData = new JSONObject(strDeviceData);
            if (type == NetworkConstants.MONTH_GRAPH_DATA) {
                objectInverterData.put("month", month);
                objectInverterData.put("year", year);
            } else {
                objectInverterData.put("date", date);
            }
            objectInverterData.put("user_id", user.getUserName());
            return objectInverterData;
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    private void graphInitialization(JSONObject mainObject, boolean isDay, int type) {
        maxValue = 0;
        mChart = null;
        mChart = getActivity().findViewById(R.id.chart_line);
        mChart.zoomOut();
        mChart.fitScreen();
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        // mChart.setScaleXEnabled(true);
        // mChart.setScaleYEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        // set an alternative background color
        // mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
        // x-axis limit line
        LimitLine llXAxis = new LimitLine(10f, "");
        llXAxis.setLineWidth(0f);
        // llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        //   xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line
        // Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Typefaces.FONT_LATO_REGULAR);
        LimitLine ll1 = new LimitLine(150f, "");
        ll1.setLineWidth(0f);
        //  ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
        ll1.setTextSize(10f);
        ll1.setTypeface(ApplicationController.fntLatoRegular);

        LimitLine ll2 = new LimitLine(0f, "");
        ll2.setLineWidth(0f);
        //   ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);
        ll2.setTypeface(ApplicationController.fntLatoRegular);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(ll1);
        leftAxis.addLimitLine(ll2);

        leftAxis.setAxisMinimum(0f);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(false);
        mChart.getAxisRight().setEnabled(false);

        // add data
        JSONArray array = null;
        JSONArray array2 = null;
        YAxis rightAxis = mChart.getAxisRight();
        try {
            if (user.getRole().equals(WMS_USER) && mainObject.optJSONObject("resultObject") != null) {
                mChart.getAxisRight().setEnabled(true);
                rightAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
                rightAxis.addLimitLine(ll1);
                rightAxis.addLimitLine(ll2);
                rightAxis.setAxisMinimum(0f);
                //leftAxis.setYOffset(20f);
                rightAxis.enableGridDashedLine(10f, 10f, 0f);
                rightAxis.setDrawZeroLine(false);
                // limit lines are drawn behind data (and not on top)
                rightAxis.setDrawLimitLinesBehindData(false);

                JSONObject resultObject = mainObject.getJSONObject("resultObject");
                array = resultObject.getJSONArray("power_generation");
                array2 = resultObject.getJSONArray("IrrData");
            } else {
                array = mainObject.getJSONArray("resultObject");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (array != null && array.length() > 0) {
            setData(10, mainObject, isDay, type);
            mChart.resetZoom();
            mChart.animateX(2500);
            float value = (float) (maxValue * 1.2);
            leftAxis.setAxisMaximum(value);
            // get the legend (only possible after setting data)
            Legend l = mChart.getLegend();
            // modify the legend ...
            l.setForm(Legend.LegendForm.LINE);
        }
        if (user.getRole().equals(WMS_USER) && array2 != null && array2.length() > 0) {
            float value = (float) (maxValueSet2 * 1.5);
            rightAxis.setAxisMaximum(value);
            // get the legend (only possible after setting data)
            Legend l = mChart.getLegend();
            // modify the legend ...
            l.setForm(Legend.LegendForm.DEFAULT);
        }
        // // dont forget to refresh the drawing
        // mChart.invalidate();
    }

    private void setOnClickListeners() {
        tvStartDate.setOnClickListener(this);
        tv_day.setOnClickListener(this);
        tv_month.setOnClickListener(this);
        tv_year.setOnClickListener(this);
        tv_lifetime.setOnClickListener(this);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        tvX.setText("" + (mSeekBarX.getProgress() + 1));
        tvY.setText("" + (mSeekBarY.getProgress()));

        // setData(mSeekBarX.getProgress() + 1, mSeekBarY.getProgress());

        // redraw
        mChart.invalidate();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }

    private void setData(float range, JSONObject mainObject, boolean isDay, int type) {
        mChart.clear();

        ArrayList<Entry> values = new ArrayList<>();
        ArrayList<Entry> values2 = new ArrayList<>();
        switch (type) {
            case NetworkConstants.TODAYS_GRAPH_DATA:
                try {
                    if (user.getRole().equals(WMS_USER) && mainObject.optJSONObject("resultObject") != null) {
                        JSONObject resulObject = mainObject.getJSONObject("resultObject");
                        if (resulObject.has("power_generation") && resulObject.getJSONArray("power_generation").length() != 0) {
                            JSONArray array = resulObject.getJSONArray("power_generation");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                String keyJSON = object.getString("date_time");
                                if (!TextUtils.isEmpty(keyJSON)) {
                                    String[] keys = keyJSON.split(" ");
                                    Date dateFormatted = formatter.parse(keyJSON);
                                    float val = Float.parseFloat(object.getString("power_generation"));
                                    if (val > maxValue)
                                        maxValue = val;
                                    values.add(new Entry(dateFormatted.getTime(), val, keys[1] + " PowerGeneration(KW)"));
                                }
                            }
                        }
                        if (resulObject.has("IrrData") && resulObject.getJSONArray("IrrData").length() != 0) {
                            JSONArray array = resulObject.getJSONArray("IrrData");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                String keyJSON = object.getString("date_time");
                                if (!TextUtils.isEmpty(keyJSON)) {
                                    String[] keys = keyJSON.split(" ");
                                    Date dateFormatted = formatter.parse(keyJSON);
                                    float val = Float.parseFloat(object.getString("value"));
                                    if (val > maxValueSet2)
                                        maxValueSet2 = val;
                                    values2.add(new Entry(dateFormatted.getTime(), val, keys[1] + " Irradiance(w/m2)"));
                                }
                            }
                        }
                    } else if (mainObject.has("resultObject") && mainObject.getJSONArray("resultObject").length() != 0) {
                        JSONArray array = mainObject.getJSONArray("resultObject");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            String keyJSON = object.getString("date_time");
                            if (!TextUtils.isEmpty(keyJSON)) {
                                String[] keys = keyJSON.split(" ");
                                Date dateFormatted = formatter.parse(keyJSON);
                                String[] parts = keyJSON.split("\\ "); // String array, each element is text between dots
                                String key = parts[1];
                                String strXAxis = parts[1];
                                String updateXis = strXAxis.replace(":", ".");
                                float val = Float.parseFloat(object.getString("power_generation"));
                                if (val > maxValue)
                                    maxValue = val;
                                values.add(new Entry(dateFormatted.getTime(), val, keys[1] + " PowerGeneration(KW)"));
                            }
                        }
                    }
                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
                break;
            case NetworkConstants.MONTH_GRAPH_DATA:
                JSONObject categoryJSONObj = null;
                try {
                    if (mainObject.has("resultObject") && mainObject.getJSONArray("resultObject").length() != 0) {
                        categoryJSONObj = mainObject.getJSONObject("resultObject");
                        // get all keys from categoryJSONObj

                        Iterator<String> iterator = categoryJSONObj.keys();
                        while (iterator.hasNext()) {
                            String key = iterator.next();
                            String[] parts = key.split("\\-"); // String array, each element is text between dots

                            String strXAxis = parts[2];
                            Log.i("TAG", "key:" + key + "--Value::" + categoryJSONObj.optString(key));
                            values.add(new Entry(Integer.parseInt(strXAxis), Integer.parseInt(categoryJSONObj.optString(key)), getResources().getDrawable(R.mipmap.ic_launcher)));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case NetworkConstants.YEAR_GRAPH_DATA:
                try {
                    if (mainObject.has("resultObject") && mainObject.getJSONArray("resultObject").length() != 0) {
                        categoryJSONObj = mainObject.getJSONObject("resultObject");
                        // get all keys from categoryJSONObj

                        Iterator<String> iterator = categoryJSONObj.keys();
                        while (iterator.hasNext()) {
                            String key = iterator.next();
                            String[] parts = key.split("\\ "); // String array, each element is text between dots

                            String strXAxis = parts[1];
                            Log.i("TAG", "key:" + key + "--Value::" + categoryJSONObj.optString(key));
                            values.add(new Entry(Integer.parseInt(strXAxis), Float.parseFloat(categoryJSONObj.optString(key)), getResources().getDrawable(R.mipmap.ic_launcher)));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }

        LineDataSet set1;
        LineDataSet set2;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Power Generation(KW)");
            LineDataSet updatedSet = setCustomisationDataSet(set1, POWER);
            updatedSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(updatedSet); // add the datasets
            if (user.getRole().equals(WMS_USER) && values2.size() > 0) {
                set2 = new LineDataSet(values2, "Irrandiance(w/m2)");
                set2.setAxisDependency(YAxis.AxisDependency.RIGHT);
                LineDataSet updatedSet2 = setCustomisationDataSet(set2, IRRADIANCE);
                dataSets.add(updatedSet2);
                updatedSet2.setDrawCircleHole(false);
                updatedSet2.setDrawCircles(false);
                updatedSet.setDrawCircleHole(false);
                updatedSet2.setDrawFilled(false);
                updatedSet.setDrawFilled(false);
                updatedSet.setDrawCircles(false);
                // create a data object with the datasets
                LineData data = new LineData(dataSets);
                // set data
                mChart.setData(data);
            } else {
                // create a data object with the datasets
                LineData data = new LineData(dataSets);
                // set data
                mChart.setData(data);
            }

        }
    }

    private LineDataSet setCustomisationDataSet(LineDataSet set, String role) {
        set.setDrawIcons(false);
        // set the line to be drawn like this "- - - - - -"
        if (role.equalsIgnoreCase(IRRADIANCE)) {
            set.enableDashedLine(10f, 0f, 0f);
            set.enableDashedHighlightLine(10f, 0f, 0f);
            set.setDrawCircleHole(false);
            set.setDrawFilled(false);
            set.setDrawValues(false);
        } else {
            set.enableDashedLine(10f, 0f, 0f);
            set.enableDashedHighlightLine(10f, 0f, 0f);
            set.setDrawCircleHole(true);
            set.setCircleRadius(3f);
            set.setValueTextSize(0f);
            set.setDrawFilled(true);
        }
        set.setLineWidth(1f);

        set.setFormLineWidth(1f);
        set.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set.setFormSize(15.f);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = null;
            if (role.equalsIgnoreCase(IRRADIANCE)) {
                drawable = ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.fade_blue);
                set.setColor(Color.BLUE);
                set.setCircleColor(Color.BLUE);
            } else {
                drawable = ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.fade_red);
                set.setColor(Color.BLACK);
                set.setCircleColor(Color.BLACK);
            }
            set.setFillDrawable(drawable);
        } else {
            set.setFillColor(Color.BLACK);
        }
        return set;
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            mChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOWHIGH", "low: " + mChart.getLowestVisibleX() + ", high: " + mChart.getHighestVisibleX());
        Log.i("MIN MAX", "xmin: " + mChart.getXChartMin() + ", xmax: " + mChart.getXChartMax() + ", ymin: " + mChart.getYChartMin() + ", ymax: " + mChart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    @Override
    public void onClick(View v) {
        DialogFragment newFragment = null;
        Bundle args;
        int month = 0;
        int year = 0;
        Calendar calendar = null;
        switch (v.getId()) {
            case R.id.tvStartDate:
                switch (selectedViewId) {
                    case R.id.tv_day:
                        newFragment = new MyDatePickerFragment(HomeFragment.this);
                        args = new Bundle();
                        args.putInt("view_id", R.id.tvStartDate);
                        newFragment.setArguments(args);
                        newFragment.show(getActivity().getFragmentManager(), "date picker");
                        break;
                    case R.id.tv_month:
                        monthPicker(Calendar.MONTH);
                        break;
                    case R.id.tv_year:
                        break;
                }
                break;
            case R.id.tv_day:
                if (mChart != null) {
                    mChart.resetZoom();
//                    mChart.clearValues();
                }

                selectView(R.id.tv_day);
                newFragment = new MyDatePickerFragment(HomeFragment.this);
                args = new Bundle();
                args.putInt("view_id", R.id.tvStartDate);
                newFragment.setArguments(args);
                newFragment.show(getActivity().getFragmentManager(), "date picker");
                break;
            case R.id.tv_month:
                selectView(R.id.tv_month);
                calendar = Calendar.getInstance();
                monthPicker(Calendar.MONTH);
                break;
            case R.id.tv_year:
                calendar = Calendar.getInstance();
                month = calendar.get(Calendar.MONTH);
                year = calendar.get(Calendar.YEAR);
                selectView(R.id.tv_year);
                tvStartDate.setText("" + year);
                monthPicker(Calendar.YEAR);
                break;
            case R.id.tv_lifetime:
                selectView(R.id.tv_lifetime);
                getGraphData("", month + 1, year, NetworkConstants.LIFE_TIME);
                break;
        }
    }

    private void setLineBarVisibility(boolean flagLine) {
        if (flagLine) {
            mChart.setVisibility(View.VISIBLE);
            mBarChart.setVisibility(View.GONE);
        } else {
            mChart.setVisibility(View.GONE);
            mBarChart.setVisibility(View.VISIBLE);
        }
    }

    private void selectView(int id) {
        selectedViewId = id;
        if (id == R.id.tv_day || id == R.id.tv_lifetime) {
            switch (id) {
                case R.id.tv_day:
                    tv_day.setBackgroundResource(R.drawable.rounded_corner_right_square_selected);
                    tv_day.setTextColor(Color.WHITE);
                    tv_month.setBackgroundResource(R.drawable.square_border_grey_color);
                    tv_month.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_year.setBackgroundResource(R.drawable.square_border_grey_color);
                    tv_year.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_lifetime.setBackgroundResource(R.drawable.rounded_corner_left_square);
                    tv_lifetime.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case R.id.tv_lifetime:
                    tv_day.setBackgroundResource(R.drawable.rounded_corner_right_square);
                    tv_day.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_month.setBackgroundResource(R.drawable.square_border_grey_color);
                    tv_month.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_year.setBackgroundResource(R.drawable.square_border_grey_color);
                    tv_year.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_lifetime.setBackgroundResource(R.drawable.rounded_corner_left_square_selected);
                    tv_lifetime.setTextColor(Color.WHITE);
                    break;
            }
        } else {
            switch (id) {
                case R.id.tv_month:
                    tv_day.setBackgroundResource(R.drawable.rounded_corner_right_square);
                    tv_day.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_month.setBackgroundResource(R.drawable.square_border_grey_color_selected);
                    tv_month.setTextColor(Color.WHITE);
                    tv_year.setBackgroundResource(R.drawable.square_border_grey_color);
                    tv_year.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_lifetime.setBackgroundResource(R.drawable.rounded_corner_left_square);
                    tv_lifetime.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case R.id.tv_year:
                    tv_day.setBackgroundResource(R.drawable.rounded_corner_right_square);
                    tv_day.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_month.setBackgroundResource(R.drawable.square_border_grey_color);
                    tv_month.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    tv_year.setBackgroundResource(R.drawable.square_border_grey_color_selected);
                    tv_year.setTextColor(Color.WHITE);
                    tv_lifetime.setBackgroundResource(R.drawable.rounded_corner_left_square);
                    tv_lifetime.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setCalendarDate(cal);
    }

    private void setCalendarDate(final Calendar calendar) {
        tvStartDate.setText(getFormatter().format(calendar.getTime()));
    }

    private void getTodaysData() {
        Date currentDate = Calendar.getInstance().getTime();

        String formattedCurrentDate = getFormatter().format(currentDate);
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        getGraphData(formattedCurrentDate, month + 1, year, NetworkConstants.TODAYS_GRAPH_DATA);
    }

    @Override
    public void onSuccess(JSONObject object, int requestCode) {
        DashboardDataModel dashboardModel;
        switch (requestCode) {
            case NetworkConstants.DASHBOARD_DATA:
                try {
                    dashboardModel = NetworkCommunication.getInstance().gson.fromJson(object.getJSONObject("resultObject").toString(), DashboardDataModel.class);
                    String[] strCapacity = dashboardModel.getInstd_capacity().split(" ");
                    tv_plant_capacity_lbl.setText(String.format("%s(%s)", getString(R.string.plant_capacity), strCapacity[1]));
                    tv_plant_capacity_value.setText(strCapacity[0]);
                    String[] strTodaysEnergy = dashboardModel.getTotalTodayEnergyGenerated().split(" ");
                    tv_today_energy_value.setText(strTodaysEnergy[0]);
                    tv_today_energy_lbl.setText(String.format("%s(%s)", getString(R.string.todays_energy), strTodaysEnergy[1]));
                    String[] strEstimatedSaving = dashboardModel.getEstimated_saving().split(" ");
                    tv_lifetime_revenue_value.setText(strEstimatedSaving[0]);
                    tv_lifetime_revenue_lbl.setText(String.format("%s(%s)", getString(R.string.lifetime_revenue), strEstimatedSaving[1]));
                    String[] strTotalEnergy = dashboardModel.getTotalEnergyGenerated().split(" ");
                    tv_lifetime_energy_value.setText(strTotalEnergy[0]);
                    tv_lifetime_energy_lbl.setText(String.format("%s(%s)", getString(R.string.lifetime_energy), strTotalEnergy[1]));
                    String[] strPower = dashboardModel.getTotalCurrentPower().split(" ");
                    tv_current_power_value.setText(strPower[0]);
                    tv_current_power_lbl.setText(String.format("%s(%s)", getString(R.string.current_power), strPower[1]));
                    if (dashboardModel.getCo2EmmisionSaved().contains(" ")) {
                        String[] strCO2 = dashboardModel.getCo2EmmisionSaved().split(" ");
                        tv_co_2_saving_value.setText(strCO2[0]);
                        tv_co_2_saving_lbl.setText(String.format("%s(%s)", getString(R.string.co2_saving), strCO2[1]));
                    } else {
                        tv_co_2_saving_value.setText(dashboardModel.getCo2EmmisionSaved());
                        tv_co_2_saving_lbl.setText(String.format("%s(%s)", getString(R.string.co2_saving), "Kg"));
                    }
                    getTodaysData();
                    if (user.getRole().equalsIgnoreCase(WMS_USER))
                        showWMSData(dashboardModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case NetworkConstants.TODAYS_GRAPH_DATA:
                setLineBarVisibility(true);
                try {
                    graphInitialization(object, false, NetworkConstants.TODAYS_GRAPH_DATA);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case NetworkConstants.YEAR_GRAPH_DATA:
                setLineBarVisibility(false);
                try {
                    barGraphInitialisation(NetworkConstants.YEAR_GRAPH_DATA, object);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case NetworkConstants.MONTH_GRAPH_DATA:
                setLineBarVisibility(false);
                try {
                    barGraphInitialisation(NetworkConstants.MONTH_GRAPH_DATA, object);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case NetworkConstants.LIFE_TIME:
                setLineBarVisibility(false);
                barGraphInitialisation(NetworkConstants.LIFE_TIME, object);
                break;
            case NetworkConstants.DEVICE_LIST:
                ArrayList<DeviceStatus> allDeviceList = new ArrayList<>();
                try {
                    if (object.has("resultObject")) {
                        JSONArray deviceList = object.getJSONArray("resultObject");
                        for (int j = 0; j < deviceList.length(); j++) {
                            Gson g = new Gson();
                            DeviceStatus model = g.fromJson(deviceList.getJSONObject(j).toString(), DeviceStatus.class);
                            allDeviceList.add(model);
                        }
                    }
                } catch (JSONException e) {
                    Log.e("", e.getMessage());
                }
                showRVDevice(allDeviceList);
                getInverterList();
                break;

            case NetworkConstants.INVERTER_DEVICE_LIST:
                ArrayList<InverterStatus> allInverterList = new ArrayList<>();
                try {
                    if (object.has("resultObject")) {
                        JSONArray deviceList = object.getJSONArray("resultObject");
                        for (int j = 0; j < deviceList.length(); j++) {
                            Gson g = new Gson();
                            InverterStatus model = g.fromJson(deviceList.getJSONObject(j).toString(), InverterStatus.class);
                            allInverterList.add(model);
                        }
                    }
                } catch (JSONException e) {
                    Log.e("", e.getMessage());
                }
                showRVInverter(allInverterList);
                getDashBoardData();
                break;
        }
    }

    private void showWMSData(DashboardDataModel model) {
        llWMSData.setVisibility(View.VISIBLE);
        tvIrradianceValue.setText(model.getIrradiation());
        tvInsolationValue.setText(model.getIrradiance());
        tvPRValue.setText(model.getPR());
        tvCUFValue.setText(model.getCUF());
    }

    private void getDashBoardData() {
        if (!flagFrom)
            getDashboardData();
        else
            getTodaysData();
    }

    @Override
    public void onFailure(int requestCode) {
        switch (requestCode) {
            case NetworkConstants.DASHBOARD_DATA:
                break;
            case NetworkConstants.DEVICE_LIST:
                getInverterList();
                break;
            case NetworkConstants.INVERTER_DEVICE_LIST:
                if (!flagFrom)
                    getDashboardData();
                else
                    getTodaysData();
                break;
        }
    }

    @Override
    public void onDateSelected(String selectedDate) {
        String formatter = "yyyy-MM-dd";
        try {
            mChart.clear();
            mChart.invalidate();
            java.text.SimpleDateFormat dateFormatter = new java.text.SimpleDateFormat(formatter);
            Date sDate = dateFormatter.parse(selectedDate);
            String formattedCurrentDate = getFormatter().format(sDate);
            Calendar calendar = Calendar.getInstance();
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            Toast.makeText(getActivity(), "Your selected date is " + formattedCurrentDate, Toast.LENGTH_SHORT).show();
            getGraphData(formattedCurrentDate, month + 1, year, NetworkConstants.TODAYS_GRAPH_DATA);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener)
                            getActivity(), year, month, day);
        }

    }


    public void monthPicker(int type) {
        final Calendar today = Calendar.getInstance();
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        MonthPickerDialog.Builder builder = null;


        if (type == Calendar.MONTH) {
            builder = new MonthPickerDialog.Builder(getActivity(), (selectedMonth, selectedYear) -> {
                selectedMonth = selectedMonth + 1;
                tvStartDate.setText("" + selectedMonth + "/" + selectedYear);
                String monthString = new DateFormatSymbols().getMonths()[selectedMonth - 1];
                Toast.makeText(getActivity(), "You have selected " + monthString + " month.", Toast.LENGTH_SHORT).show();
                getGraphData("", selectedMonth, selectedYear, NetworkConstants.MONTH_GRAPH_DATA);
            }, /* activated number in year */ today.get(Calendar.YEAR), today.get(Calendar.MONTH));

            builder.setActivatedMonth(today.get(Calendar.MONTH))
                    .setMinYear(1990)
                    .setActivatedYear(today.get(Calendar.YEAR))
                    .setMaxYear(2050)
                    .setMinMonth(Calendar.JANUARY)
                    .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                    .build()
                    .show();

        } else if (type == Calendar.YEAR) {

            builder = new MonthPickerDialog.Builder(getActivity(), new MonthPickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(int selectedMonth, int selectedYear) {
                    tvStartDate.setText("" + selectedYear);
                    Toast.makeText(getActivity(), "You have selected " + selectedYear + " year.", Toast.LENGTH_SHORT).show();
                    getGraphData("", selectedMonth + 1, selectedYear, NetworkConstants.YEAR_GRAPH_DATA);
                }
            }, year, 0);

            builder.showYearOnly()
                    .setYearRange(1990, 2030)
                    .build()
                    .show();
        }
    }

    private void setData(int type, JSONObject mainObject) {
        ArrayList<BarEntry> yVals1 = null;
        JSONArray categoryArray = null;
        try {
            if (mainObject.has("resultObject")) {
                categoryArray = mainObject.getJSONArray("resultObject");
                switch (type) {
                    case NetworkConstants.MONTH_GRAPH_DATA:
                        yVals1 = getBarEntryList(categoryArray, "date", "power_generation");
                        break;
                    case NetworkConstants.YEAR_GRAPH_DATA:
                        yVals1 = getBarEntryList(categoryArray, "month", "power_generation");
                        break;
                    case NetworkConstants.LIFE_TIME:
                        yVals1 = getBarEntryList(categoryArray, "year", "power_generation");
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        BarDataSet set1;
        if (mBarChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mBarChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mBarChart.getData().notifyDataChanged();
            mBarChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "Energy Generated(kWh)");

            set1.setDrawIcons(false);

            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(0f);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.9f);

            mBarChart.setData(data);
        }
    }

    private ArrayList<BarEntry> getBarEntryList(JSONArray categoryArray, String dateKey, String genrationKey) {
        ArrayList<BarEntry> yVals = new ArrayList<>();
        try {
            for (int i = 0; i < categoryArray.length(); i++) {
                String dateTime = categoryArray.getJSONObject(i).getString(dateKey);
                String powerGeneration = categoryArray.getJSONObject(i).getString(genrationKey);
                Log.i("TAG", "key:" + dateTime + "--Value::" + powerGeneration);
                float val = Float.parseFloat(powerGeneration);
                if (val > maxValue)
                    maxValue = val;
                yVals.add(new BarEntry(i, val, dateTime));
            }
        } catch (JSONException e) {
            Log.e("", Objects.requireNonNull(e.getMessage()));
        }
        return yVals;
    }

    private void barGraphInitialisation(int type, JSONObject mainObject) {
        maxValue = 0;
        mBarChart.clear();
        mBarChart.invalidate();
        mBarChart.zoomOut();
        mBarChart = getView().findViewById(R.id.chart_bar);
        mBarChart.setOnChartValueSelectedListener(this);

        mBarChart.setDrawBarShadow(false);
        mBarChart.setDrawValueAboveBar(true);

        mBarChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mBarChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mBarChart.setPinchZoom(false);
        mBarChart.setDoubleTapToZoomEnabled(false);
        mBarChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);
        IAxisValueFormatter custom = new MyAxisValueFormatter();
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        YAxis rightAxis = mBarChart.getAxisRight();
        rightAxis.setEnabled(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mBarChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(0f);
        l.setXEntrySpace(4f);
        // l.setExtra(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });
        // l.setCustom(ColorTemplate.VORDIPLOM_COLORS, new String[] { "abc",
        // "def", "ghj", "ikl", "mno" });

        XYMarkerView mv = new XYMarkerView(getActivity(), xAxisFormatter);
        mv.setChartView(mBarChart); // For bounds control
        mBarChart.setMarker(mv); // Set the marker to the chart


        setData(type, mainObject);

        rightAxis.setAxisMaximum((float) (maxValue * 1.2));
        mChart.zoomOut();
        // setting datai


        // mChart.setDrawLegend(false);
    }

}
