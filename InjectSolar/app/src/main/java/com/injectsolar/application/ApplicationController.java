package com.injectsolar.application;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.injectsolar.utils.Typefaces;


/**
 * Created by Aniket on 27/06/2019.
 */
public class ApplicationController extends Application {

    public static Typeface fntLatoBold;
    public static Typeface fntLatoRegular;
    public static Typeface fntMontserratBold;
    public static Typeface fntMontserratRegular;
    private static ApplicationController instance;
    private static final String TAG = ApplicationController.class.getSimpleName();
    public static Context appContext;
    private static ApplicationController applicationController;

    /**
     * Preload typefaces.
     */
    private void preloadTypefaces() {
        Log.d(TAG, "preloadTypefaces");
        fntLatoBold = Typefaces.get(getApplicationContext(), Typefaces.FONT_LATO_BOLD);
        fntLatoRegular = Typefaces.get(getApplicationContext(), Typefaces.FONT_LATO_REGULAR);
        fntMontserratBold = Typefaces.get(getApplicationContext(), Typefaces.FONT_MONTSERRAT_BOLD);
        fntMontserratRegular = Typefaces.get(getApplicationContext(), Typefaces.FONT_MONTSERRAT_REGULAR);
    }

    /* Gets the application context.
     *
     * @return the application context
     */
    public static Context getContext() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        applicationController = this;
        preloadTypefaces();

    }


    private RequestQueue mRequestQueue;


    public static synchronized ApplicationController getInstance() {
        return applicationController;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
