package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.injectsolar.R;
import com.injectsolar.models.MPPTStringsModel;
import com.injectsolar.models.SectionHeader;
import com.injectsolar.views.custom_views.SectionRecyclerViewAdapter;
import com.injectsolar.views.holders.ChildViewHolder;
import com.injectsolar.views.holders.SectionViewHolder;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class MPPTStringRecyclerAdapter extends SectionRecyclerViewAdapter<SectionHeader, MPPTStringsModel, SectionViewHolder, ChildViewHolder> {

    private Context context;
    private boolean isMPPT;

    public MPPTStringRecyclerAdapter(Context context, List<SectionHeader> sectionHeaderItemList, boolean isMPPT) {
        super(context, sectionHeaderItemList);
        this.context = context;
        this.isMPPT = isMPPT;
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.section_item, sectionViewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, childViewGroup, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, SectionHeader sectionHeader) {
        String strInverterName = sectionHeader.getSectionText();
        if (isMPPT) {
            sectionViewHolder.tvInverterNo.setText("MPPT");
        }else {
            sectionViewHolder.tvInverterNo.setText("STRING");
        }
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder childViewHolder, int sectionPosition, int childPosition, MPPTStringsModel child) {
        if (isMPPT) {
            String strInverterName = "MPPT " + (childPosition + 1);
            childViewHolder.tv_mmpt.setText(strInverterName.toUpperCase());
        } else {
            String strInverterName = "String " + (childPosition + 1);
            childViewHolder.tv_mmpt.setText(strInverterName.toUpperCase());
        }

        childViewHolder.tv_v.setText(child.getV());
        childViewHolder.tv_i.setText(child.getI());
        childViewHolder.tv_p.setText(child.getP());
    }
}
