package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;
import com.injectsolar.models.EPCDeviceDataUserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EPCUserListViewAdapter extends RecyclerView.Adapter<EPCUserListViewAdapter.CustomViewHolder> {
    private ArrayList<EPCDeviceDataUserModel> feedItemList;
    private Context mContext;

    public EPCUserListViewAdapter(Context context, ArrayList<EPCDeviceDataUserModel> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.epc_user_list_row, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        EPCDeviceDataUserModel feedItem = feedItemList.get(i);
        int pos = i + 1;
        customViewHolder.tvLoginId.setText(feedItem.getLogin_id());
        customViewHolder.tvCurrentPowerEPC.setText(feedItem.getTotalCurrentPower());
        customViewHolder.tvPlantCapacityEPC.setText( feedItem.getPlant_capacity());
        customViewHolder.tvTodaysGeneration.setText(feedItem.getTotalTodayEnergyGenerated());
        customViewHolder.tvTotalGeneration.setText(feedItem.getTotalTodayEnergyGenerated());
        customViewHolder.tvMonthGeneration.setText(feedItem.getTotalMonthlyEnergyGenerated());
        customViewHolder.tvPlantStatus.setText(feedItem.getDevice_status());
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvLoginId)
        protected TextView tvLoginId;
        @BindView(R.id.tvPlantCapacityEPC)
        protected TextView tvPlantCapacityEPC;
        @BindView(R.id.tvCurrentPowerEPC)
        protected TextView tvCurrentPowerEPC;
        @BindView(R.id.tvTodaysGeneration)
        protected TextView tvTodaysGeneration;
        @BindView(R.id.tvMonthGeneration)
        protected TextView tvMonthGeneration;
        @BindView(R.id.tvTotalGeneration)
        protected TextView tvTotalGeneration;
        @BindView(R.id.tvPlantStatus)
        protected TextView tvPlantStatus;

        public CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            // this.imageView = (ImageView) view.findViewById(R.id.thumbnail);

        }
    }
}
