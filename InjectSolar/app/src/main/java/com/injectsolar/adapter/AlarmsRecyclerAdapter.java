package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.injectsolar.R;
import com.injectsolar.models.AlarmSectionHeader;
import com.injectsolar.models.AlarmsModel;
import com.injectsolar.views.custom_views.SectionRecyclerViewAdapter;
import com.injectsolar.views.holders.AlaramChildViewHolder;
import com.injectsolar.views.holders.AlarmSectionViewHolder;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class AlarmsRecyclerAdapter extends SectionRecyclerViewAdapter<AlarmSectionHeader, AlarmsModel, AlarmSectionViewHolder, AlaramChildViewHolder> {
    private Context context;
    public AlarmsRecyclerAdapter(Context context, List<AlarmSectionHeader> sectionHeaderItemList, boolean isCleared) {
        super(context, sectionHeaderItemList);
        this.context = context;
    }

    @Override
    public AlarmSectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.alaram_section_item, sectionViewGroup, false);
        return new AlarmSectionViewHolder(view);
    }

    @Override
    public AlaramChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.alarm_item_layout, childViewGroup, false);
        return new AlaramChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(AlarmSectionViewHolder sectionViewHolder, int sectionPosition, AlarmSectionHeader sectionHeader) {
        String strInverterName = "DEVICE ";
        sectionViewHolder.tv_device_inverter.setText(strInverterName);
    }

    @Override
    public void onBindChildViewHolder(AlaramChildViewHolder childViewHolder, int sectionPosition, int childPosition, AlarmsModel child) {
        String strInverterName = child.getDev_name();
        childViewHolder.tv_item_device_inverter.setText(strInverterName);
        childViewHolder.tv_item_alarm_code.setText(child.getInv_name());
        childViewHolder.tv_item_alaram.setText(child.getAlarm_id());
        childViewHolder.tv_item_occurrence_time.setText(child.getDate_time());
        childViewHolder.tv_item_alarm_message.setText(child.getAlarm_msg());
    }
}
