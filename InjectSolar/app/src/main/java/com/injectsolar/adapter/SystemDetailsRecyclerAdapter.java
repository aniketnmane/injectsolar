package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.injectsolar.R;
import com.injectsolar.models.DeviceInfo;
import com.injectsolar.models.SensorInfo;
import com.injectsolar.models.SubDeviceInfo;
import com.injectsolar.models.SystemDetailsSectionHeader;
import com.injectsolar.models.SystemDetailsUIModel;
import com.injectsolar.views.custom_views.SectionRecyclerViewAdapter;
import com.injectsolar.views.holders.SystemDetailSectionViewHolder;
import com.injectsolar.views.holders.SystemDetailsChildViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class SystemDetailsRecyclerAdapter extends SectionRecyclerViewAdapter<SystemDetailsSectionHeader, DeviceInfo, SystemDetailSectionViewHolder, SystemDetailsChildViewHolder> {

    private Context context;
    private boolean isNormal;

    public SystemDetailsRecyclerAdapter(Context context, List<SystemDetailsSectionHeader> sectionHeaderItemList, boolean isNormal) {
        super(context, sectionHeaderItemList);
        this.context = context;
        this.isNormal = isNormal;
    }

    @Override
    public SystemDetailSectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.system_details_row_section, sectionViewGroup, false);
        return new SystemDetailSectionViewHolder(view);
    }

    @Override
    public SystemDetailsChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.system_details_row, childViewGroup, false);
        return new SystemDetailsChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SystemDetailSectionViewHolder sectionViewHolder, int sectionPosition, SystemDetailsSectionHeader section) {
        if (isNormal) sectionViewHolder.tvSensorNameLbl.setVisibility(View.GONE);
    }

    @Override
    public void onBindChildViewHolder(SystemDetailsChildViewHolder childViewHolder, int sectionPosition, int childPosition, DeviceInfo child) {
        childViewHolder.tvDeviceNameSerialNo.setText(String.format("%s-%s(%s)", child.getDev_name(), child.getSerial_no(), child.getMaster_dev_name()));
        ArrayList<SubDeviceInfo> deviceList = child.getSub_device_info_list();
        SystemDetailsUIModel model = getNames(deviceList);
        childViewHolder.tvInverterNo.setText(model.getInverterNo());
        childViewHolder.tvInverterName.setText(model.getInverterName());
        childViewHolder.tvInverterCapacity.setText(model.getInverterCapacity());
        childViewHolder.tvInverterSpecification.setText(model.getInverterSpecification());
        if (!isNormal) {
            childViewHolder.tvSensorName.setVisibility(View.VISIBLE);
            childViewHolder.tvSensorName.setText(model.getSensorName());
        } else childViewHolder.tvSensorName.setVisibility(View.GONE);
        // String.join("\n", deviceList.get(childPosition).getSensor_info())
        //childViewHolder.tvSensorName.setText(deviceList.get(childPosition).getSensor_info());
        // Log.d("Test", String.join("\n", deviceList.get(childPosition).getSensor_info()));
        //Observable.combineLatest()
    }

    private int invNo = 0;

    private SystemDetailsUIModel getNames(ArrayList<SubDeviceInfo> subDeviceInfo) {
        SystemDetailsUIModel updatedModel = new SystemDetailsUIModel();
        String deviceNameSrNo = "";
        StringBuilder inverterNo = new StringBuilder();
        StringBuilder inverterName = new StringBuilder();
        StringBuilder inverterCapacity = new StringBuilder();
        StringBuilder inverterSpecification = new StringBuilder();
        StringBuilder sensorName = new StringBuilder();
        for (int i = 0; i < subDeviceInfo.size(); i++) {
            if (subDeviceInfo.get(i).getSensor_info() != null && sensorName.length() == 0)
                sensorName = new StringBuilder(getSensorName(subDeviceInfo.get(i).getSensor_info()));
            else {
                if (subDeviceInfo.get(i).getSensor_info() != null)
                    sensorName.append("\n").append(getSensorName(subDeviceInfo.get(i).getSensor_info()));
            }

            if (subDeviceInfo.get(i).getName() != null && inverterName.length() == 0)
                inverterName = new StringBuilder(subDeviceInfo.get(i).getName());
            else {
                if (subDeviceInfo.get(i).getName() != null)
                    inverterName.append("\n").append(subDeviceInfo.get(i).getName());
            }

            if (inverterNo.length() == 0) {
                inverterNo = new StringBuilder(subDeviceInfo.get(i).getInv_name());
            } else {
                invNo++;
                inverterNo.append("\n").append(subDeviceInfo.get(i).getInv_name());
            }
            if (subDeviceInfo.get(i).getInstalled_capacity() != null && inverterCapacity.length() == 0)
                inverterCapacity = new StringBuilder(subDeviceInfo.get(i).getInstalled_capacity());
            else {
                if (subDeviceInfo.get(i).getInstalled_capacity() != null)
                    inverterCapacity.append("\n").append(subDeviceInfo.get(i).getInstalled_capacity());
            }
            if (subDeviceInfo.get(i).getSpec() != null && inverterSpecification.length() == 0)
                inverterSpecification = new StringBuilder(subDeviceInfo.get(i).getSpec());
            else {
                if (subDeviceInfo.get(i).getInstalled_capacity() != null)
                    inverterSpecification.append("\n").append(subDeviceInfo.get(i).getSpec());
            }
        }
        updatedModel.setInverterCapacity(inverterCapacity.toString());
        updatedModel.setInverterNo(inverterNo.toString());
        updatedModel.setInverterName(inverterName.toString());
        updatedModel.setInverterSpecification(inverterSpecification.toString());
        updatedModel.setSensorName(sensorName.toString());
        return updatedModel;
    }

    private String getSensorName(ArrayList<SensorInfo> sensorInfoArrayList) {
        StringBuilder sensorName = new StringBuilder();
        if (sensorInfoArrayList != null && sensorInfoArrayList.size() > 0) {
            for (int i = 0; i < sensorInfoArrayList.size(); i++) {
                SensorInfo item = sensorInfoArrayList.get(i);
                if (sensorName.length() == 0)
                    sensorName = new StringBuilder(item.getSensorName());
                else
                    sensorName.append("\n").append(item.getSensorName());
            }
        }
        return sensorName.toString();
    }
}
