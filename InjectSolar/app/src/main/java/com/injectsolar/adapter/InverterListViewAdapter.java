package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;
import com.injectsolar.models.InverterDataModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InverterListViewAdapter extends RecyclerView.Adapter<InverterListViewAdapter.CustomViewHolder> {
    private ArrayList<InverterDataModel> feedItemList;

    public InverterListViewAdapter(Context context, ArrayList<InverterDataModel> feedItemList) {
        this.feedItemList = feedItemList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inverte_data_list_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        InverterDataModel feedItem = feedItemList.get(i);
        int inverterNumber = 1 + i;
        customViewHolder.tv_device_Inverter.setText(String.format("Inverter-" + inverterNumber + ": %s", feedItem.getInv_name()));
        customViewHolder.tv_status.setText(feedItem.getDeviceStatus());
        customViewHolder.tv_current_power.setText(String.format("Current Power (kW)\n\n%s", feedItem.getCurrentPower()));
        customViewHolder.tv_todays_energy.setText(String.format("Today's Energy (KWh)\n\n%s", feedItem.getTodayGeneration()));
        customViewHolder.tv_lifetime_energy.setText(String.format("Lifetime Energy (KWh)\n\n%s", feedItem.getTotalGeneration()));
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_device_inverter)
        protected TextView tv_device_Inverter;
        @BindView(R.id.tv_status)
        protected TextView tv_status;
        @BindView(R.id.tv_current_power)
        protected TextView tv_current_power;
        @BindView(R.id.tv_todays_energy)
        protected TextView tv_todays_energy;
        @BindView(R.id.tv_lifetime_energy)
        protected TextView tv_lifetime_energy;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            // this.imageView = (ImageView) view.findViewById(R.id.thumbnail);

        }
    }
}
