package com.injectsolar.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.injectsolar.R;
import com.injectsolar.models.EPCDeviceDataUserModel;
import com.injectsolar.models.EPCUserSectionHeader;
import com.injectsolar.views.custom_views.SectionRecyclerViewAdapter;
import com.injectsolar.views.holders.EpcUserChildViewHolder;
import com.injectsolar.views.holders.EpcUserSectionViewHolder;

import java.util.List;

/**
 * Created by Aniket on 11/7/19.
 */

public class EPCUserRecyclerAdapter extends SectionRecyclerViewAdapter<EPCUserSectionHeader, EPCDeviceDataUserModel, EpcUserSectionViewHolder, EpcUserChildViewHolder> {

    private Context context;
    private boolean isNormal;

    public EPCUserRecyclerAdapter(Context context, List<EPCUserSectionHeader> sectionHeaderItemList, boolean isNormal) {
        super(context, sectionHeaderItemList);
        this.context = context;
        this.isNormal = isNormal;
    }

    @Override
    public EpcUserSectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.epc_user_list_section_item, sectionViewGroup, false);
        return new EpcUserSectionViewHolder(view);
    }

    @Override
    public EpcUserChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.epc_user_list_row, childViewGroup, false);
        return new EpcUserChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(EpcUserSectionViewHolder sectionViewHolder, int sectionPosition, EPCUserSectionHeader section) {

    }

    @Override
    public void onBindChildViewHolder(EpcUserChildViewHolder childViewHolder, int sectionPosition, int childPosition, EPCDeviceDataUserModel child) {
        childViewHolder.tvLoginId.setText(Html.fromHtml("<u>" + child.getLogin_id() + "</u>"));
        childViewHolder.tvLoginId.setTextColor(Color.parseColor("#609CF6"));
        childViewHolder.tvCurrentPowerEPC.setText(child.getTotalCurrentPower());
        childViewHolder.tvMonthGeneration.setText(child.getTotalMonthlyEnergyGenerated());
        childViewHolder.tvPlantCapacityEPC.setText(child.getPlant_capacity());
        if (child.getDevice_status().equalsIgnoreCase("connected"))
            childViewHolder.tvPlantStatus.setTextColor(Color.parseColor("#679C57"));
        else
            childViewHolder.tvPlantStatus.setTextColor(Color.parseColor("#EC5B50"));
        childViewHolder.tvPlantStatus.setText(child.getDevice_status());
        childViewHolder.tvTodaysGeneration.setText(child.getTotalTodayEnergyGenerated());
        childViewHolder.tvTotalGeneration.setText(child.getTotalEnergyGenerated());
    }
}
