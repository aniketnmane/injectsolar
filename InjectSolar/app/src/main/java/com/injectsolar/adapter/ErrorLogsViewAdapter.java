package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;
import com.injectsolar.models.ErrorLogsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ErrorLogsViewAdapter extends RecyclerView.Adapter<ErrorLogsViewAdapter.CustomViewHolder> {
    private ArrayList<ErrorLogsModel> feedItemList;
    private Context mContext;

    public ErrorLogsViewAdapter(Context context, ArrayList<ErrorLogsModel> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.error_list_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        ErrorLogsModel feedItem = feedItemList.get(i);
        String inverterNo = "" + i + 1;
        customViewHolder.tv_device_Inverter.setText("Inverter: " + inverterNo);
        customViewHolder.tv_received_at.setText("Received At: " + feedItem.getReceived_at());
        customViewHolder.tv_error_no.setText("Error No: " + feedItem.getErrorNo());
        customViewHolder.tv_name_dc.setText("Name: " + feedItem.getAlarmName());
        customViewHolder.tv_msg.setText("Message: " + feedItem.getAlarmMsg());

        //Render image using Picasso library
       /* if (!TextUtils.isEmpty(feedItem.getThumbnail())) {
            Picasso.with(mContext).load(feedItem.getThumbnail())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(customViewHolder.imageView);
        }

        //Setting text view title
        customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));*/
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_device_inverter)
        protected TextView tv_device_Inverter;
        @BindView(R.id.tv_received_at)
        protected TextView tv_received_at;
        @BindView(R.id.tv_error_no)
        protected TextView tv_error_no;
        @BindView(R.id.tv_name_dc)
        protected TextView tv_name_dc;
        @BindView(R.id.tv_msg)
        protected TextView tv_msg;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            // this.imageView = (ImageView) view.findViewById(R.id.thumbnail);

        }
    }
}