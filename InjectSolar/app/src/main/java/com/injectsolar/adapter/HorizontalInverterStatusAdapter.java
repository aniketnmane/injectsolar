package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;
import com.injectsolar.models.InverterStatus;

import java.util.List;

/**
 * Created by Aniket .
 */

public class HorizontalInverterStatusAdapter extends RecyclerView.Adapter<HorizontalInverterStatusAdapter.MyView> {
    private List<InverterStatus> list;
    private Context context;

    public class MyView extends RecyclerView.ViewHolder {
        public TextView textView;
        public CardView cardView;

        public MyView(View view) {
            super(view);
            textView = view.findViewById(R.id.tvItem);
            cardView = view.findViewById(R.id.cardview);

        }
    }

    public HorizontalInverterStatusAdapter(List<InverterStatus> horizontalList) {
        this.list = horizontalList;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_item, parent, false);
        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {
        InverterStatus status = list.get(position);
        if (status.getInvStatus().equalsIgnoreCase("Disconnected")) {
            holder.cardView.setBackgroundResource(R.drawable.button_rounded_corners_red_bg);
        } else {
            holder.cardView.setBackgroundResource(R.drawable.button_rounded_corners_green_bg);
        }
        holder.textView.setText(list.get(position).getInvName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}