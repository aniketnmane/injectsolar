package com.injectsolar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.injectsolar.R;
import com.injectsolar.models.MPPTStringsModel;
import com.injectsolar.models.SectionHeader;
import com.injectsolar.views.custom_views.SectionRecyclerViewAdapter;
import com.injectsolar.views.holders.AlaramChildViewHolder;
import com.injectsolar.views.holders.AlarmSectionViewHolder;
import java.util.List;

public class AlaramSectionAdapter extends SectionRecyclerViewAdapter<SectionHeader, MPPTStringsModel, AlarmSectionViewHolder, AlaramChildViewHolder> {
    private Context context;
    private boolean isUncleared;

    public AlaramSectionAdapter(Context context, List<SectionHeader> sectionHeaderItemList, boolean isUncleared) {
        super(context, sectionHeaderItemList);
        this.context = context;
        this.isUncleared = isUncleared;
    }

    @Override
    public AlarmSectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.alaram_section_item, sectionViewGroup, false);
        return new AlarmSectionViewHolder(view);
    }

    @Override
    public AlaramChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.alarm_item_layout, childViewGroup, false);
        return new AlaramChildViewHolder(view);
    }


    @Override
    public void onBindSectionViewHolder(AlarmSectionViewHolder sectionViewHolder, int sectionPosition, SectionHeader sectionHeader) {
        String strInverterName = "Inverter " + (sectionPosition + 1);
    }

    @Override
    public void onBindChildViewHolder(AlaramChildViewHolder childViewHolder, int sectionPosition, int childPosition, MPPTStringsModel child) {
        if (isUncleared) {
            String strInverterName = "Uncleared Alarms " + (childPosition + 1);
            childViewHolder.tv_item_device_inverter.setText(strInverterName);
        } else {
            String strInverterName = "Cleared Alarms " + (childPosition + 1);
            childViewHolder.tv_item_alarm_message.setText(strInverterName);
        }

        childViewHolder.tv_item_alarm_code.setText(child.getV());
        childViewHolder.tv_item_alarm_message.setText(child.getI());
        childViewHolder.tv_item_occurrence_time.setText(child.getP());
        childViewHolder.tv_item_alaram.setText(child.getP());
    }
}
