package com.injectsolar.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.injectsolar.R;
import com.injectsolar.models.ErrorLogsModel;
import com.injectsolar.models.ParameterModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParametersViewAdapter extends RecyclerView.Adapter<ParametersViewAdapter.CustomViewHolder> {
    private ArrayList<ParameterModel> feedItemList;
    private Context mContext;

    public ParametersViewAdapter(Context context, ArrayList<ParameterModel> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.parameter_list_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        ParameterModel feedItem = feedItemList.get(i);
        customViewHolder.tv_parameterName.setText(feedItem.getP_name());
        customViewHolder.tv_parameterValue.setText(feedItem.getP_value());
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_parameterName)
        protected TextView tv_parameterName;
        @BindView(R.id.tv_parameterValue)
        protected TextView tv_parameterValue;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}