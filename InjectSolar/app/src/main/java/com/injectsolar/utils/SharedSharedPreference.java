package com.injectsolar.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedSharedPreference {
    private static SharedPreferences sharedPreferences;

    public static final String mypreference = "mypref";
    public static final String userName = "userName";
    public static final String userPassword = "userPassword";

    private static final SharedSharedPreference ourInstance = new SharedSharedPreference();

    public static SharedSharedPreference getInstance(Context context) {
        sharedPreferences = context.getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        return ourInstance;
    }

    private SharedSharedPreference() {
    }

    public void putUserDetails(String userName, String userPassword) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(this.userName, userName);
        editor.putString(this.userPassword, userPassword);
        editor.commit();
    }

    public SharedPreferences getSharedPreferences() {

        return sharedPreferences;
    }

    public static void clearPreference(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public String getUserName() {
        return getSharedPreferences().getString(userName, "");
    }

    public String getUserPassword() {
        return getSharedPreferences().getString(userPassword, "");
    }

}
