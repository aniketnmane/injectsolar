package com.injectsolar.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

public class Typefaces {
    private static final String TAG = Typefaces.class.getSimpleName();
    public static final String FONT_LATO_BOLD = "font/HelveticaNeueLTPro-Bd.otf";
    public static final String FONT_LATO_REGULAR = "font/HelveticaNeueLTPro-Lt.otf";
    public static final String FONT_MONTSERRAT_BOLD = "font/HelveticaNeueLTPro-Th.otf";
    public static final String FONT_MONTSERRAT_REGULAR = "font/HelveticaNeueLTPro-Md.otf";

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(Context c, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(c.getAssets(),
                            assetPath);
                    cache.put(assetPath, t);
                } catch (Exception e) {
                    Log.e(TAG, "Could not get typeface '" + assetPath
                            + "' because " + e.getMessage());
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}
