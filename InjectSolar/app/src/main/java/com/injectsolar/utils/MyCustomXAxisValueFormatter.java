package com.injectsolar.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;

public class MyCustomXAxisValueFormatter implements IAxisValueFormatter {

    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

       return formatter.format(value);
    }
}
