# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class tvInverterNo to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file tvInverterNo.
#-renamesourcefileattribute SourceFile
-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}
#Parceler
-keep interface org.parceler.**
-keep interface org.parceler.Parcel
-keep @org.parceler.Parcel class * { *; }
-keep class **$$Parcelable { *; }
-keep class org.parceler.Parceler$$Parcels

-keep public class com.injectsolar.models.** {
  public void set*(***);
  public *** get*();
}
-keep public class com.injectsolar.models.SensorDataModel {
  public void set*(***);
  public *** get*();
}
-keep public class com.injectsolar.models.SensorModel {
  public void set*(***);
  public *** get*();
}
